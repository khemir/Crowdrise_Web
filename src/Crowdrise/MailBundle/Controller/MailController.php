<?php
namespace Crowdrise\MailBundle\Controller;

use Crowdrise\UserBundle\Entity\utilisateur;
use Swift_Mailer;
use Swift_Message;
use Swift_SendmailTransport;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class MailController extends Controller{
    public function indexAction($name)
    {
        return $this->render('CrowdriseMailBundle:Default:index.html.twig', array('name' => $name));
    }
    
    public function redirectionAction()
    {
        return $this->render('CrowdriseMailBundle:Default:mail.html.twig');
    }
    
    
    public function sendMailAction()
    {
        $Request = $this->getRequest();
        if($Request->getMethod() == "POST")
        {
            $Subject = $Request->get("Subject");
            $u = new utilisateur();
            $message = $Request->get("message");
            
            
            $mailer = $this->container->get('mailer');
            $transport = Swift_SendmailTransport::newInstance('smtp.gmail.com',465,'ssl');
           
            $mailer = Swift_Mailer::newInstance($transport);
            
            $message = Swift_Message::newInstance('Test')
                    ->setSubject($Subject)
                    ->setFrom($u->getEmail())
                    ->setTo('crowdrise.adm@gmail.com')
                    ->setBody($message);
           
            $this->get('mailer')->send($message);
            
            
        }
        
        
        
        
        
        return $this->render('CrowdriseMailBundle:Default:mail.html.twig');
    }
}
