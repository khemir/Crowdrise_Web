<?php
namespace Crowdrise\MailBundle\Entity; 
class Mail 
{ 
private $nom;
private $prenom; 
private $tel;
private $from;
private $text;


    public function getNom()
    {
        return $this->nom;
    }

    public function getText()
    {
        return $this->text;
    }
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }
    
     public function getPrenom()
    {
        return $this->prenom;
    }
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }
       public function getTel()
    {
        return $this->tel;
    }
    public function setTel($tel)
    {
        $this->tel = $tel;

        return $this;
    }
        public function getFrom()
    {
        return $this->from;
    }
    public function setFrom($from)
    {
        $this->from = $from;

        return $this;
    }
    
}
?>