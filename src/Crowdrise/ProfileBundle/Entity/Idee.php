<?php

namespace Crowdrise\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Idee
 *
 * @ORM\Table(name="idee", indexes={@ORM\Index(name="id_utilisateur", columns={"id_utilisateur"})})
 * @ORM\Entity
 */
class Idee
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_idee", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idIdee;

    /**
     * @var string
     *
     * @ORM\Column(name="intitule_idee", type="string", length=30, nullable=false)
     */
    private $intituleIdee;

    /**
     * @var string
     *
     * @ORM\Column(name="description_idee", type="string", length=500, nullable=false)
     */
    private $descriptionIdee;

    /**
     * @var string
     *
     * @ORM\Column(name="categorie_idee", type="string", length=30, nullable=false)
     */
    private $categorieIdee;

    /**
     * @var string
     *
     * @ORM\Column(name="etat_idee", type="string", length=30, nullable=false)
     */
    private $etatIdee;

    /**
     * @var string
     *
     * @ORM\Column(name="date_depot_idee", type="string", length=30, nullable=false)
     */
    private $dateDepotIdee;

    /**
     * @var float
     *
     * @ORM\Column(name="somme_recolte_idee", type="float", precision=10, scale=0, nullable=false)
     */
    private $sommeRecolteIdee;

    /**
     * @var float
     *
     * @ORM\Column(name="note_idee", type="float", precision=10, scale=0, nullable=false)
     */
    private $noteIdee;

    /**
     * @var string
     *
     * @ORM\Column(name="URL_video", type="string", length=255, nullable=false)
     */
    private $urlVideo;

    /**
     * @var \Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_utilisateur", referencedColumnName="id_utilisateur")
     * })
     */
    private $idUtilisateur;


}

