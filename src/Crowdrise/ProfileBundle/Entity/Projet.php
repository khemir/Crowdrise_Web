<?php

namespace Crowdrise\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Projet
 *
 * @ORM\Table(name="projet", indexes={@ORM\Index(name="fk_projet_utilisateur", columns={"id_utilisateur"})})
 * @ORM\Entity
 */
class Projet
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_projet", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idProjet;

    /**
     * @var string
     *
     * @ORM\Column(name="intitule_projet", type="string", length=30, nullable=false)
     */
    private $intituleProjet;

    /**
     * @var string
     *
     * @ORM\Column(name="categorie_projet", type="string", length=30, nullable=false)
     */
    private $categorieProjet;

    /**
     * @var string
     *
     * @ORM\Column(name="description_projet", type="string", length=500, nullable=false)
     */
    private $descriptionProjet;

    /**
     * @var string
     *
     * @ORM\Column(name="competences_demande_projet", type="string", length=500, nullable=false)
     */
    private $competencesDemandeProjet;

    /**
     * @var string
     *
     * @ORM\Column(name="motcle_projet", type="string", length=30, nullable=false)
     */
    private $motcleProjet;

    /**
     * @var string
     *
     * @ORM\Column(name="date_depot_projet", type="string", length=30, nullable=false)
     */
    private $dateDepotProjet;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbsolvers_projet", type="integer", nullable=false)
     */
    private $nbsolversProjet;

    /**
     * @var \Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_utilisateur", referencedColumnName="id_utilisateur")
     * })
     */
    private $idUtilisateur;


}

