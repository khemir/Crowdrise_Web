<?php

namespace Crowdrise\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Paiement
 *
 * @ORM\Table(name="paiement", indexes={@ORM\Index(name="fk_utilisateurp", columns={"id_utilisateur_p"}), @ORM\Index(name="fk_utilisateurb", columns={"id_utilisateur_b"})})
 * @ORM\Entity
 */
class Paiement
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_paiement", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idPaiement;

    /**
     * @var integer
     *
     * @ORM\Column(name="montant_paiement", type="integer", nullable=false)
     */
    private $montantPaiement;

    /**
     * @var integer
     *
     * @ORM\Column(name="date_paiement", type="integer", nullable=false)
     */
    private $datePaiement;

    /**
     * @var \Utilisateur
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_utilisateur_b", referencedColumnName="id_utilisateur")
     * })
     */
    private $idUtilisateurB;

    /**
     * @var \Utilisateur
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_utilisateur_p", referencedColumnName="id_utilisateur")
     * })
     */
    private $idUtilisateurP;


}

