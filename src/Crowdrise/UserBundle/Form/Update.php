<?php

namespace Crowdrise\UserBundle\Form;

use FOS\UserBundle\Util\LegacyFormHelper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class Update extends AbstractType {

    public function buildForm(FormBuilderInterface
    $builder, array $options) {
        $builder
            ->add('username', null, array('label' => 'form.username', 'translation_domain' => 'FOSUserBundle'))
            ->add('email', LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\EmailType'), array('label' => 'form.email', 'translation_domain' => 'FOSUserBundle'))
            ->add('nomUtilisateur',null, array('label' => 'ForName', 'translation_domain' => 'FOSUserBundle'))
            ->add('competences',null, array('label' => 'Skills', 'translation_domain' => 'FOSUserBundle'))
            ->add('experience',null, array('label' => 'Experience', 'translation_domain' => 'FOSUserBundle'))
         
            ;
        // ->setMethod("GET");
    }

    public function getName() {
        return 'nom';
    }
 
}
