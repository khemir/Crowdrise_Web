<?php

namespace Crowdrise\UserBundle\Controller;

use Crowdrise\UserBundle\Entity\utilisateur;
use Crowdrise\UserBundle\Form\ImageForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\User\UserInterface;
use Crowdrise\UserBundle\Form\Update;

class ProfileController extends Controller {
    
    
    
    public function AfficheProjetAction() {
        $em = $this->getDoctrine()->getManager();
        $projets = $em->getRepository("CrowdriseUserBundle:Projet")->findAll();
        return $this->render("CrowdriseUserBundle:Projet:AfficheProjet.html.twig", array("projet" => $projets));
    }
    
    
     public function showAction()
    {
        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        return $this->render('FOSUserBundle:Profile:show.html.twig', array(
            'user' => $user
        ));
    }
    
//    public function editAction(Request $request)
//    {
//        $user = $this->getUser();
//        if (!is_object($user) || !$user instanceof UserInterface) {
//            throw new AccessDeniedException('This user does not have access to this section.');
//        }
//
//        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
//        $dispatcher = $this->get('event_dispatcher');
//
//        $event = new GetResponseUserEvent($user, $request);
//        $dispatcher->dispatch(FOSUserEvents::PROFILE_EDIT_INITIALIZE, $event);
//
//        if (null !== $event->getResponse()) {
//            return $event->getResponse();
//        }
//
//        /** @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface */
//        $formFactory = $this->get('fos_user.profile.form.factory');
//
//        $form = $formFactory->createForm();
//        $form->setData($user);
//
//        $form->handleRequest($request);
//
//        if ($form->isValid()) {
//            /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
//            $userManager = $this->get('fos_user.user_manager');
//
//            $event = new FormEvent($form, $request);
//            $dispatcher->dispatch(FOSUserEvents::PROFILE_EDIT_SUCCESS, $event);
//
//            $userManager->updateUser($user);
//
//            if (null === $response = $event->getResponse()) {
//                $url = $this->generateUrl('fos_user_profile_show');
//                $response = new RedirectResponse($url);
//            }
//
//            $dispatcher->dispatch(FOSUserEvents::PROFILE_EDIT_COMPLETED, new FilterUserResponseEvent($user, $request, $response));
//
//            return $response;
//        }
//
//        return $this->render('CrowdriseUserBundle:Profile:edit.html.twig', array(
//            'form' => $form->createView()
//        ));
//    }
    
    public function updateAction($idUtilisateur) {
        $em = $this->getDoctrine()->getManager();
        $modele = $em->getRepository('CrowdriseUserBundle:utilisateur')->find($idUtilisateur);
        $Form = $this->createForm(new Update(), $modele);
        $request = $this->get('request_stack')->getCurrentRequest();
        $Form->handleRequest($request);

        if ($Form->isValid()) {

            $em->persist($modele);
            $em->flush();
            return $this->redirect($this->generateUrl('fos_user_profile_show'));
        }
        return $this->render("CrowdriseUserBundle:Profile:Update.html.twig", array('Form' => $Form->createView()));
    }
   public function uploadAction() 
           {
       $im=new utilisateur(); 
       $form = $this->createForm(new ImageForm(), $im);
       $request = $this->get('request_stack')->getCurrentRequest(); 
       $form->handleRequest($request);
       if ($form->isValid()) 
           {
           $em = $this->getDoctrine()->getManager();
           $stream = fopen($im->getFile(),'rb'); 
           $im->setImg(stream_get_contents($stream)); 
           $em->persist($im); $em->flush(); 
           return $this->render('CrowdriseUserBundle:Profile:index.html.twig', array());
           }
           return $this->render('CrowdriseUserBundle:Profile:upload.html.twig', array('Form'=>$form->createView())); 
          
           }

           public function listAction()
                   {
               $em = $this->getDoctrine()->getManager();
               $image=$em->getRepository('CrowdriseUserBundle:utilisateur')->findAll();
               return $this->render('CrowdriseUserBundle:Profile:list.html.twig', array('images'=>$image)); }
          public function afficheAction($id) 
                  {
              $em = $this->getDoctrine()->getManager(); 
              $image=$em->getRepository('CrowdriseUserBundle:utilisateur')->find($id);
              return $this->render('CrowdriseUserBundle:Profile:affiche.html.twig', array('images'=>$image)); }
public function photoAction($id)
        {
    $em = $this->getDoctrine()->getManager(); 
    $image_obj = $em->getRepository('CrowdriseUserBundle:utilisateur')->find($id);
    $photo=$image_obj->getId(); 
    $response = new StreamedResponse(function () use ($photo) { echo stream_get_contents($photo); });
    $response->headers->set('Content-Type', 'image/jpeg'); 
    return $response; }
              
              
                  }
           

    
    