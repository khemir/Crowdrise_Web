<?php

namespace Crowdrise\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('CrowdriseUserBundle:Default:index.html.twig', array('name' => $name));
    }
}
