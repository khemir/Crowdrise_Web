<?php

namespace Crowdrise\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FinancementIdee
 *
 * @ORM\Table(name="financement_idee", indexes={@ORM\Index(name="fk_financement_idee_utilisateur", columns={"id_utilisateur"}), @ORM\Index(name="fk_financement_idee_idee", columns={"id_idee"})})
 * @ORM\Entity
 */
class FinancementIdee
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_financement", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idFinancement;

    /**
     * @var float
     *
     * @ORM\Column(name="montant_fidee", type="float", precision=10, scale=0, nullable=false)
     */
    private $montantFidee;

    /**
     * @var string
     *
     * @ORM\Column(name="date_fidee", type="string", length=30, nullable=false)
     */
    private $dateFidee;

    /**
     * @var \Idee
     *
     * @ORM\ManyToOne(targetEntity="Idee")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_idee", referencedColumnName="id_idee")
     * })
     */
    private $idee;

    /**
     * @var \Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_utilisateur", referencedColumnName="id_utilisateur")
     * })
     */
    private $idUtilisateur;


}

