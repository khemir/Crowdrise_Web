<?php
namespace Crowdrise\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Symfony\Component\Validator\Constraints as Assert;

/**
  * @ORM\Entity 
  * @ORM\Table(name="utilisateur")
  */
class utilisateur extends BaseUser
{ 
     /**
     * @var integer
     *
     * @ORM\Column(name="id_utilisateur", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
     protected $idUtilisateur;
 
 
 

     /**
     * @var string
     *
     * @ORM\Column(name="nom_utilisateur", type="string", length=30, nullable=false)
     */
    private $nomUtilisateur;
    
  

    /**
     * @var string
     *
     * @ORM\Column(name="prenom_utilisateur", type="string", length=30, nullable=false)
     */
    private $prenomUtilisateur;
    /**
     * @var integer
     *
     * @ORM\Column(name="note", type="integer", nullable=true)
     */
    private $note;

    /**
     * @var string
     *
     * @ORM\Column(name="competences", type="string", length=500, nullable=true)
     */
    private $competences;

    /**
     * @var string
     *
     * @ORM\Column(name="experience", type="string", length=200, nullable=true)
     */
    private $experience;

    /**
     * @var integer
     *
     * @ORM\Column(name="nb_projets", type="integer", nullable=true)
     */
    private $nbProjets;

    /**
     * @var integer
     *
     * @ORM\Column(name="nb_idees", type="integer", nullable=true)
     */
    private $nbIdees;

    /**
     * @var string
     *
     * @ORM\Column(name="URL_img", type="string", length=255, nullable=true)
     */
    private $urlImg;
    /** 
  *
  * @Assert\File(maxSize="6000000")
  *
  */
 public $file;
 function getFile() {
     return $this->file;
 }

 function setFile($file) {
     $this->file = $file;
 }
     function getNomUtilisateur() {
        return $this->nomUtilisateur;
    }

    function setPrenomUtilisateur($prenom) {
        $this->prenom= $prenom;
    }
     function getPrenomUtilisateur() {
        return $this->prenomUtilisateur;
    }

    function setNomUtilisateur($nom) {
        $this->nomUtilisateur= $nom;
    }

    public function __construct() 
            { 
        parent::__construct();
        // your own logic 
            } 
     public function getParent()
             { return 'FOSUserBundle'; }public function getId(){
}

function getIdUtilisateur() {
    return $this->idUtilisateur;
}

function getNote() {
    return $this->note;
}

function getCompetences() {
    return $this->competences;
}

function getExperience() {
    return $this->experience;
}

function getNbProjets() {
    return $this->nbProjets;
}

function getNbIdees() {
    return $this->nbIdees;
}

function getUrlImg() {
    return $this->urlImg;
}

function setIdUtilisateur($idUtilisateur) {
    $this->idUtilisateur = $idUtilisateur;
}

function setNote($note) {
    $this->note = $note;
}

function setCompetences($competences) {
    $this->competences = $competences;
}

function setExperience($experience) {
    $this->experience = $experience;
}

function setNbProjets($nbProjets) {
    $this->nbProjets = $nbProjets;
}

function setNbIdees($nbIdees) {
    $this->nbIdees = $nbIdees;
}

function setUrlImg($urlImg) {
    $this->urlImg = $urlImg;
}

function getBloque() {
    return $this->bloque;
}

function setBloque($bloque) {
    $this->bloque = $bloque;
}


} 
?>