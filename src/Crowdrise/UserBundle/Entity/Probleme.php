<?php

namespace Crowdrise\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Probleme
 *
 * @ORM\Table(name="probleme", indexes={@ORM\Index(name="fk_probleme_utilisateur", columns={"id_utilisateur"})})
 * @ORM\Entity
 */
class Probleme
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_probleme", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idProbleme;

    /**
     * @var string
     *
     * @ORM\Column(name="intitule_probleme", type="string", length=30, nullable=false)
     */
    private $intituleProbleme;

    /**
     * @var string
     *
     * @ORM\Column(name="contenu_probleme", type="string", length=500, nullable=false)
     */
    private $contenuProbleme;

    /**
     * @var string
     *
     * @ORM\Column(name="date_depot_probleme", type="string", length=50, nullable=false)
     */
    private $dateDepotProbleme;

    /**
     * @var string
     *
     * @ORM\Column(name="etat_probleme", type="string", length=30, nullable=false)
     */
    private $etatProbleme;

    /**
     * @var \Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_utilisateur", referencedColumnName="id_utilisateur")
     * })
     */
    private $idUtilisateur;


}

