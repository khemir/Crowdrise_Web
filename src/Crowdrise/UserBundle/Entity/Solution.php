<?php

namespace Crowdrise\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Solution
 *
 * @ORM\Table(name="solution", indexes={@ORM\Index(name="fk_solution_utilisateur", columns={"id_utilisateur"}), @ORM\Index(name="fk_solution_probleme", columns={"id_probleme"})})
 * @ORM\Entity
 */
class Solution
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_solution", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idSolution;

    /**
     * @var string
     *
     * @ORM\Column(name="intitule_solution", type="string", length=30, nullable=false)
     */
    private $intituleSolution;

    /**
     * @var string
     *
     * @ORM\Column(name="date_solution", type="string", length=30, nullable=false)
     */
    private $dateSolution;

    /**
     * @var string
     *
     * @ORM\Column(name="contenu_solution", type="string", length=500, nullable=false)
     */
    private $contenuSolution;

    /**
     * @var \Probleme
     *
     * @ORM\ManyToOne(targetEntity="Probleme")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_probleme", referencedColumnName="id_probleme")
     * })
     */
    private $idProbleme;

    /**
     * @var \Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_utilisateur", referencedColumnName="id_utilisateur")
     * })
     */
    private $idUtilisateur;


}

