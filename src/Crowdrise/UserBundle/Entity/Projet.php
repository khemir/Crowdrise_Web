<?php

namespace Crowdrise\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Projet
 *
 * @ORM\Table(name="projet", indexes={@ORM\Index(name="fk_projet_utilisateur", columns={"id_utilisateur"})})
 * @ORM\Entity
 */
class Projet
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_projet", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idProjet;

    /**
     * @var string
     *
     * @ORM\Column(name="intitule_projet", type="string", length=30, nullable=false)
     */
    private $intituleProjet;

    /**
     * @var string
     *
     * @ORM\Column(name="categorie_projet", type="string", length=30, nullable=false)
     */
    private $categorieProjet;

    /**
     * @var string
     *
     * @ORM\Column(name="description_projet", type="string", length=500, nullable=false)
     */
    private $descriptionProjet;

    /**
     * @var string
     *
     * @ORM\Column(name="competences_demande_projet", type="string", length=500, nullable=false)
     */
    private $competencesDemandeProjet;

    /**
     * @var string
     *
     * @ORM\Column(name="motcle_projet", type="string", length=30, nullable=false)
     */
    private $motcleProjet;

    /**
     * @var string
     *
     * @ORM\Column(name="date_depot_projet", type="string", length=30, nullable=false)
     */
    private $dateDepotProjet;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbsolvers_projet", type="integer", nullable=false)
     */
    private $nbsolversProjet;

    /**
     * @var \Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_utilisateur", referencedColumnName="id_utilisateur")
     * })
     */
    private $idUtilisateur;
    function getIdProjet() {
        return $this->idProjet;
    }

    function getIntituleProjet() {
        return $this->intituleProjet;
    }

    function getCategorieProjet() {
        return $this->categorieProjet;
    }

    function getDescriptionProjet() {
        return $this->descriptionProjet;
    }

    function getCompetencesDemandeProjet() {
        return $this->competencesDemandeProjet;
    }

    function getMotcleProjet() {
        return $this->motcleProjet;
    }

    function getDateDepotProjet() {
        return $this->dateDepotProjet;
    }

    function getNbsolversProjet() {
        return $this->nbsolversProjet;
    }

    function getIdUtilisateur() {
        return $this->idUtilisateur;
    }

    function setIdProjet($idProjet) {
        $this->idProjet = $idProjet;
    }

    function setIntituleProjet($intituleProjet) {
        $this->intituleProjet = $intituleProjet;
    }

    function setCategorieProjet($categorieProjet) {
        $this->categorieProjet = $categorieProjet;
    }

    function setDescriptionProjet($descriptionProjet) {
        $this->descriptionProjet = $descriptionProjet;
    }

    function setCompetencesDemandeProjet($competencesDemandeProjet) {
        $this->competencesDemandeProjet = $competencesDemandeProjet;
    }

    function setMotcleProjet($motcleProjet) {
        $this->motcleProjet = $motcleProjet;
    }

    function setDateDepotProjet($dateDepotProjet) {
        $this->dateDepotProjet = $dateDepotProjet;
    }

    function setNbsolversProjet($nbsolversProjet) {
        $this->nbsolversProjet = $nbsolversProjet;
    }

    function setIdUtilisateur(\Utilisateur $idUtilisateur) {
        $this->idUtilisateur = $idUtilisateur;
    }



}

