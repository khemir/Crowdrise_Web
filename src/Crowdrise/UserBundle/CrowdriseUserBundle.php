<?php

namespace Crowdrise\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class CrowdriseUserBundle extends Bundle
{
    public function getParent()
            { return 'FOSUserBundle'; }
}
