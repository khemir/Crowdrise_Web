<?php

namespace Crowdrise\AdministrationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Probleme
 *
 * @ORM\Table(name="probleme", indexes={@ORM\Index(name="fk_probleme_utilisateur", columns={"id_utilisateur"})})
 * @ORM\Entity(repositoryClass="Crowdrise\AdministrationBundle\Entity\ProblemeRepository")
 */
class Probleme
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_probleme", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idProbleme;

    /**
     * @var string
     *
     * @ORM\Column(name="intitule_probleme", type="string", length=30, nullable=false)
     */
    private $intituleProbleme;

    /**
     * @var string
     *
     * @ORM\Column(name="contenu_probleme", type="string", length=500, nullable=false)
     */
    private $contenuProbleme;

    /**
     * @var string
     *
     * @ORM\Column(name="date_depot_probleme", type="string", length=50, nullable=false)
     */
    private $dateDepotProbleme;

    /**
     * @var string
     *
     * @ORM\Column(name="etat_probleme", type="string", length=30, nullable=false)
     */
    private $etatProbleme;

    /**
     * @var \Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_utilisateur", referencedColumnName="id_utilisateur")
     * })
     */
    private $idUtilisateur;



    /**
     * Get idProbleme
     *
     * @return integer 
     */
    public function getIdProbleme()
    {
        return $this->idProbleme;
    }

    /**
     * Set intituleProbleme
     *
     * @param string $intituleProbleme
     * @return Probleme
     */
    public function setIntituleProbleme($intituleProbleme)
    {
        $this->intituleProbleme = $intituleProbleme;

        return $this;
    }

    /**
     * Get intituleProbleme
     *
     * @return string 
     */
    public function getIntituleProbleme()
    {
        return $this->intituleProbleme;
    }

    /**
     * Set contenuProbleme
     *
     * @param string $contenuProbleme
     * @return Probleme
     */
    public function setContenuProbleme($contenuProbleme)
    {
        $this->contenuProbleme = $contenuProbleme;

        return $this;
    }

    /**
     * Get contenuProbleme
     *
     * @return string 
     */
    public function getContenuProbleme()
    {
        return $this->contenuProbleme;
    }

    /**
     * Set dateDepotProbleme
     *
     * @param string $dateDepotProbleme
     * @return Probleme
     */
    public function setDateDepotProbleme($dateDepotProbleme)
    {
        $this->dateDepotProbleme = $dateDepotProbleme;

        return $this;
    }

    /**
     * Get dateDepotProbleme
     *
     * @return string 
     */
    public function getDateDepotProbleme()
    {
        return $this->dateDepotProbleme;
    }

    /**
     * Set etatProbleme
     *
     * @param string $etatProbleme
     * @return Probleme
     */
    public function setEtatProbleme($etatProbleme)
    {
        $this->etatProbleme = $etatProbleme;

        return $this;
    }

    /**
     * Get etatProbleme
     *
     * @return string 
     */
    public function getEtatProbleme()
    {
        return $this->etatProbleme;
    }

    /**
     * Set idUtilisateur
     *
     * @param \Crowdrise\AdministrationBundle\Entity\Utilisateur $idUtilisateur
     * @return Probleme
     */
    public function setIdUtilisateur(\Crowdrise\AdministrationBundle\Entity\Utilisateur $idUtilisateur = null)
    {
        $this->idUtilisateur = $idUtilisateur;

        return $this;
    }

    /**
     * Get idUtilisateur
     *
     * @return \Crowdrise\AdministrationBundle\Entity\Utilisateur 
     */
    public function getIdUtilisateur()
    {
        return $this->idUtilisateur;
    }
}
