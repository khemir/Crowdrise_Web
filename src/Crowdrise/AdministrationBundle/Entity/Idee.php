<?php

namespace Crowdrise\AdministrationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Idee
 *
 * @ORM\Table(name="idee", indexes={@ORM\Index(name="id_utilisateur", columns={"id_utilisateur"})})
 * @ORM\Entity(repositoryClass="Crowdrise\AdministrationBundle\Entity\IdeeRepository")
 */
class Idee
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_idee", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idIdee;

    /**
     * @var string
     *
     * @ORM\Column(name="intitule_idee", type="string", length=30, nullable=false)
     */
    private $intituleIdee;

    /**
     * @var string
     *
     * @ORM\Column(name="description_idee", type="string", length=500, nullable=false)
     */
    private $descriptionIdee;

    /**
     * @var string
     *
     * @ORM\Column(name="categorie_idee", type="string", length=30, nullable=false)
     */
    private $categorieIdee;

    /**
     * @var string
     *
     * @ORM\Column(name="etat_idee", type="string", length=30, nullable=false)
     */
    private $etatIdee;

    /**
     * @var string
     *
     * @ORM\Column(name="date_depot_idee", type="string", length=30, nullable=false)
     */
    private $dateDepotIdee;

    /**
     * @var float
     *
     * @ORM\Column(name="somme_recolte_idee", type="float", precision=10, scale=0, nullable=false)
     */
    private $sommeRecolteIdee;

    /**
     * @var float
     *
     * @ORM\Column(name="note_idee", type="float", precision=10, scale=0, nullable=false)
     */
    private $noteIdee;

    /**
     * @var string
     *
     * @ORM\Column(name="URL_video", type="string", length=255, nullable=false)
     */
    private $urlVideo;

    /**
     * @var \Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_utilisateur", referencedColumnName="id_utilisateur")
     * })
     */
    private $idUtilisateur;



    /**
     * Get idIdee
     *
     * @return integer 
     */
    public function getIdIdee()
    {
        return $this->idIdee;
    }

    /**
     * Set intituleIdee
     *
     * @param string $intituleIdee
     * @return Idee
     */
    public function setIntituleIdee($intituleIdee)
    {
        $this->intituleIdee = $intituleIdee;

        return $this;
    }

    /**
     * Get intituleIdee
     *
     * @return string 
     */
    public function getIntituleIdee()
    {
        return $this->intituleIdee;
    }

    /**
     * Set descriptionIdee
     *
     * @param string $descriptionIdee
     * @return Idee
     */
    public function setDescriptionIdee($descriptionIdee)
    {
        $this->descriptionIdee = $descriptionIdee;

        return $this;
    }

    /**
     * Get descriptionIdee
     *
     * @return string 
     */
    public function getDescriptionIdee()
    {
        return $this->descriptionIdee;
    }

    /**
     * Set categorieIdee
     *
     * @param string $categorieIdee
     * @return Idee
     */
    public function setCategorieIdee($categorieIdee)
    {
        $this->categorieIdee = $categorieIdee;

        return $this;
    }

    /**
     * Get categorieIdee
     *
     * @return string 
     */
    public function getCategorieIdee()
    {
        return $this->categorieIdee;
    }

    /**
     * Set etatIdee
     *
     * @param string $etatIdee
     * @return Idee
     */
    public function setEtatIdee($etatIdee)
    {
        $this->etatIdee = $etatIdee;

        return $this;
    }

    /**
     * Get etatIdee
     *
     * @return string 
     */
    public function getEtatIdee()
    {
        return $this->etatIdee;
    }

    /**
     * Set dateDepotIdee
     *
     * @param string $dateDepotIdee
     * @return Idee
     */
    public function setDateDepotIdee($dateDepotIdee)
    {
        $this->dateDepotIdee = $dateDepotIdee;

        return $this;
    }

    /**
     * Get dateDepotIdee
     *
     * @return string 
     */
    public function getDateDepotIdee()
    {
        return $this->dateDepotIdee;
    }

    /**
     * Set sommeRecolteIdee
     *
     * @param float $sommeRecolteIdee
     * @return Idee
     */
    public function setSommeRecolteIdee($sommeRecolteIdee)
    {
        $this->sommeRecolteIdee = $sommeRecolteIdee;

        return $this;
    }

    /**
     * Get sommeRecolteIdee
     *
     * @return float 
     */
    public function getSommeRecolteIdee()
    {
        return $this->sommeRecolteIdee;
    }

    /**
     * Set noteIdee
     *
     * @param float $noteIdee
     * @return Idee
     */
    public function setNoteIdee($noteIdee)
    {
        $this->noteIdee = $noteIdee;

        return $this;
    }

    /**
     * Get noteIdee
     *
     * @return float 
     */
    public function getNoteIdee()
    {
        return $this->noteIdee;
    }

    /**
     * Set urlVideo
     *
     * @param string $urlVideo
     * @return Idee
     */
    public function setUrlVideo($urlVideo)
    {
        $this->urlVideo = $urlVideo;

        return $this;
    }

    /**
     * Get urlVideo
     *
     * @return string 
     */
    public function getUrlVideo()
    {
        return $this->urlVideo;
    }

    /**
     * Set idUtilisateur
     *
     * @param \Crowdrise\AdministrationBundle\Entity\Utilisateur $idUtilisateur
     * @return Idee
     */
    public function setIdUtilisateur(\Crowdrise\AdministrationBundle\Entity\Utilisateur $idUtilisateur = null)
    {
        $this->idUtilisateur = $idUtilisateur;

        return $this;
    }

    /**
     * Get idUtilisateur
     *
     * @return \Crowdrise\AdministrationBundle\Entity\Utilisateur 
     */
    public function getIdUtilisateur()
    {
        return $this->idUtilisateur;
    }
}
