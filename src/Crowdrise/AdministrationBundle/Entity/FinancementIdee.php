<?php

namespace Crowdrise\AdministrationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FinancementIdee
 *
 * @ORM\Table(name="financement_idee", indexes={@ORM\Index(name="fk_financement_idee_utilisateur", columns={"id_utilisateur"}), @ORM\Index(name="fk_financement_idee_idee", columns={"id_idee"})})
 * @ORM\Entity
 */
class FinancementIdee
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_financement", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idFinancement;

    /**
     * @var float
     *
     * @ORM\Column(name="montant_fidee", type="float", precision=10, scale=0, nullable=false)
     */
    private $montantFidee;

    /**
     * @var string
     *
     * @ORM\Column(name="date_fidee", type="string", length=30, nullable=false)
     */
    private $dateFidee;

    /**
     * @var \Idee
     *
     * @ORM\ManyToOne(targetEntity="Idee")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_idee", referencedColumnName="id_idee")
     * })
     */
    private $idee;

    /**
     * @var \Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_utilisateur", referencedColumnName="id_utilisateur")
     * })
     */
    private $idUtilisateur;



    /**
     * Get idFinancement
     *
     * @return integer 
     */
    public function getIdFinancement()
    {
        return $this->idFinancement;
    }

    /**
     * Set montantFidee
     *
     * @param float $montantFidee
     * @return FinancementIdee
     */
    public function setMontantFidee($montantFidee)
    {
        $this->montantFidee = $montantFidee;

        return $this;
    }

    /**
     * Get montantFidee
     *
     * @return float 
     */
    public function getMontantFidee()
    {
        return $this->montantFidee;
    }

    /**
     * Set dateFidee
     *
     * @param string $dateFidee
     * @return FinancementIdee
     */
    public function setDateFidee($dateFidee)
    {
        $this->dateFidee = $dateFidee;

        return $this;
    }

    /**
     * Get dateFidee
     *
     * @return string 
     */
    public function getDateFidee()
    {
        return $this->dateFidee;
    }

    /**
     * Set idee
     *
     * @param \Crowdrise\AdministrationBundle\Entity\Idee $idee
     * @return FinancementIdee
     */
    public function setIdee(\Crowdrise\AdministrationBundle\Entity\Idee $idee = null)
    {
        $this->idee = $idee;

        return $this;
    }

    /**
     * Get idee
     *
     * @return \Crowdrise\AdministrationBundle\Entity\Idee 
     */
    public function getIdee()
    {
        return $this->idee;
    }

    /**
     * Set idUtilisateur
     *
     * @param \Crowdrise\AdministrationBundle\Entity\Utilisateur $idUtilisateur
     * @return FinancementIdee
     */
    public function setIdUtilisateur(\Crowdrise\AdministrationBundle\Entity\Utilisateur $idUtilisateur = null)
    {
        $this->idUtilisateur = $idUtilisateur;

        return $this;
    }

    /**
     * Get idUtilisateur
     *
     * @return \Crowdrise\AdministrationBundle\Entity\Utilisateur 
     */
    public function getIdUtilisateur()
    {
        return $this->idUtilisateur;
    }
}
