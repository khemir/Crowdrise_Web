<?php

namespace Crowdrise\AdministrationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Solution
 *
 * @ORM\Table(name="solution", indexes={@ORM\Index(name="fk_solution_utilisateur", columns={"id_utilisateur"}), @ORM\Index(name="fk_solution_probleme", columns={"id_probleme"})})
 * @ORM\Entity
 */
class Solution
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_solution", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idSolution;

    /**
     * @var string
     *
     * @ORM\Column(name="intitule_solution", type="string", length=30, nullable=false)
     */
    private $intituleSolution;

    /**
     * @var string
     *
     * @ORM\Column(name="date_solution", type="string", length=30, nullable=false)
     */
    private $dateSolution;

    /**
     * @var string
     *
     * @ORM\Column(name="contenu_solution", type="string", length=500, nullable=false)
     */
    private $contenuSolution;

    /**
     * @var \Probleme
     *
     * @ORM\ManyToOne(targetEntity="Probleme")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_probleme", referencedColumnName="id_probleme")
     * })
     */
    private $idProbleme;

    /**
     * @var \Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_utilisateur", referencedColumnName="id_utilisateur")
     * })
     */
    private $idUtilisateur;



    /**
     * Get idSolution
     *
     * @return integer 
     */
    public function getIdSolution()
    {
        return $this->idSolution;
    }

    /**
     * Set intituleSolution
     *
     * @param string $intituleSolution
     * @return Solution
     */
    public function setIntituleSolution($intituleSolution)
    {
        $this->intituleSolution = $intituleSolution;

        return $this;
    }

    /**
     * Get intituleSolution
     *
     * @return string 
     */
    public function getIntituleSolution()
    {
        return $this->intituleSolution;
    }

    /**
     * Set dateSolution
     *
     * @param string $dateSolution
     * @return Solution
     */
    public function setDateSolution($dateSolution)
    {
        $this->dateSolution = $dateSolution;

        return $this;
    }

    /**
     * Get dateSolution
     *
     * @return string 
     */
    public function getDateSolution()
    {
        return $this->dateSolution;
    }

    /**
     * Set contenuSolution
     *
     * @param string $contenuSolution
     * @return Solution
     */
    public function setContenuSolution($contenuSolution)
    {
        $this->contenuSolution = $contenuSolution;

        return $this;
    }

    /**
     * Get contenuSolution
     *
     * @return string 
     */
    public function getContenuSolution()
    {
        return $this->contenuSolution;
    }

    /**
     * Set idProbleme
     *
     * @param \Crowdrise\AdministrationBundle\Entity\Probleme $idProbleme
     * @return Solution
     */
    public function setIdProbleme(\Crowdrise\AdministrationBundle\Entity\Probleme $idProbleme = null)
    {
        $this->idProbleme = $idProbleme;

        return $this;
    }

    /**
     * Get idProbleme
     *
     * @return \Crowdrise\AdministrationBundle\Entity\Probleme 
     */
    public function getIdProbleme()
    {
        return $this->idProbleme;
    }

    /**
     * Set idUtilisateur
     *
     * @param \Crowdrise\AdministrationBundle\Entity\Utilisateur $idUtilisateur
     * @return Solution
     */
    public function setIdUtilisateur(\Crowdrise\AdministrationBundle\Entity\Utilisateur $idUtilisateur = null)
    {
        $this->idUtilisateur = $idUtilisateur;

        return $this;
    }

    /**
     * Get idUtilisateur
     *
     * @return \Crowdrise\AdministrationBundle\Entity\Utilisateur 
     */
    public function getIdUtilisateur()
    {
        return $this->idUtilisateur;
    }
}
