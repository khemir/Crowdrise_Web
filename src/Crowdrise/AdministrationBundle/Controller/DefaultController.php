<?php

namespace Crowdrise\AdministrationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Crowdrise\AdministrationBundle\Entity\Idee;
use Ob\HighchartsBundle\Highcharts\Highchart;

class DefaultController extends Controller
{
    
    
    public function administrationAction()
    {
       $em = $this->getDoctrine()->getManager();
       
       $idees = $em->getRepository("CrowdriseAdministrationBundle:Idee")->findBy(array('etatIdee' => 'En_Attente'));
       
       $reclamations = $em->getRepository("CrowdriseAdministrationBundle:Reclamation")->findAll();
     
       $nbU = $em->getRepository("CrowdriseAdministrationBundle:Utilisateur")->CountNBusers();
       
       $nbI = $em->getRepository("CrowdriseAdministrationBundle:Idee")->CountNBIdeas();
       
       $nbP = $em->getRepository("CrowdriseAdministrationBundle:Projet")->CountNBProjects();

       $nbC = $em->getRepository("CrowdriseAdministrationBundle:Reclamation")->CountNBClaims();
       
       $nbProb = $em->getRepository("CrowdriseAdministrationBundle:Probleme")->CountNBProblemes();


    // Pie Chart1
$ob = new Highchart();
$ob->chart->renderTo('piechart');
$ob->chart->type('pie');
$ob->title->text('Submitted Work Statistics');
$ob->plotOptions->series(
    array(
        'dataLabels' => array(
            'enabled' => true,
            'format' => '{point.name}: {point.y:.1f}%'
        )
    )
);

$ob->tooltip->headerFormat('<span style="font-size:11px">{series.name}</span><br>');
$ob->tooltip->pointFormat('<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>');

$totale=intval($nbI)+intval($nbP)+intval($nbProb);

$data = array(
    array(
        'name' => 'Ideas',
        'y' => (intval($nbI)*100)/$totale,
        'drilldown' => 'Ideas',
        'visible' => true
    ),
    array(
        'name' => 'Projects',
        'y' => (intval($nbP)*100)/$totale,
        'drilldown' => 'Projects',
        'visible' => true
    ),
    array('Problems', (intval($nbProb)*100)/$totale));

$ob->series(
    array(
        array(
            'name' => 'Number : ',
            'colorByPoint' => true,
            'data' => $data
        )
    )
);



  // Pie Chart2
$ob3 = new Highchart();
$ob3->chart->renderTo('piechart2');
$ob3->title->text('Ideas Categories Statistics');
$ob3->plotOptions->pie(array(
    'allowPointSelect'  => true,
    'cursor'    => 'pointer',
    'dataLabels'    => array('enabled' => false),
    'showInLegend'  => true
));

$data2 = array(
    
    array('Technology', count($em->getRepository("CrowdriseAdministrationBundle:Idee")->findBy(array('categorieIdee' => 'Technology')))),
    array('Arts', count($em->getRepository("CrowdriseAdministrationBundle:Idee")->findBy(array('categorieIdee' => 'Arts')))),
    array('Health', count($em->getRepository("CrowdriseAdministrationBundle:Idee")->findBy(array('categorieIdee' => 'Health')))),
    array('Donation', count($em->getRepository("CrowdriseAdministrationBundle:Idee")->findBy(array('categorieIdee' => 'Donation')))  ),
    array('Other', count($em->getRepository("CrowdriseAdministrationBundle:Idee")->findBy(array('categorieIdee' => 'Other')))));

$ob3->series(array(array('type' => 'pie','name' => 'Sum', 'data' => $data2)));

            ///
            



           
            //line charts
           
    $series = array(
        array("name" => "Data Serie Name",    "data" => array(1,2,4,5,6,3,8))
    );

    $ob2 = new Highchart();
    $ob2->chart->renderTo('linechart');  
    $ob2->title->text('Types of Works');
    $ob2->xAxis->title(array('text'  => "Type"));
    $ob2->yAxis->title(array('text'  => "Number"));
    $ob2->series($series);

///
   
        
       return $this->render('CrowdriseAdministrationBundle:Default:index.html.twig',array("idees"=>$idees,"reclamations"=>$reclamations,
           "nbUsers"=>$nbU,
           "nbIdeas"=>$nbI,
           "nbProjects"=>$nbP,
           "nbClaims"=>$nbC,
            "chart" => $ob,
            "chart2"=>$ob3,
           "chartline"=>$ob2));
    }
    

}
