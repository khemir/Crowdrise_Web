<?php

namespace Crowdrise\AdministrationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Crowdrise\AdministrationBundle\Entity\Utilisateur;
use Swift_Message;

class UtilisateurController extends Controller{
    
    public function affichageUtilisateursAction(){
        
        $em = $this->getDoctrine()->getManager();
        $utilisateurs = $em->getRepository("CrowdriseAdministrationBundle:Utilisateur")->findAll();
        
        return $this->render('CrowdriseAdministrationBundle:Default:users.html.twig', array("utilisateurs"=>$utilisateurs));
    }
    
    public function suppUtilisateurAction($id)
    {
         $em = $this->getDoctrine()->getManager();
         $utilisateur = $em->getRepository('CrowdriseAdministrationBundle:Utilisateur')->find($id);
         $em->remove($utilisateur);
         $em->flush();
         return $this->redirect($this->generateUrl('crowdrise_administration_users'));

    }
    
    public function blockUtilisateurAction($id)
    {
     
        $em = $this->getDoctrine()->getManager();
         
        $block = $em->getRepository("CrowdriseAdministrationBundle:Utilisateur")->blockUser($id);
        return $this->redirect($this->generateUrl('crowdrise_administration_users'));

    }
    
    public function reportUtilisateurAction($email)
    {
         
            $Subject = 'Crowdrise Administration';
            
            $message = 'Hello member , you have been reported by administrator.'
                            . 'If you have any questions or concerns please contact us.';
                        
            $mailer = $this->container->get('mailer');
            $transport = \Swift_SendmailTransport::newInstance('smtp.gmail.com',465,'ssl');
           
            $mailer = \Swift_Mailer::newInstance($transport);
            
            $message = \Swift_Message::newInstance('Test')
                    ->setSubject($Subject)
                    ->setFrom('crowdrise.adm@gmail.com')
                    ->setTo($email)
                    ->setBody($message);
           
            $this->get('mailer')->send($message);
        
        return $this->redirect($this->generateUrl('crowdrise_administration_users'));

    }
    
}
