<?php

namespace Crowdrise\AdministrationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Crowdrise\AdministrationBundle\Entity\Idee;

class IdeeController extends Controller{
    
    
    public function affichageIdeesAction(){
        
        $em = $this->getDoctrine()->getManager();
        $idees = $em->getRepository("CrowdriseAdministrationBundle:Idee")->findBy(array('etatIdee' => 'En_Attente'));
        
        return $this->render('CrowdriseAdministrationBundle:Default:ideas_requests.html.twig', array("idees"=>$idees));
    }
    
    public function affichageAllIdeesAction(){
        
        $em = $this->getDoctrine()->getManager();
        $idees = $em->getRepository("CrowdriseAdministrationBundle:Idee")->findAll();
        
        return $this->render('CrowdriseAdministrationBundle:Default:allideas.html.twig', array("idees"=>$idees));
    }
    
  
    public function detailsIdeaAction(){
        
        
        $id = $this->get('request')->get('id');
        $intitule = $this->get('request')->get('intitule');
        $description = $this->get('request')->get('description');
        $categorie = $this->get('request')->get('categorie');
        $date = $this->get('request')->get('date');
        $somme = $this->get('request')->get('somme');
        $note = $this->get('request')->get('note');
        
        $request = $this->getRequest();
        
        if ($request->getMethod() == "POST") {
            
            $somme = $request->get('somme');
      
           $em = $this->getDoctrine()->getManager();
         
        $accp = $em->getRepository("CrowdriseAdministrationBundle:Idee")->sponsorIdea($id,$somme); 
           

        }
        
        return $this->render('CrowdriseAdministrationBundle:Default:details_idea.html.twig', array("id" => $id, "intitule" => $intitule, "description" => $description, 
            "categorie" => $categorie ,"date" => $date ,"somme" => $somme,"note" => $note));
        
        
        }
    
    
   public function acceptIdeeAction()
    {
         $id = $this->get('request')->get('id');
         $email = $this->get('request')->get('email');
         
         $em = $this->getDoctrine()->getManager();
         
        $accp = $em->getRepository("CrowdriseAdministrationBundle:Idee")->AcceptIdea($id);
        
        $Subject = 'Crowdrise Administration';
            
            $message = 'Hello member , Congratulations ! your idea has been posted successfully.';
                        
            $mailer = $this->container->get('mailer');
            $transport = \Swift_SendmailTransport::newInstance('smtp.gmail.com',465,'ssl');
           
            $mailer = \Swift_Mailer::newInstance($transport);
            
            $message = \Swift_Message::newInstance('Test')
                    ->setSubject($Subject)
                    ->setFrom('crowdrise.adm@gmail.com')
                    ->setTo($email)
                    ->setBody($message);
           
            $this->get('mailer')->send($message);
        
        return $this->redirect($this->generateUrl('crowdrise_administration_ideas_requests'));
    }
    
    public function RefuseIdeaAction($id)
    {
         $em = $this->getDoctrine()->getManager();
         
        $accp = $em->getRepository("CrowdriseAdministrationBundle:Idee")->RefuseIdea($id);
        return $this->redirect($this->generateUrl('crowdrise_administration_ideas_requests'));
    }
}
