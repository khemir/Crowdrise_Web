<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FOS\UserBundle\Propel;

use FOS\UserBundle\Propel\om\BaseGroup;
use FOS\UserBundle\Model\GroupInterface;

class Group extends BaseGroup implements GroupInterface
{
    public function addRole($role) {
        
    }

    public function getIdUtilisateur() {
        
    }

    public function getName() {
        
    }

    public function getRoles() {
        
    }

    public function hasRole($role) {
        
    }

    public function removeRole($role) {
        
    }

    public function setName($name) {
        
    }

    public function setRoles(array $roles) {
        
    }

}
