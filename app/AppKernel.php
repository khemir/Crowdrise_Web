<?php



use Acme\DemoBundle\AcmeDemoBundle;
use Crowdrise\AdministrationBundle\CrowdriseAdministrationBundle;
use Crowdrise\IdeesBundle\CrowdriseIdeesBundle;
use Crowdrise\MailBundle\CrowdriseMailBundle;
use Crowdrise\MembreBundle\CrowdriseMembreBundle;
use Crowdrise\ProblemesBundle\CrowdriseProblemesBundle;
use Crowdrise\ProfileBundle\CrowdriseProfileBundle;
use Crowdrise\ProjetsBundle\CrowdriseProjetsBundle;
use Crowdrise\UserBundle\CrowdriseUserBundle;
use Doctrine\Bundle\DoctrineBundle\DoctrineBundle;
use FOS\UserBundle\FOSUserBundle;
use Ibrows\XeditableBundle\IbrowsXeditableBundle;

use Sensio\Bundle\DistributionBundle\SensioDistributionBundle;
use Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle;
use Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle;
use Symfony\Bundle\AsseticBundle\AsseticBundle;
use Symfony\Bundle\FrameworkBundle\FrameworkBundle;
use Symfony\Bundle\MonologBundle\MonologBundle;
use Symfony\Bundle\SecurityBundle\SecurityBundle;
use Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle;
use Symfony\Bundle\TwigBundle\TwigBundle;
use Symfony\Bundle\WebProfilerBundle\WebProfilerBundle;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\HttpKernel\Kernel;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            new FrameworkBundle(),
            new SecurityBundle(),
            new TwigBundle(),
            new MonologBundle(),
            new SwiftmailerBundle(),
            new AsseticBundle(),
            new DoctrineBundle(),
            new SensioFrameworkExtraBundle(),
            new CrowdriseAdministrationBundle(),
            new CrowdriseProjetsBundle(),
            new CrowdriseIdeesBundle(),
            new CrowdriseProfileBundle(),
            new CrowdriseProblemesBundle(),
            new CrowdriseMembreBundle(),
            new FOSUserBundle(),
            new CrowdriseUserBundle(),
            new CrowdriseMailBundle(),
            new IbrowsXeditableBundle(),
            new Ob\HighchartsBundle\ObHighchartsBundle(),            
        );

        if (in_array($this->getEnvironment(), array('dev', 'test'))) {
            $bundles[] = new AcmeDemoBundle();
            $bundles[] = new WebProfilerBundle();
            $bundles[] = new SensioDistributionBundle();
            $bundles[] = new SensioGeneratorBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(__DIR__.'/config/config_'.$this->getEnvironment().'.yml');
    }
}
