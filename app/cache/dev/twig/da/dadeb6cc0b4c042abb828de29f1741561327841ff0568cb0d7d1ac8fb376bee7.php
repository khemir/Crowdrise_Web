<?php

/* IbrowsXeditableBundle::xeditableform_delete.html.twig */
class __TwigTemplate_6f2907ad6be4972e64a6f1e9ab9ce9b8314fe22dfbc2a9654a46d5fe9bba9898 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("delete.confirm", array("%value" => $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars", array()), "data", array()))), "html", null, true);
    }

    public function getTemplateName()
    {
        return "IbrowsXeditableBundle::xeditableform_delete.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* {{ 'delete.confirm'|trans({'%value': form.vars.data}) }}*/
