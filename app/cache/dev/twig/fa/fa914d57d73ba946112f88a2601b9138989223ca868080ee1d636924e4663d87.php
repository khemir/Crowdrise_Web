<?php

/* CrowdriseUserBundle:Profile:list.html.twig */
class __TwigTemplate_a57ce39419ba095e7ad45f1e8d6c61268e47f0a4e9baea19835d3cef1013dbaa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<table border=\"2\">
    <tr> 
        <th> Id </th> 
       
        <th> Show picture </th> 
    </tr> 
    ";
        // line 7
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["images"]) ? $context["images"] : $this->getContext($context, "images")));
        foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
            echo " 
        <tr>
            <th>";
            // line 9
            echo twig_escape_filter($this->env, $this->getAttribute($context["image"], "id", array()), "html", null, true);
            echo "</th>
            
            <th> <a href=\"";
            // line 11
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("crowdrise_user_aff_article", array("id" => $this->getAttribute($context["image"], "id", array()))), "html", null, true);
            echo "\"> Show picture </a> </th> 
        </tr>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 14
        echo "</table>";
    }

    public function getTemplateName()
    {
        return "CrowdriseUserBundle:Profile:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  48 => 14,  39 => 11,  34 => 9,  27 => 7,  19 => 1,);
    }
}
/* <table border="2">*/
/*     <tr> */
/*         <th> Id </th> */
/*        */
/*         <th> Show picture </th> */
/*     </tr> */
/*     {% for image in images %} */
/*         <tr>*/
/*             <th>{{image.id}}</th>*/
/*             */
/*             <th> <a href="{{path("crowdrise_user_aff_article", {'id':image.id })}}"> Show picture </a> </th> */
/*         </tr>*/
/*     {% endfor %}*/
/* </table>*/
