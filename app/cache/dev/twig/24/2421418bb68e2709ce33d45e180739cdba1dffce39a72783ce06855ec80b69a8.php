<?php

/* CrowdriseAdministrationBundle::layout.html.twig */
class __TwigTemplate_21bea5bf6952665b8b5de50fb129aa2c09b227227a702e594f0a21af36c749e7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'navbar' => array($this, 'block_navbar'),
            'nav_li_dash' => array($this, 'block_nav_li_dash'),
            'nav_li_users' => array($this, 'block_nav_li_users'),
            'nav_li_ideasreq' => array($this, 'block_nav_li_ideasreq'),
            'nav_li_projects' => array($this, 'block_nav_li_projects'),
            'nav_li_claims' => array($this, 'block_nav_li_claims'),
            'nav_li_allideas' => array($this, 'block_nav_li_allideas'),
            'pagecontent' => array($this, 'block_pagecontent'),
            'pageheader' => array($this, 'block_pageheader'),
            'breadcrumb' => array($this, 'block_breadcrumb'),
            'nbusers' => array($this, 'block_nbusers'),
            'nbideas' => array($this, 'block_nbideas'),
            'nbprojects' => array($this, 'block_nbprojects'),
            'nbclaims' => array($this, 'block_nbclaims'),
            'recentidasactivity' => array($this, 'block_recentidasactivity'),
            'recentclaimsactivity' => array($this, 'block_recentclaimsactivity'),
            'stat1' => array($this, 'block_stat1'),
            'stat2' => array($this, 'block_stat2'),
            'stat3' => array($this, 'block_stat3'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
    
";
        // line 4
        $this->displayBlock('head', $context, $blocks);
        // line 57
        echo "
<body class=\"sidebar-wide\">


";
        // line 61
        $this->displayBlock('navbar', $context, $blocks);
        // line 79
        echo "


    
<!-- Page container -->
<div class=\"page-container\">
  <!-- Sidebar -->
  <div class=\"sidebar collapse\">
    <div class=\"sidebar-content\">




      <!-- User dropdown -->
      <div class=\"user-menu dropdown\"><a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\"><img src=\"";
        // line 93
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/images/demo/users/face1.png"), "html", null, true);
        echo "\" alt=\"\">
        <div class=\"user-info\">Oussama Aydi <span>Administrator</span></div>
        </a>


        <div class=\"popup dropdown-menu dropdown-menu-right\">
          <div class=\"thumbnail\">
            <div class=\"thumb\"><img alt=\"\" src=\"";
        // line 100
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/images/demo/users/face1.png"), "html", null, true);
        echo "\">
              <div class=\"thumb-options\"><span><a href=\"#\" class=\"btn btn-icon btn-success\"><i class=\"icon-pencil\"></i></a><a href=\"#\" class=\"btn btn-icon btn-success\"><i class=\"icon-remove\"></i></a></span></div>
            </div>
            <div class=\"caption text-center\">
              <h6>Oussama Aydi <small>Administrator</small></h6>
            </div>
          </div>
  
        </div>
      </div>
      <!-- /user dropdown -->



      <!-- Main navigation -->
      <ul class=\"navigation\">
          
       ";
        // line 117
        $this->displayBlock('nav_li_dash', $context, $blocks);
        // line 120
        echo "       
       ";
        // line 121
        $this->displayBlock('nav_li_users', $context, $blocks);
        // line 124
        echo "       
       ";
        // line 125
        $this->displayBlock('nav_li_ideasreq', $context, $blocks);
        // line 128
        echo "       
       ";
        // line 129
        $this->displayBlock('nav_li_projects', $context, $blocks);
        // line 132
        echo "       
       ";
        // line 133
        $this->displayBlock('nav_li_claims', $context, $blocks);
        // line 136
        echo "       
       ";
        // line 137
        $this->displayBlock('nav_li_allideas', $context, $blocks);
        // line 140
        echo "         
      </ul>
      <!-- /main navigation -->



    </div>
  </div>
  <!-- /sidebar -->



  
";
        // line 153
        $this->displayBlock('pagecontent', $context, $blocks);
        // line 328
        echo "  
  
</div>
<!-- /page container -->

    
</body>
</html>";
    }

    // line 4
    public function block_head($context, array $blocks = array())
    {
        // line 5
        echo "
<head>
<meta charset=\"utf-8\">
<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
<title>Crowdrise Admin</title>



<link href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\">
<link href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/css/londinium-theme.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\">
<link href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/css/styles.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\">
<link href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/css/icons.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\">


<link href=\"http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext\" rel=\"stylesheet\" type=\"text/css\">


<script type=\"text/javascript\" src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js\"></script>
<script type=\"text/javascript\" src=\"http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js\"></script>

<script type=\"text/javascript\" src=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/charts/sparkline.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/forms/uniform.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/forms/select2.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/forms/inputmask.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/forms/autosize.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/forms/inputlimit.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/forms/listbox.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/forms/multiselect.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/forms/validate.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/forms/tags.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/forms/switch.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 37
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/forms/uploader/plupload.full.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/forms/uploader/plupload.queue.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/forms/wysihtml5/wysihtml5.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/forms/wysihtml5/toolbar.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/interface/daterangepicker.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/interface/fancybox.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/interface/moment.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 44
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/interface/jgrowl.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 45
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/interface/datatables.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 46
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/interface/colorpicker.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 47
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/interface/fullcalendar.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 48
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/interface/timepicker.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 49
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/interface/collapsible.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 50
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 51
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/application.js"), "html", null, true);
        echo "\"></script>


</head>

";
    }

    // line 61
    public function block_navbar($context, array $blocks = array())
    {
        // line 62
        echo "
<!-- Navbar -->
<div class=\"navbar navbar-inverse\" role=\"navigation\">
  <div class=\"navbar-header\"><a class=\"navbar-brand\" href=\"";
        // line 65
        echo $this->env->getExtension('routing')->getPath("crowdrise_administration_homepage");
        echo "\"><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/images/logo.png"), "html", null, true);
        echo "\" alt=\"Londinium\"></a><a class=\"sidebar-toggle\"><i class=\"icon-paragraph-justify2\"></i></a>
    <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#navbar-icons\"><span class=\"sr-only\">Toggle navbar</span><i class=\"icon-grid3\"></i></button>
    <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".sidebar\"><span class=\"sr-only\">Toggle navigation</span><i class=\"icon-paragraph-justify2\"></i></button>
  </div>
  <ul class=\"nav navbar-nav navbar-right collapse\" id=\"navbar-icons\">

    <li><a href=\"#\"><i class=\"icon-exit\"></i> Logout</a>
    </li>
  </ul>
</div>


<!-- /navbar -->
";
    }

    // line 117
    public function block_nav_li_dash($context, array $blocks = array())
    {
        // line 118
        echo "        <li class=\"active\"><a href=\"";
        echo $this->env->getExtension('routing')->getPath("crowdrise_administration_homepage");
        echo "\"><span>Dashboard</span> <i class=\"icon-screen2\"></i></a></li>
       ";
    }

    // line 121
    public function block_nav_li_users($context, array $blocks = array())
    {
        // line 122
        echo "        <li><a href=\"";
        echo $this->env->getExtension('routing')->getPath("crowdrise_administration_users");
        echo "\"><span>Users</span> <i class=\"icon-users2\"></i></a></li>
        ";
    }

    // line 125
    public function block_nav_li_ideasreq($context, array $blocks = array())
    {
        // line 126
        echo "       <li><a href=\"";
        echo $this->env->getExtension('routing')->getPath("crowdrise_administration_ideas_requests");
        echo "\"><span>Ideas Requests</span> <i class=\"icon-stack\"></i></a> </li>
       ";
    }

    // line 129
    public function block_nav_li_projects($context, array $blocks = array())
    {
        // line 130
        echo "       <li><a href=\"";
        echo $this->env->getExtension('routing')->getPath("crowdrise_administration_projects");
        echo "\"><span>Projects</span> <i class=\"icon-stackoverflow\"></i></a> </li>
       ";
    }

    // line 133
    public function block_nav_li_claims($context, array $blocks = array())
    {
        // line 134
        echo "        <li><a href=\"";
        echo $this->env->getExtension('routing')->getPath("crowdrise_administration_claims");
        echo "\"><span>Claims</span> <i class=\"icon-bubble6\"></i></a> </li>
       ";
    }

    // line 137
    public function block_nav_li_allideas($context, array $blocks = array())
    {
        // line 138
        echo "        <li><a href=\"";
        echo $this->env->getExtension('routing')->getPath("crowdrise_administration_all_ideas");
        echo "\"><span>All Ideas</span> <i class=\"icon-lamp2\"></i></a> </li>
       ";
    }

    // line 153
    public function block_pagecontent($context, array $blocks = array())
    {
        // line 154
        echo "  <!-- Page content -->
  <div class=\"page-content\">
      
   ";
        // line 157
        $this->displayBlock('pageheader', $context, $blocks);
        // line 173
        echo "
    
    ";
        // line 175
        $this->displayBlock('breadcrumb', $context, $blocks);
        // line 187
        echo "

     <!-- Page statistics -->
    <ul class=\"page-stats list-justified\">
      <li class=\"bg-primary\">
        <div class=\"page-stats-showcase\"> <span>No. of Users</span>
          ";
        // line 193
        $this->displayBlock('nbusers', $context, $blocks);
        // line 194
        echo "        </div>
        <div class=\"bar-default chart\">10,14,8,45,23,41,22,31,19,12, 28, 21, 24, 20</div>
      </li>


      <li class=\"bg-danger\">
        <div class=\"page-stats-showcase\"> <span>No. of Ideas</span>
          ";
        // line 201
        $this->displayBlock('nbideas', $context, $blocks);
        // line 202
        echo "        </div>
        <div class=\"bar-default chart\">10,14,8,45,23,41,22,31,19,12, 28, 21, 24, 20</div>
      </li>


      <li class=\"bg-success\">
        <div class=\"page-stats-showcase\"> <span>No. of Projects</span>
          ";
        // line 209
        $this->displayBlock('nbprojects', $context, $blocks);
        // line 210
        echo "        </div>
        <div class=\"bar-default chart\">10,14,8,45,23,41,22,31,19,12, 28, 21, 24, 20</div>
      </li>


      <li class=\"bg-info\">
        <div class=\"page-stats-showcase\"> <span>No. of Claims</span>
          ";
        // line 217
        $this->displayBlock('nbclaims', $context, $blocks);
        // line 218
        echo "        </div>
        <div class=\"bar-default chart\">10,14,8,45,23,41,22,31,19,12, 28, 21, 24, 20</div>
      </li>


    </ul>
    <!-- /page statistics -->

    
    
    <!-- Page tabs -->
    <div class=\"tabbable page-tabs\">
      <ul class=\"nav nav-tabs\">
        <li class=\"active\"><a href=\"#activity\" data-toggle=\"tab\"><i class=\"icon-file\"></i> Activity</a></li>
        <li><a href=\"#statistics\" data-toggle=\"tab\"><i class=\"icon-bars\"></i> Statistics</a></li>
      </ul>



      <div class=\"tab-content\">


        <!-- First tab -->


        <div class=\"tab-pane active fade in\" id=\"activity\">
 
          <!-- Recent activity -->
          <div class=\"block\">
            <h6 class=\"heading-hr\"><i class=\"icon-file\"></i> Recent Ideas activity</h6>


            <ul class=\"media-list\">

                ";
        // line 252
        $this->displayBlock('recentidasactivity', $context, $blocks);
        // line 261
        echo "

            </ul>
          </div>
                
          
          <div class=\"block\">
            <h6 class=\"heading-hr\"><i class=\"icon-file\"></i> Recent Claims activity</h6>


            <ul class=\"media-list\">

             ";
        // line 273
        $this->displayBlock('recentclaimsactivity', $context, $blocks);
        // line 283
        echo "
            </ul>
          </div>
        </div>
        <!-- /first tab -->
        
        
        
        <!-- Secong tab -->
        
        <div class=\"tab-pane active fade in\" id=\"statistics\">
            
            <div class=\"block\">
            <h6 class=\"heading-hr\"><i class=\"icon-pie\"></i> Types Statistics</h6>
            ";
        // line 297
        $this->displayBlock('stat1', $context, $blocks);
        // line 298
        echo "            </div>
            
            <div class=\"block\">
            <h6 class=\"heading-hr\"><i class=\"icon-stats\"></i> Ideas Categories Statistics</h6>
            ";
        // line 302
        $this->displayBlock('stat2', $context, $blocks);
        // line 303
        echo "            </div>
            
           
            <div class=\"block\">
            <h6 class=\"heading-hr\"><i class=\"icon-stats\"></i> Types Statistics</h6>
            ";
        // line 308
        $this->displayBlock('stat3', $context, $blocks);
        // line 309
        echo "            </div>
        </div>

        <!-- /Secong tab -->



      </div>
    </div>
    <!-- /page tabs -->
    <!-- Footer -->
    <div class=\"footer clearfix\">
      <div class=\"pull-left\">&copy; 2016. Crowdrise Admin by <a href=\"http://themeforest.net/user/Kopyov\">DevX Team</a></div>
      <div class=\"pull-right icons-group\"> <a href=\"#\"><i class=\"icon-screen2\"></i></a> <a href=\"#\"><i class=\"icon-balance\"></i></a> <a href=\"#\"><i class=\"icon-cog3\"></i></a> </div>
    </div>
    <!-- /footer -->
  </div>
  <!-- /page content -->
  ";
    }

    // line 157
    public function block_pageheader($context, array $blocks = array())
    {
        echo "   
       
    <!-- Page header -->
    <div class=\"page-header\">
      <div class=\"page-title\">
        <h3>Dashboard <small>Welcome Administrator. This is your managment space.</small></h3>
      </div>
      <div id=\"reportrange\" class=\"range\">
        <div class=\"visible-xs header-element-toggle\"><a class=\"btn btn-primary btn-icon\"><i class=\"icon-calendar\"></i></a></div>
        <div class=\"date-range\"></div>
        <span class=\"label label-danger\">9</span></div>
    </div>


    <!-- /page header -->
    ";
    }

    // line 175
    public function block_breadcrumb($context, array $blocks = array())
    {
        // line 176
        echo "    <!-- Breadcrumbs line -->
    <div class=\"breadcrumb-line\">
      <ul class=\"breadcrumb\">
        <li><a href=\"index.html\">Home</a></li>
        <li class=\"active\">Dashboard</li>
      </ul>
      <div class=\"visible-xs breadcrumb-toggle\"><a class=\"btn btn-link btn-lg btn-icon\" data-toggle=\"collapse\" data-target=\".breadcrumb-buttons\"><i class=\"icon-menu2\"></i></a></div>

    </div>
    <!-- /breadcrumbs line -->
    ";
    }

    // line 193
    public function block_nbusers($context, array $blocks = array())
    {
        echo "<h2>22</h2>";
    }

    // line 201
    public function block_nbideas($context, array $blocks = array())
    {
        echo "<h2>22</h2>";
    }

    // line 209
    public function block_nbprojects($context, array $blocks = array())
    {
        echo "<h2>22</h2>";
    }

    // line 217
    public function block_nbclaims($context, array $blocks = array())
    {
        echo "<h2>22</h2>";
    }

    // line 252
    public function block_recentidasactivity($context, array $blocks = array())
    {
        // line 253
        echo "                    
              <li class=\"media\"><a class=\"pull-left\" href=\"#\"><img class=\"media-object\" src=\"";
        // line 254
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/images/demo/users/face2.png"), "html", null, true);
        echo "\" alt=\"\"></a>
                <div class=\"media-body\">
                  <div class=\"clearfix\"><a href=\"#\" class=\"media-heading\">Mahmoud tabka</a><span class=\"media-notice\">January 10, 2016 / 10:20 pm</span></div>

                  Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.</div>
              </li>
                ";
    }

    // line 273
    public function block_recentclaimsactivity($context, array $blocks = array())
    {
        // line 274
        echo "
              <li class=\"media\"><a class=\"pull-left\" href=\"#\"><img class=\"media-object\" src=\"";
        // line 275
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/images/demo/users/face2.png"), "html", null, true);
        echo "\" alt=\"\"></a>
                <div class=\"media-body\">
                  <div class=\"clearfix\"><a href=\"#\" class=\"media-heading\">Mahmoud tabka</a><span class=\"media-notice\">January 10, 2016 / 10:20 pm</span></div>

                  Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.</div>
              </li>
          
             ";
    }

    // line 297
    public function block_stat1($context, array $blocks = array())
    {
        echo " ";
    }

    // line 302
    public function block_stat2($context, array $blocks = array())
    {
        echo " ";
    }

    // line 308
    public function block_stat3($context, array $blocks = array())
    {
        echo " ";
    }

    public function getTemplateName()
    {
        return "CrowdriseAdministrationBundle::layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  670 => 308,  664 => 302,  658 => 297,  646 => 275,  643 => 274,  640 => 273,  629 => 254,  626 => 253,  623 => 252,  617 => 217,  611 => 209,  605 => 201,  599 => 193,  585 => 176,  582 => 175,  561 => 157,  539 => 309,  537 => 308,  530 => 303,  528 => 302,  522 => 298,  520 => 297,  504 => 283,  502 => 273,  488 => 261,  486 => 252,  450 => 218,  448 => 217,  439 => 210,  437 => 209,  428 => 202,  426 => 201,  417 => 194,  415 => 193,  407 => 187,  405 => 175,  401 => 173,  399 => 157,  394 => 154,  391 => 153,  384 => 138,  381 => 137,  374 => 134,  371 => 133,  364 => 130,  361 => 129,  354 => 126,  351 => 125,  344 => 122,  341 => 121,  334 => 118,  331 => 117,  311 => 65,  306 => 62,  303 => 61,  293 => 51,  289 => 50,  285 => 49,  281 => 48,  277 => 47,  273 => 46,  269 => 45,  265 => 44,  261 => 43,  257 => 42,  253 => 41,  249 => 40,  245 => 39,  241 => 38,  237 => 37,  233 => 36,  229 => 35,  225 => 34,  221 => 33,  217 => 32,  213 => 31,  209 => 30,  205 => 29,  201 => 28,  197 => 27,  193 => 26,  181 => 17,  177 => 16,  173 => 15,  169 => 14,  158 => 5,  155 => 4,  144 => 328,  142 => 153,  127 => 140,  125 => 137,  122 => 136,  120 => 133,  117 => 132,  115 => 129,  112 => 128,  110 => 125,  107 => 124,  105 => 121,  102 => 120,  100 => 117,  80 => 100,  70 => 93,  54 => 79,  52 => 61,  46 => 57,  44 => 4,  39 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html lang="en">*/
/*     */
/* {% block head %}*/
/* */
/* <head>*/
/* <meta charset="utf-8">*/
/* <meta http-equiv="X-UA-Compatible" content="IE=edge">*/
/* <meta name="viewport" content="width=device-width, initial-scale=1">*/
/* <title>Crowdrise Admin</title>*/
/* */
/* */
/* */
/* <link href="{{asset('assets_admin/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">*/
/* <link href="{{asset('assets_admin/css/londinium-theme.min.css')}}" rel="stylesheet" type="text/css">*/
/* <link href="{{asset('assets_admin/css/styles.min.css')}}" rel="stylesheet" type="text/css">*/
/* <link href="{{asset('assets_admin/css/icons.min.css')}}" rel="stylesheet" type="text/css">*/
/* */
/* */
/* <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">*/
/* */
/* */
/* <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>*/
/* <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>*/
/* */
/* <script type="text/javascript" src="{{asset('assets_admin/js/plugins/charts/sparkline.min.js')}}"></script>*/
/* <script type="text/javascript" src="{{asset('assets_admin/js/plugins/forms/uniform.min.js')}}"></script>*/
/* <script type="text/javascript" src="{{asset('assets_admin/js/plugins/forms/select2.min.js')}}"></script>*/
/* <script type="text/javascript" src="{{asset('assets_admin/js/plugins/forms/inputmask.js')}}"></script>*/
/* <script type="text/javascript" src="{{asset('assets_admin/js/plugins/forms/autosize.js')}}"></script>*/
/* <script type="text/javascript" src="{{asset('assets_admin/js/plugins/forms/inputlimit.min.js')}}"></script>*/
/* <script type="text/javascript" src="{{asset('assets_admin/js/plugins/forms/listbox.js')}}"></script>*/
/* <script type="text/javascript" src="{{asset('assets_admin/js/plugins/forms/multiselect.js')}}"></script>*/
/* <script type="text/javascript" src="{{asset('assets_admin/js/plugins/forms/validate.min.js')}}"></script>*/
/* <script type="text/javascript" src="{{asset('assets_admin/js/plugins/forms/tags.min.js')}}"></script>*/
/* <script type="text/javascript" src="{{asset('assets_admin/js/plugins/forms/switch.min.js')}}"></script>*/
/* <script type="text/javascript" src="{{asset('assets_admin/js/plugins/forms/uploader/plupload.full.min.js')}}"></script>*/
/* <script type="text/javascript" src="{{asset('assets_admin/js/plugins/forms/uploader/plupload.queue.min.js')}}"></script>*/
/* <script type="text/javascript" src="{{asset('assets_admin/js/plugins/forms/wysihtml5/wysihtml5.min.js')}}"></script>*/
/* <script type="text/javascript" src="{{asset('assets_admin/js/plugins/forms/wysihtml5/toolbar.js')}}"></script>*/
/* <script type="text/javascript" src="{{asset('assets_admin/js/plugins/interface/daterangepicker.js')}}"></script>*/
/* <script type="text/javascript" src="{{asset('assets_admin/js/plugins/interface/fancybox.min.js')}}"></script>*/
/* <script type="text/javascript" src="{{asset('assets_admin/js/plugins/interface/moment.js')}}"></script>*/
/* <script type="text/javascript" src="{{asset('assets_admin/js/plugins/interface/jgrowl.min.js')}}"></script>*/
/* <script type="text/javascript" src="{{asset('assets_admin/js/plugins/interface/datatables.min.js')}}"></script>*/
/* <script type="text/javascript" src="{{asset('assets_admin/js/plugins/interface/colorpicker.js')}}"></script>*/
/* <script type="text/javascript" src="{{asset('assets_admin/js/plugins/interface/fullcalendar.min.js')}}"></script>*/
/* <script type="text/javascript" src="{{asset('assets_admin/js/plugins/interface/timepicker.min.js')}}"></script>*/
/* <script type="text/javascript" src="{{asset('assets_admin/js/plugins/interface/collapsible.min.js')}}"></script>*/
/* <script type="text/javascript" src="{{asset('assets_admin/js/bootstrap.min.js')}}"></script>*/
/* <script type="text/javascript" src="{{asset('assets_admin/js/application.js')}}"></script>*/
/* */
/* */
/* </head>*/
/* */
/* {% endblock %}*/
/* */
/* <body class="sidebar-wide">*/
/* */
/* */
/* {% block navbar %}*/
/* */
/* <!-- Navbar -->*/
/* <div class="navbar navbar-inverse" role="navigation">*/
/*   <div class="navbar-header"><a class="navbar-brand" href="{{path('crowdrise_administration_homepage')}}"><img src="{{asset('assets_admin/images/logo.png')}}" alt="Londinium"></a><a class="sidebar-toggle"><i class="icon-paragraph-justify2"></i></a>*/
/*     <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-icons"><span class="sr-only">Toggle navbar</span><i class="icon-grid3"></i></button>*/
/*     <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar"><span class="sr-only">Toggle navigation</span><i class="icon-paragraph-justify2"></i></button>*/
/*   </div>*/
/*   <ul class="nav navbar-nav navbar-right collapse" id="navbar-icons">*/
/* */
/*     <li><a href="#"><i class="icon-exit"></i> Logout</a>*/
/*     </li>*/
/*   </ul>*/
/* </div>*/
/* */
/* */
/* <!-- /navbar -->*/
/* {% endblock %}*/
/* */
/* */
/* */
/*     */
/* <!-- Page container -->*/
/* <div class="page-container">*/
/*   <!-- Sidebar -->*/
/*   <div class="sidebar collapse">*/
/*     <div class="sidebar-content">*/
/* */
/* */
/* */
/* */
/*       <!-- User dropdown -->*/
/*       <div class="user-menu dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="{{asset('assets_admin/images/demo/users/face1.png')}}" alt="">*/
/*         <div class="user-info">Oussama Aydi <span>Administrator</span></div>*/
/*         </a>*/
/* */
/* */
/*         <div class="popup dropdown-menu dropdown-menu-right">*/
/*           <div class="thumbnail">*/
/*             <div class="thumb"><img alt="" src="{{asset('assets_admin/images/demo/users/face1.png')}}">*/
/*               <div class="thumb-options"><span><a href="#" class="btn btn-icon btn-success"><i class="icon-pencil"></i></a><a href="#" class="btn btn-icon btn-success"><i class="icon-remove"></i></a></span></div>*/
/*             </div>*/
/*             <div class="caption text-center">*/
/*               <h6>Oussama Aydi <small>Administrator</small></h6>*/
/*             </div>*/
/*           </div>*/
/*   */
/*         </div>*/
/*       </div>*/
/*       <!-- /user dropdown -->*/
/* */
/* */
/* */
/*       <!-- Main navigation -->*/
/*       <ul class="navigation">*/
/*           */
/*        {% block nav_li_dash %}*/
/*         <li class="active"><a href="{{path('crowdrise_administration_homepage')}}"><span>Dashboard</span> <i class="icon-screen2"></i></a></li>*/
/*        {% endblock %}*/
/*        */
/*        {% block nav_li_users %}*/
/*         <li><a href="{{path('crowdrise_administration_users')}}"><span>Users</span> <i class="icon-users2"></i></a></li>*/
/*         {% endblock %}*/
/*        */
/*        {% block nav_li_ideasreq %}*/
/*        <li><a href="{{path('crowdrise_administration_ideas_requests')}}"><span>Ideas Requests</span> <i class="icon-stack"></i></a> </li>*/
/*        {% endblock %}*/
/*        */
/*        {% block nav_li_projects %}*/
/*        <li><a href="{{path('crowdrise_administration_projects')}}"><span>Projects</span> <i class="icon-stackoverflow"></i></a> </li>*/
/*        {% endblock %}*/
/*        */
/*        {% block nav_li_claims %}*/
/*         <li><a href="{{path('crowdrise_administration_claims')}}"><span>Claims</span> <i class="icon-bubble6"></i></a> </li>*/
/*        {% endblock %}*/
/*        */
/*        {% block nav_li_allideas %}*/
/*         <li><a href="{{path('crowdrise_administration_all_ideas')}}"><span>All Ideas</span> <i class="icon-lamp2"></i></a> </li>*/
/*        {% endblock %}*/
/*          */
/*       </ul>*/
/*       <!-- /main navigation -->*/
/* */
/* */
/* */
/*     </div>*/
/*   </div>*/
/*   <!-- /sidebar -->*/
/* */
/* */
/* */
/*   */
/* {% block pagecontent %}*/
/*   <!-- Page content -->*/
/*   <div class="page-content">*/
/*       */
/*    {% block pageheader %}   */
/*        */
/*     <!-- Page header -->*/
/*     <div class="page-header">*/
/*       <div class="page-title">*/
/*         <h3>Dashboard <small>Welcome Administrator. This is your managment space.</small></h3>*/
/*       </div>*/
/*       <div id="reportrange" class="range">*/
/*         <div class="visible-xs header-element-toggle"><a class="btn btn-primary btn-icon"><i class="icon-calendar"></i></a></div>*/
/*         <div class="date-range"></div>*/
/*         <span class="label label-danger">9</span></div>*/
/*     </div>*/
/* */
/* */
/*     <!-- /page header -->*/
/*     {% endblock %}*/
/* */
/*     */
/*     {% block breadcrumb %}*/
/*     <!-- Breadcrumbs line -->*/
/*     <div class="breadcrumb-line">*/
/*       <ul class="breadcrumb">*/
/*         <li><a href="index.html">Home</a></li>*/
/*         <li class="active">Dashboard</li>*/
/*       </ul>*/
/*       <div class="visible-xs breadcrumb-toggle"><a class="btn btn-link btn-lg btn-icon" data-toggle="collapse" data-target=".breadcrumb-buttons"><i class="icon-menu2"></i></a></div>*/
/* */
/*     </div>*/
/*     <!-- /breadcrumbs line -->*/
/*     {% endblock %}*/
/* */
/* */
/*      <!-- Page statistics -->*/
/*     <ul class="page-stats list-justified">*/
/*       <li class="bg-primary">*/
/*         <div class="page-stats-showcase"> <span>No. of Users</span>*/
/*           {% block nbusers %}<h2>22</h2>{% endblock %}*/
/*         </div>*/
/*         <div class="bar-default chart">10,14,8,45,23,41,22,31,19,12, 28, 21, 24, 20</div>*/
/*       </li>*/
/* */
/* */
/*       <li class="bg-danger">*/
/*         <div class="page-stats-showcase"> <span>No. of Ideas</span>*/
/*           {% block nbideas %}<h2>22</h2>{% endblock %}*/
/*         </div>*/
/*         <div class="bar-default chart">10,14,8,45,23,41,22,31,19,12, 28, 21, 24, 20</div>*/
/*       </li>*/
/* */
/* */
/*       <li class="bg-success">*/
/*         <div class="page-stats-showcase"> <span>No. of Projects</span>*/
/*           {% block nbprojects %}<h2>22</h2>{% endblock %}*/
/*         </div>*/
/*         <div class="bar-default chart">10,14,8,45,23,41,22,31,19,12, 28, 21, 24, 20</div>*/
/*       </li>*/
/* */
/* */
/*       <li class="bg-info">*/
/*         <div class="page-stats-showcase"> <span>No. of Claims</span>*/
/*           {% block nbclaims %}<h2>22</h2>{% endblock %}*/
/*         </div>*/
/*         <div class="bar-default chart">10,14,8,45,23,41,22,31,19,12, 28, 21, 24, 20</div>*/
/*       </li>*/
/* */
/* */
/*     </ul>*/
/*     <!-- /page statistics -->*/
/* */
/*     */
/*     */
/*     <!-- Page tabs -->*/
/*     <div class="tabbable page-tabs">*/
/*       <ul class="nav nav-tabs">*/
/*         <li class="active"><a href="#activity" data-toggle="tab"><i class="icon-file"></i> Activity</a></li>*/
/*         <li><a href="#statistics" data-toggle="tab"><i class="icon-bars"></i> Statistics</a></li>*/
/*       </ul>*/
/* */
/* */
/* */
/*       <div class="tab-content">*/
/* */
/* */
/*         <!-- First tab -->*/
/* */
/* */
/*         <div class="tab-pane active fade in" id="activity">*/
/*  */
/*           <!-- Recent activity -->*/
/*           <div class="block">*/
/*             <h6 class="heading-hr"><i class="icon-file"></i> Recent Ideas activity</h6>*/
/* */
/* */
/*             <ul class="media-list">*/
/* */
/*                 {% block recentidasactivity %}*/
/*                     */
/*               <li class="media"><a class="pull-left" href="#"><img class="media-object" src="{{asset('assets_admin/images/demo/users/face2.png')}}" alt=""></a>*/
/*                 <div class="media-body">*/
/*                   <div class="clearfix"><a href="#" class="media-heading">Mahmoud tabka</a><span class="media-notice">January 10, 2016 / 10:20 pm</span></div>*/
/* */
/*                   Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.</div>*/
/*               </li>*/
/*                 {% endblock %}*/
/* */
/* */
/*             </ul>*/
/*           </div>*/
/*                 */
/*           */
/*           <div class="block">*/
/*             <h6 class="heading-hr"><i class="icon-file"></i> Recent Claims activity</h6>*/
/* */
/* */
/*             <ul class="media-list">*/
/* */
/*              {% block recentclaimsactivity %}*/
/* */
/*               <li class="media"><a class="pull-left" href="#"><img class="media-object" src="{{asset('assets_admin/images/demo/users/face2.png')}}" alt=""></a>*/
/*                 <div class="media-body">*/
/*                   <div class="clearfix"><a href="#" class="media-heading">Mahmoud tabka</a><span class="media-notice">January 10, 2016 / 10:20 pm</span></div>*/
/* */
/*                   Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.</div>*/
/*               </li>*/
/*           */
/*              {% endblock %}*/
/* */
/*             </ul>*/
/*           </div>*/
/*         </div>*/
/*         <!-- /first tab -->*/
/*         */
/*         */
/*         */
/*         <!-- Secong tab -->*/
/*         */
/*         <div class="tab-pane active fade in" id="statistics">*/
/*             */
/*             <div class="block">*/
/*             <h6 class="heading-hr"><i class="icon-pie"></i> Types Statistics</h6>*/
/*             {% block stat1 %} {% endblock %}*/
/*             </div>*/
/*             */
/*             <div class="block">*/
/*             <h6 class="heading-hr"><i class="icon-stats"></i> Ideas Categories Statistics</h6>*/
/*             {% block stat2 %} {% endblock %}*/
/*             </div>*/
/*             */
/*            */
/*             <div class="block">*/
/*             <h6 class="heading-hr"><i class="icon-stats"></i> Types Statistics</h6>*/
/*             {% block stat3 %} {% endblock %}*/
/*             </div>*/
/*         </div>*/
/* */
/*         <!-- /Secong tab -->*/
/* */
/* */
/* */
/*       </div>*/
/*     </div>*/
/*     <!-- /page tabs -->*/
/*     <!-- Footer -->*/
/*     <div class="footer clearfix">*/
/*       <div class="pull-left">&copy; 2016. Crowdrise Admin by <a href="http://themeforest.net/user/Kopyov">DevX Team</a></div>*/
/*       <div class="pull-right icons-group"> <a href="#"><i class="icon-screen2"></i></a> <a href="#"><i class="icon-balance"></i></a> <a href="#"><i class="icon-cog3"></i></a> </div>*/
/*     </div>*/
/*     <!-- /footer -->*/
/*   </div>*/
/*   <!-- /page content -->*/
/*   {% endblock %}*/
/*   */
/*   */
/* </div>*/
/* <!-- /page container -->*/
/* */
/*     */
/* </body>*/
/* </html>*/
