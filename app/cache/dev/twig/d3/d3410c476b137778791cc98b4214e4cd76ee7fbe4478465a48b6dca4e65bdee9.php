<?php

/* CrowdriseAdministrationBundle:Default:details_idea.html.twig */
class __TwigTemplate_586ec5e6d311ac91449d0d56a7ae84fd74c5ca8acd5c024ba78803206fff52c0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("CrowdriseAdministrationBundle::layout.html.twig", "CrowdriseAdministrationBundle:Default:details_idea.html.twig", 1);
        $this->blocks = array(
            'nav_li_dash' => array($this, 'block_nav_li_dash'),
            'nav_li_allideas' => array($this, 'block_nav_li_allideas'),
            'pagecontent' => array($this, 'block_pagecontent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "CrowdriseAdministrationBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_nav_li_dash($context, array $blocks = array())
    {
        // line 4
        echo "<li><a href=\"";
        echo $this->env->getExtension('routing')->getPath("crowdrise_administration_homepage");
        echo "\"><span>Dashboard</span> <i class=\"icon-screen2\"></i></a></li>
";
    }

    // line 7
    public function block_nav_li_allideas($context, array $blocks = array())
    {
        // line 8
        echo "<li class=\"active\"><a href=\"";
        echo $this->env->getExtension('routing')->getPath("crowdrise_administration_all_ideas");
        echo "\"><span>All Ideas</span> <i class=\"icon-lamp2\"></i></a> </li>
";
    }

    // line 12
    public function block_pagecontent($context, array $blocks = array())
    {
        // line 13
        echo "
    <!-- Page content -->
  <div class=\"page-content\">
    <!-- Page header -->
    <div class=\"page-header\">
      <div class=\"page-title\">
        <h3>Details Idea <small>Managment</small></h3>
      </div>

      <div id=\"reportrange\" class=\"range\">
        <div class=\"visible-xs header-element-toggle\"><a class=\"btn btn-primary btn-icon\"><i class=\"icon-calendar\"></i></a></div>
        <div class=\"date-range\"></div>
        <span class=\"label label-danger\">9</span></div>

    </div>
    <!-- /page header -->
    <!-- Breadcrumbs line -->
    <div class=\"breadcrumb-line\">
      <ul class=\"breadcrumb\">
        <li><a href=\"index.html\">All Ideas</a></li>
        <li class=\"active\">Details</li>
      </ul>
      <div class=\"visible-xs breadcrumb-toggle\"><a class=\"btn btn-link btn-lg btn-icon\" data-toggle=\"collapse\" data-target=\".breadcrumb-buttons\"><i class=\"icon-menu2\"></i></a></div>
     

    </div>
    <!-- /breadcrumbs line -->
    <!-- Page tabs -->
    <div class=\"tabbable page-tabs\">
   
      <div class=\"tab-content\">
        <!-- First tab -->

         <div class=\"callout callout-danger fade in\">
      <h5>";
        // line 47
        echo twig_escape_filter($this->env, (isset($context["categorie"]) ? $context["categorie"] : $this->getContext($context, "categorie")), "html", null, true);
        echo "</h5>           
    </div>


              <!-- Task -->
              <div class=\"block task task-high\">
                <div class=\"row with-padding\">
                  <div class=\"col-sm-9\">
                    <div class=\"task-description\"><a href=\"#\">";
        // line 55
        echo twig_escape_filter($this->env, (isset($context["intitule"]) ? $context["intitule"] : $this->getContext($context, "intitule")), "html", null, true);
        echo "</a><span>";
        echo twig_escape_filter($this->env, (isset($context["description"]) ? $context["description"] : $this->getContext($context, "description")), "html", null, true);
        echo "</span></div>
                  </div>
                  <div class=\"col-sm-3\">
                    <div class=\"task-info text-right\"><span>";
        // line 58
        echo twig_escape_filter($this->env, (isset($context["date"]) ? $context["date"] : $this->getContext($context, "date")), "html", null, true);
        echo "</span><span><span class=\"label label-danger\">";
        echo twig_escape_filter($this->env, (isset($context["somme"]) ? $context["somme"] : $this->getContext($context, "somme")), "html", null, true);
        echo " \$ </span></span>
                    
                           <a href=\"#\"><i class=\"icon-thumbs-up2\"></i> <span>Rating</span> <strong class=\"label label-success\">";
        // line 60
        echo twig_escape_filter($this->env, (isset($context["note"]) ? $context["note"] : $this->getContext($context, "note")), "html", null, true);
        echo "</strong></a></div>
                  </div>
                </div>
              
              </div>


            
                
        <!-- /fifth tab -->

    <!-- Add comment form -->
        <div class=\"block\">
          <h6><i class=\"icon-bubble-plus\"></i> Sponsoring</h6>
          <div class=\"well\">

            <form method=\"POST\">
              <div class=\"form-group\">
                <div class=\"row\">
                  <div class=\"col-md-6\">
                    <label>Your sum</label>
                          <div class=\"input-group\"><span class=\"input-group-addon\">\$</span>
                <input type=\"number\" name=\"somme\" class=\"form-control\">
                <span class=\"input-group-addon\">.00</span></div>
                  </div>
               
                </div>
              </div>
            
              <div class=\"form-actions text-right\">
                <input type=\"reset\" value=\"Cancel\" class=\"btn btn-danger\">
                <input type=\"submit\" value=\"Donate\" class=\"btn btn-primary\">
              </div>
            </form>
          </div>
        </div>
        <!-- /add comment form -->

        
        <!-- /first tab -->

      </div>
    </div>
    <!-- /page tabs -->
    <!-- Footer -->
    <div class=\"footer clearfix\">
      <div class=\"pull-left\">&copy; 2016. Crowdrise Admin by <a href=\"http://themeforest.net/user/Kopyov\">DevX Team</a></div>
      <div class=\"pull-right icons-group\"> <a href=\"#\"><i class=\"icon-screen2\"></i></a> <a href=\"#\"><i class=\"icon-balance\"></i></a> <a href=\"#\"><i class=\"icon-cog3\"></i></a> </div>
    </div>
    <!-- /footer -->
  </div>
  <!-- /page content -->
";
    }

    public function getTemplateName()
    {
        return "CrowdriseAdministrationBundle:Default:details_idea.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  115 => 60,  108 => 58,  100 => 55,  89 => 47,  53 => 13,  50 => 12,  43 => 8,  40 => 7,  33 => 4,  30 => 3,  11 => 1,);
    }
}
/* {% extends "CrowdriseAdministrationBundle::layout.html.twig" %}*/
/* */
/* {% block nav_li_dash %}*/
/* <li><a href="{{path('crowdrise_administration_homepage')}}"><span>Dashboard</span> <i class="icon-screen2"></i></a></li>*/
/* {% endblock %}*/
/* */
/* {% block nav_li_allideas %}*/
/* <li class="active"><a href="{{path('crowdrise_administration_all_ideas')}}"><span>All Ideas</span> <i class="icon-lamp2"></i></a> </li>*/
/* {% endblock %}*/
/* */
/* */
/* {% block pagecontent %}*/
/* */
/*     <!-- Page content -->*/
/*   <div class="page-content">*/
/*     <!-- Page header -->*/
/*     <div class="page-header">*/
/*       <div class="page-title">*/
/*         <h3>Details Idea <small>Managment</small></h3>*/
/*       </div>*/
/* */
/*       <div id="reportrange" class="range">*/
/*         <div class="visible-xs header-element-toggle"><a class="btn btn-primary btn-icon"><i class="icon-calendar"></i></a></div>*/
/*         <div class="date-range"></div>*/
/*         <span class="label label-danger">9</span></div>*/
/* */
/*     </div>*/
/*     <!-- /page header -->*/
/*     <!-- Breadcrumbs line -->*/
/*     <div class="breadcrumb-line">*/
/*       <ul class="breadcrumb">*/
/*         <li><a href="index.html">All Ideas</a></li>*/
/*         <li class="active">Details</li>*/
/*       </ul>*/
/*       <div class="visible-xs breadcrumb-toggle"><a class="btn btn-link btn-lg btn-icon" data-toggle="collapse" data-target=".breadcrumb-buttons"><i class="icon-menu2"></i></a></div>*/
/*      */
/* */
/*     </div>*/
/*     <!-- /breadcrumbs line -->*/
/*     <!-- Page tabs -->*/
/*     <div class="tabbable page-tabs">*/
/*    */
/*       <div class="tab-content">*/
/*         <!-- First tab -->*/
/* */
/*          <div class="callout callout-danger fade in">*/
/*       <h5>{{categorie}}</h5>           */
/*     </div>*/
/* */
/* */
/*               <!-- Task -->*/
/*               <div class="block task task-high">*/
/*                 <div class="row with-padding">*/
/*                   <div class="col-sm-9">*/
/*                     <div class="task-description"><a href="#">{{intitule}}</a><span>{{description}}</span></div>*/
/*                   </div>*/
/*                   <div class="col-sm-3">*/
/*                     <div class="task-info text-right"><span>{{date}}</span><span><span class="label label-danger">{{somme}} $ </span></span>*/
/*                     */
/*                            <a href="#"><i class="icon-thumbs-up2"></i> <span>Rating</span> <strong class="label label-success">{{note}}</strong></a></div>*/
/*                   </div>*/
/*                 </div>*/
/*               */
/*               </div>*/
/* */
/* */
/*             */
/*                 */
/*         <!-- /fifth tab -->*/
/* */
/*     <!-- Add comment form -->*/
/*         <div class="block">*/
/*           <h6><i class="icon-bubble-plus"></i> Sponsoring</h6>*/
/*           <div class="well">*/
/* */
/*             <form method="POST">*/
/*               <div class="form-group">*/
/*                 <div class="row">*/
/*                   <div class="col-md-6">*/
/*                     <label>Your sum</label>*/
/*                           <div class="input-group"><span class="input-group-addon">$</span>*/
/*                 <input type="number" name="somme" class="form-control">*/
/*                 <span class="input-group-addon">.00</span></div>*/
/*                   </div>*/
/*                */
/*                 </div>*/
/*               </div>*/
/*             */
/*               <div class="form-actions text-right">*/
/*                 <input type="reset" value="Cancel" class="btn btn-danger">*/
/*                 <input type="submit" value="Donate" class="btn btn-primary">*/
/*               </div>*/
/*             </form>*/
/*           </div>*/
/*         </div>*/
/*         <!-- /add comment form -->*/
/* */
/*         */
/*         <!-- /first tab -->*/
/* */
/*       </div>*/
/*     </div>*/
/*     <!-- /page tabs -->*/
/*     <!-- Footer -->*/
/*     <div class="footer clearfix">*/
/*       <div class="pull-left">&copy; 2016. Crowdrise Admin by <a href="http://themeforest.net/user/Kopyov">DevX Team</a></div>*/
/*       <div class="pull-right icons-group"> <a href="#"><i class="icon-screen2"></i></a> <a href="#"><i class="icon-balance"></i></a> <a href="#"><i class="icon-cog3"></i></a> </div>*/
/*     </div>*/
/*     <!-- /footer -->*/
/*   </div>*/
/*   <!-- /page content -->*/
/* {% endblock %}*/
/* */
