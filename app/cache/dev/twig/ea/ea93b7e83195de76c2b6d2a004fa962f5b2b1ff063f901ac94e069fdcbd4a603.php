<?php

/* CrowdriseUserBundle:Profile:upload.html.twig */
class __TwigTemplate_c819e5866ac9ae8e505b91e0bb320d458089980dd7c0c5454d4889c33217543c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<h1>Upload File</h1> 
<form action=\"#\" method=\"post\" ";
        // line 2
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["Form"]) ? $context["Form"] : $this->getContext($context, "Form")), 'enctype');
        echo ">
    ";
        // line 3
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["Form"]) ? $context["Form"] : $this->getContext($context, "Form")), 'widget');
        echo "
    <input type=\"submit\" value=\"Upload Document\" /> 
</form>";
    }

    public function getTemplateName()
    {
        return "CrowdriseUserBundle:Profile:upload.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  26 => 3,  22 => 2,  19 => 1,);
    }
}
/* <h1>Upload File</h1> */
/* <form action="#" method="post" {{ form_enctype(Form) }}>*/
/*     {{ form_widget(Form) }}*/
/*     <input type="submit" value="Upload Document" /> */
/* </form>*/
