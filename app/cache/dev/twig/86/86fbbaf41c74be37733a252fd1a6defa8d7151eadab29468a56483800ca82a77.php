<?php

/* CrowdriseMembreBundle:Default:register.html.twig */
class __TwigTemplate_51566f4b0fb8b7fc9e51f9777b391318106d11f6f9c8ca0216b0f47f4b2cd8c9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("CrowdriseMembreBundle::layout.html.twig", "CrowdriseMembreBundle:Default:register.html.twig", 1);
        $this->blocks = array(
            'container' => array($this, 'block_container'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "CrowdriseMembreBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_container($context, array $blocks = array())
    {
        // line 3
        echo "    
    <section>
\t\t\t<div id=\"page-wrapper\" class=\"sign-in-wrapper\">
\t\t\t\t<div class=\"graphs\">
\t\t\t\t\t<div class=\"sign-up\">
\t\t\t\t\t\t<h1>Create an account</h1>
\t\t\t\t\t\t<p class=\"creating\">Having hands on experience in creating innovative designs,I do offer design 
\t\t\t\t\t\t\tsolutions which harness.</p>
\t\t\t\t\t\t<h2>Personal Information</h2>
\t\t\t\t\t\t<div class=\"sign-u\">
\t\t\t\t\t\t\t<div class=\"sign-up1\">
\t\t\t\t\t\t\t\t<h4>Email Address* :</h4>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"sign-up2\">
\t\t\t\t\t\t\t\t<form>
\t\t\t\t\t\t\t\t\t<input type=\"text\" placeholder=\" \" required=\" \"/>
\t\t\t\t\t\t\t\t</form>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"clearfix\"> </div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"sign-u\">
\t\t\t\t\t\t\t<div class=\"sign-up1\">
\t\t\t\t\t\t\t\t<h4>Password* :</h4>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"sign-up2\">
\t\t\t\t\t\t\t\t<form>
\t\t\t\t\t\t\t\t\t<input type=\"password\" placeholder=\" \" required=\" \"/>
\t\t\t\t\t\t\t\t</form>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"clearfix\"> </div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"sign-u\">
\t\t\t\t\t\t\t<div class=\"sign-up1\">
\t\t\t\t\t\t\t\t<h4>Confirm Password* :</h4>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"sign-up2\">
\t\t\t\t\t\t\t\t<form>
\t\t\t\t\t\t\t\t\t<input type=\"password\" placeholder=\" \" required=\" \"/>
\t\t\t\t\t\t\t\t</form>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"clearfix\"> </div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"sub_home\">
\t\t\t\t\t\t\t<div class=\"sub_home_left\">
\t\t\t\t\t\t\t\t<form>
\t\t\t\t\t\t\t\t\t<input type=\"submit\" value=\"Create\">
\t\t\t\t\t\t\t\t</form>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"sub_home_right\">
\t\t\t\t\t\t\t\t<p>Go Back to <a href=\"index.html\">Home</a></p>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"clearfix\"> </div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>

\t</section>
";
    }

    public function getTemplateName()
    {
        return "CrowdriseMembreBundle:Default:register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends "CrowdriseMembreBundle::layout.html.twig" %}*/
/* {% block container %}*/
/*     */
/*     <section>*/
/* 			<div id="page-wrapper" class="sign-in-wrapper">*/
/* 				<div class="graphs">*/
/* 					<div class="sign-up">*/
/* 						<h1>Create an account</h1>*/
/* 						<p class="creating">Having hands on experience in creating innovative designs,I do offer design */
/* 							solutions which harness.</p>*/
/* 						<h2>Personal Information</h2>*/
/* 						<div class="sign-u">*/
/* 							<div class="sign-up1">*/
/* 								<h4>Email Address* :</h4>*/
/* 							</div>*/
/* 							<div class="sign-up2">*/
/* 								<form>*/
/* 									<input type="text" placeholder=" " required=" "/>*/
/* 								</form>*/
/* 							</div>*/
/* 							<div class="clearfix"> </div>*/
/* 						</div>*/
/* 						<div class="sign-u">*/
/* 							<div class="sign-up1">*/
/* 								<h4>Password* :</h4>*/
/* 							</div>*/
/* 							<div class="sign-up2">*/
/* 								<form>*/
/* 									<input type="password" placeholder=" " required=" "/>*/
/* 								</form>*/
/* 							</div>*/
/* 							<div class="clearfix"> </div>*/
/* 						</div>*/
/* 						<div class="sign-u">*/
/* 							<div class="sign-up1">*/
/* 								<h4>Confirm Password* :</h4>*/
/* 							</div>*/
/* 							<div class="sign-up2">*/
/* 								<form>*/
/* 									<input type="password" placeholder=" " required=" "/>*/
/* 								</form>*/
/* 							</div>*/
/* 							<div class="clearfix"> </div>*/
/* 						</div>*/
/* 						<div class="sub_home">*/
/* 							<div class="sub_home_left">*/
/* 								<form>*/
/* 									<input type="submit" value="Create">*/
/* 								</form>*/
/* 							</div>*/
/* 							<div class="sub_home_right">*/
/* 								<p>Go Back to <a href="index.html">Home</a></p>*/
/* 							</div>*/
/* 							<div class="clearfix"> </div>*/
/* 						</div>*/
/* 					</div>*/
/* 				</div>*/
/* 			</div>*/
/* */
/* 	</section>*/
/* {% endblock %}*/
