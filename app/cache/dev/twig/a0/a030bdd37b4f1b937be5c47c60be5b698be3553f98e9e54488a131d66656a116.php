<?php

/* FOSUserBundle:Profile:show_content.html.twig */
class __TwigTemplate_73beb5d3d00ec368cb649d83e9ec1a89f8b86f34382e4e354e806c25bd39e630 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "

   
";
        // line 4
        echo "       
<div class=\"fos_user_user_show\">
    <div class=\"container\">                       
            <img class=\"profile-image img-responsive pull-left\" src=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_profile/images/images.jpg"), "html", null, true);
        echo "\" />
            <div class=\"profile-content pull-left\">
                <center> <h1 class=\"name\"> ";
        // line 9
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array()), "html", null, true);
        echo "</h1></center>
                <center><h4 class=\"desc\"> ";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "email", array()), "html", null, true);
        echo "</h4> </center>  
                <center> <ul class=\"social list-inline\">
                    <li><a href=\"#\"><i class=\"fa fa-twitter\"></i></a></li>                   
                    <li><a href=\"#\"><i class=\"fa fa-google-plus\"></i></a></li>
                    <li><a href=\"#\"><i class=\"fa fa-linkedin\"></i></a></li>
                    <li><a href=\"#\"><i class=\"fa fa-github-alt\"></i></a></li>                  
                    <li class=\"last-item\"><a href=\"#\"><i class=\"fa fa-hacker-news\"></i></a></li>                 
                    </ul> </center>

            </div><!--//profile-->
         

    </div>
           
        
              <div class=\"secondary col-md-7  col-sm-12 col-xs-12\">
                  <section class=\"about section\">
                    <div class=\"section-inner\">
                        <h2 class=\"heading\">About Me</h2>
                        <div class=\"content\">
 
           
               <p>  <strong>";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("profile.show.username", array(), "FOSUserBundle"), "html", null, true);
        echo ":</strong> ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array()), "html", null, true);
        echo "<br>
                                <strong>";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("profile.show.email", array(), "FOSUserBundle"), "html", null, true);
        echo ":</strong> ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "email", array()), "html", null, true);
        echo "<br>
                                <strong> ";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Your forname", array(), "FOSUserBundle"), "html", null, true);
        echo ":</strong> ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "nomUtilisateur", array()), "html", null, true);
        echo "<br>
                            </p>
                         
                        </div><!--//content-->
                    </div><!--//section-inner-->                 
                </section><!--//section-->
                  <section class=\"experience section\">
                    <div class=\"section-inner\">
                        <h2 class=\"heading\">Work Experience</h2>
                        <div class=\"content\">
                            
        <strong> ";
        // line 45
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Experience", array(), "FOSUserBundle"), "html", null, true);
        echo ": </strong>";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "experience", array()), "html", null, true);
        echo "

                        </div><!--//content-->  
                    </div><!--//section-inner-->                 
                </section><!--//section-->
                 <aside class=\"info aside section\">
                    <div class=\"section-inner\">
                        <h2 class=\"heading sr-only\">Basic Information</h2>
                        <div class=\"content\">
                            <ul class=\"list-unstyled\">
                                <li><i class=\"fa fa-map-marker\"></i><span class=\"sr-only\">Location:</span>Tunis, Tunisia</li>
                                <li><i class=\"fa fa-envelope-o\"></i><span class=\"sr-only\">Email:</span><a href=\"#\">";
        // line 56
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "email", array()), "html", null, true);
        echo "</a></li>
                                <li><i class=\"fa fa-link\"></i><span class=\"sr-only\">Website:</span><a href=\"#\">http://www.website.com</a></li>
                            </ul>
                        </div><!--//content-->  
                    </div><!--//section-inner-->                 
                </aside><!--//aside-->
                
                <aside class=\"skills aside section\">
                    <div class=\"section-inner\">
                        <h2 class=\"heading\">Skills</h2>
                        <div class=\"content\">
                            <p class=\"intro\">
                                Intro about your skills goes here. Keep the list lean and only show your primary skillset. You can always provide a link to your Linkedin or Coderwall profile so people can get more info there.</p>
                            ";
        // line 69
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "competences", array()), "html", null, true);
        echo "
                                      
                        </div><!--//content-->  
                    </div><!--//section-inner-->                 
                </aside><!--//section-->
                
                
                
                      
                
                
                <aside class=\"list music aside section\">
                    <div class=\"section-inner\">
                        <h2 class=\"heading\">Favourite coding music</h2>
                        <div class=\"content\">
                            <ul class=\"list-unstyled\">                                
                                <li><i class=\"fa fa-headphones\"></i> <a href=\"http://www.jango.com/\">www.jango.com</a></li>
                            </ul>
                        </div><!--//content-->
                    </div><!--//section-inner-->
                </aside><!--//section-->
                
                
                
               
              
            </div>

    <div class=\"primary pull-right col-sm-offset-0 col-xs-offset-12\">
             <section class=\"latest section\">
                  
                            <hr class=\"divider\" />
                            <div class=\"item row\">
                                <a class=\"col-md-12 col-sm-12 col-xs-12\" href=\"http://themes.3rdwavemedia.com/website-templates/responsive-bootstrap-theme-mobile-apps-atom/\" target=\"_blank\">
                                <img class=\"img-responsive project-image\" src=\"";
        // line 103
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_profile/images/projects/project-5.png"), "html", null, true);
        echo "\" alt=\"project name\" />
                                </a>
                                <div class=\"desc col-md-8 col-sm-8 col-xs-12\">
                                    <h3 class=\"title\"><a href=\"http://themes.3rdwavemedia.com/website-templates/responsive-bootstrap-theme-mobile-apps-atom/\" target=\"_blank\">View Projetcs</a></h3>
                                    <p><a class=\"more-link\" href=\"http://themes.3rdwavemedia.com/website-templates/responsive-bootstrap-theme-mobile-apps-atom/\" target=\"_blank\"><i class=\"fa fa-external-link\"></i> Find out more</a></p>
                                </div><!--//desc-->                          
                            </div><!--//item-->
                             <div class=\"item row\">
                                <a class=\"col-md-12 col-sm-12 col-xs-12\" href=\"http://themes.3rdwavemedia.com/website-templates/responsive-bootstrap-theme-for-startups-tempo/\" target=\"_blank\">
                                <img class=\"img-responsive project-image\" src=\"";
        // line 112
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_profile/images/projects/project-1.png"), "html", null, true);
        echo "\" alt=\"project name\" />
                                </a>
                                <div class=\"desc col-md-8 col-sm-8 col-xs-12\">
                                    <h3 class=\"title\"><a href=\"http://themes.3rdwavemedia.com/website-templates/responsive-bootstrap-theme-for-startups-tempo/\" target=\"_blank\">View Ideas</a></h3>
                                    <p><a class=\"more-link\" href=\"http://themes.3rdwavemedia.com/website-templates/responsive-bootstrap-theme-for-startups-tempo/\" target=\"_blank\"><i class=\"fa fa-external-link\"></i> Find out more</a></p>
                                </div><!--//desc-->                          
                            </div><!--//item--> 
                            <div class=\"item row\">
                                <a class=\"col-md-12 col-sm-12 col-xs-12\" href=\"http://themes.3rdwavemedia.com/website-templates/responsive-bootstrap-theme-for-mobile-apps-delta/\" target=\"_blank\">
                                <img class=\"img-responsive project-image\" src=\"";
        // line 121
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_profile/images/projects/project-2.png"), "html", null, true);
        echo "\" alt=\"project name\" />
                                </a>
                                <div class=\"desc col-md-8 col-sm-8 col-xs-12\">
                                    <h3 class=\"title\"><a href=\"http://themes.3rdwavemedia.com/website-templates/responsive-bootstrap-theme-for-mobile-apps-delta/\" target=\"_blank\">View Forum</a></h3>
                                    <p><a class=\"more-link\" href=\"http://themes.3rdwavemedia.com/website-templates/responsive-bootstrap-theme-for-mobile-apps-delta/\" target=\"_blank\"><i class=\"fa fa-external-link\"></i> Find out more</a></p>
                                </div><!--//desc-->                          
                            </div><!--//item-->
                </section><!--//section-->
    </div>     
</div>";
    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Profile:show_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  184 => 121,  172 => 112,  160 => 103,  123 => 69,  107 => 56,  91 => 45,  75 => 34,  69 => 33,  63 => 32,  38 => 10,  34 => 9,  29 => 7,  24 => 4,  19 => 1,);
    }
}
/* */
/* */
/*    */
/* {% trans_default_domain 'FOSUserBundle' %}       */
/* <div class="fos_user_user_show">*/
/*     <div class="container">                       */
/*             <img class="profile-image img-responsive pull-left" src="{{asset('assets_profile/images/images.jpg')}}" />*/
/*             <div class="profile-content pull-left">*/
/*                 <center> <h1 class="name"> {{ user.username }}</h1></center>*/
/*                 <center><h4 class="desc"> {{ user.email }}</h4> </center>  */
/*                 <center> <ul class="social list-inline">*/
/*                     <li><a href="#"><i class="fa fa-twitter"></i></a></li>                   */
/*                     <li><a href="#"><i class="fa fa-google-plus"></i></a></li>*/
/*                     <li><a href="#"><i class="fa fa-linkedin"></i></a></li>*/
/*                     <li><a href="#"><i class="fa fa-github-alt"></i></a></li>                  */
/*                     <li class="last-item"><a href="#"><i class="fa fa-hacker-news"></i></a></li>                 */
/*                     </ul> </center>*/
/* */
/*             </div><!--//profile-->*/
/*          */
/* */
/*     </div>*/
/*            */
/*         */
/*               <div class="secondary col-md-7  col-sm-12 col-xs-12">*/
/*                   <section class="about section">*/
/*                     <div class="section-inner">*/
/*                         <h2 class="heading">About Me</h2>*/
/*                         <div class="content">*/
/*  */
/*            */
/*                <p>  <strong>{{ 'profile.show.username'|trans }}:</strong> {{ user.username }}<br>*/
/*                                 <strong>{{ 'profile.show.email'|trans }}:</strong> {{ user.email }}<br>*/
/*                                 <strong> {{ 'Your forname'|trans }}:</strong> {{ user.nomUtilisateur }}<br>*/
/*                             </p>*/
/*                          */
/*                         </div><!--//content-->*/
/*                     </div><!--//section-inner-->                 */
/*                 </section><!--//section-->*/
/*                   <section class="experience section">*/
/*                     <div class="section-inner">*/
/*                         <h2 class="heading">Work Experience</h2>*/
/*                         <div class="content">*/
/*                             */
/*         <strong> {{ 'Experience'|trans }}: </strong>{{ user.experience }}*/
/* */
/*                         </div><!--//content-->  */
/*                     </div><!--//section-inner-->                 */
/*                 </section><!--//section-->*/
/*                  <aside class="info aside section">*/
/*                     <div class="section-inner">*/
/*                         <h2 class="heading sr-only">Basic Information</h2>*/
/*                         <div class="content">*/
/*                             <ul class="list-unstyled">*/
/*                                 <li><i class="fa fa-map-marker"></i><span class="sr-only">Location:</span>Tunis, Tunisia</li>*/
/*                                 <li><i class="fa fa-envelope-o"></i><span class="sr-only">Email:</span><a href="#">{{ user.email }}</a></li>*/
/*                                 <li><i class="fa fa-link"></i><span class="sr-only">Website:</span><a href="#">http://www.website.com</a></li>*/
/*                             </ul>*/
/*                         </div><!--//content-->  */
/*                     </div><!--//section-inner-->                 */
/*                 </aside><!--//aside-->*/
/*                 */
/*                 <aside class="skills aside section">*/
/*                     <div class="section-inner">*/
/*                         <h2 class="heading">Skills</h2>*/
/*                         <div class="content">*/
/*                             <p class="intro">*/
/*                                 Intro about your skills goes here. Keep the list lean and only show your primary skillset. You can always provide a link to your Linkedin or Coderwall profile so people can get more info there.</p>*/
/*                             {{ user.competences }}*/
/*                                       */
/*                         </div><!--//content-->  */
/*                     </div><!--//section-inner-->                 */
/*                 </aside><!--//section-->*/
/*                 */
/*                 */
/*                 */
/*                       */
/*                 */
/*                 */
/*                 <aside class="list music aside section">*/
/*                     <div class="section-inner">*/
/*                         <h2 class="heading">Favourite coding music</h2>*/
/*                         <div class="content">*/
/*                             <ul class="list-unstyled">                                */
/*                                 <li><i class="fa fa-headphones"></i> <a href="http://www.jango.com/">www.jango.com</a></li>*/
/*                             </ul>*/
/*                         </div><!--//content-->*/
/*                     </div><!--//section-inner-->*/
/*                 </aside><!--//section-->*/
/*                 */
/*                 */
/*                 */
/*                */
/*               */
/*             </div>*/
/* */
/*     <div class="primary pull-right col-sm-offset-0 col-xs-offset-12">*/
/*              <section class="latest section">*/
/*                   */
/*                             <hr class="divider" />*/
/*                             <div class="item row">*/
/*                                 <a class="col-md-12 col-sm-12 col-xs-12" href="http://themes.3rdwavemedia.com/website-templates/responsive-bootstrap-theme-mobile-apps-atom/" target="_blank">*/
/*                                 <img class="img-responsive project-image" src="{{asset('assets_profile/images/projects/project-5.png')}}" alt="project name" />*/
/*                                 </a>*/
/*                                 <div class="desc col-md-8 col-sm-8 col-xs-12">*/
/*                                     <h3 class="title"><a href="http://themes.3rdwavemedia.com/website-templates/responsive-bootstrap-theme-mobile-apps-atom/" target="_blank">View Projetcs</a></h3>*/
/*                                     <p><a class="more-link" href="http://themes.3rdwavemedia.com/website-templates/responsive-bootstrap-theme-mobile-apps-atom/" target="_blank"><i class="fa fa-external-link"></i> Find out more</a></p>*/
/*                                 </div><!--//desc-->                          */
/*                             </div><!--//item-->*/
/*                              <div class="item row">*/
/*                                 <a class="col-md-12 col-sm-12 col-xs-12" href="http://themes.3rdwavemedia.com/website-templates/responsive-bootstrap-theme-for-startups-tempo/" target="_blank">*/
/*                                 <img class="img-responsive project-image" src="{{asset('assets_profile/images/projects/project-1.png')}}" alt="project name" />*/
/*                                 </a>*/
/*                                 <div class="desc col-md-8 col-sm-8 col-xs-12">*/
/*                                     <h3 class="title"><a href="http://themes.3rdwavemedia.com/website-templates/responsive-bootstrap-theme-for-startups-tempo/" target="_blank">View Ideas</a></h3>*/
/*                                     <p><a class="more-link" href="http://themes.3rdwavemedia.com/website-templates/responsive-bootstrap-theme-for-startups-tempo/" target="_blank"><i class="fa fa-external-link"></i> Find out more</a></p>*/
/*                                 </div><!--//desc-->                          */
/*                             </div><!--//item--> */
/*                             <div class="item row">*/
/*                                 <a class="col-md-12 col-sm-12 col-xs-12" href="http://themes.3rdwavemedia.com/website-templates/responsive-bootstrap-theme-for-mobile-apps-delta/" target="_blank">*/
/*                                 <img class="img-responsive project-image" src="{{asset('assets_profile/images/projects/project-2.png')}}" alt="project name" />*/
/*                                 </a>*/
/*                                 <div class="desc col-md-8 col-sm-8 col-xs-12">*/
/*                                     <h3 class="title"><a href="http://themes.3rdwavemedia.com/website-templates/responsive-bootstrap-theme-for-mobile-apps-delta/" target="_blank">View Forum</a></h3>*/
/*                                     <p><a class="more-link" href="http://themes.3rdwavemedia.com/website-templates/responsive-bootstrap-theme-for-mobile-apps-delta/" target="_blank"><i class="fa fa-external-link"></i> Find out more</a></p>*/
/*                                 </div><!--//desc-->                          */
/*                             </div><!--//item-->*/
/*                 </section><!--//section-->*/
/*     </div>     */
/* </div>*/
