<?php

/* IbrowsXeditableBundle::xeditableform.html.twig */
class __TwigTemplate_a729203537b603b1cf7a5d2020f42d80939254d04694ae0ca6d3f439a8977a35 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget', array("attr" => (isset($context["attributes"]) ? $context["attributes"] : $this->getContext($context, "attributes"))));
        echo "
 ";
        // line 2
        echo $this->env->getExtension('ibrows_xeditable')->xeditInlineRender((isset($context["xeditableFormMapper"]) ? $context["xeditableFormMapper"] : $this->getContext($context, "xeditableFormMapper")), "username", array("data-emptytext" => $this->env->getExtension('translator')->trans("username")));
        echo "
  ";
        // line 3
        echo $this->env->getExtension('ibrows_xeditable')->xeditInlineRender((isset($context["xeditableFormMapper"]) ? $context["xeditableFormMapper"] : $this->getContext($context, "xeditableFormMapper")), "email", array("data-emptytext" => $this->env->getExtension('translator')->trans("email")));
    }

    public function getTemplateName()
    {
        return "IbrowsXeditableBundle::xeditableform.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 3,  23 => 2,  19 => 1,);
    }
}
/* {{ form_widget(form, {'attr': attributes}) }}*/
/*  {{ xedit_inline_render(xeditableFormMapper, 'username', {'data-emptytext': 'username'|trans}) }}*/
/*   {{ xedit_inline_render(xeditableFormMapper, 'email', {'data-emptytext': 'email'|trans}) }}*/
