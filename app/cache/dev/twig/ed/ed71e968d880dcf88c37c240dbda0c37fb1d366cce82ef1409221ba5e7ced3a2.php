<?php

/* FOSUserBundle:Profile:edit_content.html.twig */
class __TwigTemplate_c0bce08c2652c49dd33345efd0f54471705739e38cdd7e96b4eb50d0502de271 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "
    
    
<center>
                         <div class=\"form-group\">";
        // line 6
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "username", array()), 'row', array("attr" => array("class" => "radio inline")));
        echo "</div>
    <div class=\"form-group\">";
        // line 7
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "email", array()), 'row', array("attr" => array("class" => "radio inline")));
        echo "</div>
    <div class=\"form-group\">";
        // line 8
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nomUtilisateur", array()), 'row', array("attr" => array("class" => "radio inline")));
        echo "</div>
    <div class=\"form-group\">";
        // line 9
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "competences", array()), 'row', array("attr" => array("class" => "form-control")));
        echo "</div>
    <div class=\"form-group\">";
        // line 10
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "experience", array()), 'row', array("attr" => array("class" => "form-control")));
        echo "</div>
                                
";
        // line 12
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start', array("action" => $this->env->getExtension('routing')->getPath("fos_user_profile_edit"), "attr" => array("class" => "fos_user_profile_edit")));
        echo "
    ";
        // line 13
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "
    <div>
        
    <div class=\"form-group\">";
        // line 16
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "username", array()), 'row', array("attr" => array("class" => "form-control")));
        echo "</div>
    <div class=\"form-group\">";
        // line 17
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nomUtilisateur", array()), 'row', array("attr" => array("class" => "form-control")));
        echo "</div>
    <div class=\"form-group\">";
        // line 18
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "competences", array()), 'row', array("attr" => array("class" => "form-control")));
        echo "</div>
    <div class=\"form-group\">";
        // line 19
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "experience", array()), 'row', array("attr" => array("class" => "form-control")));
        echo "</div>
    <div class=\"form-group\">";
        // line 20
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "current_password", array()), 'row', array("attr" => array("class" => "form-control")));
        echo "</div></div></center>
            ";
        // line 22
        echo "<div class=\"new_people\">
            <a input type=\"submit\" href=\"";
        // line 23
        echo $this->env->getExtension('routing')->getPath("fos_user_profile_show");
        echo "\">Update changes</a>
    <input type=\"submit\" href=\"";
        // line 24
        echo $this->env->getExtension('routing')->getPath("fos_user_profile_show");
        echo "\" />
</div>  
";
        // line 26
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
";
    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Profile:edit_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  88 => 26,  83 => 24,  79 => 23,  76 => 22,  72 => 20,  68 => 19,  64 => 18,  60 => 17,  56 => 16,  50 => 13,  46 => 12,  41 => 10,  37 => 9,  33 => 8,  29 => 7,  25 => 6,  19 => 2,);
    }
}
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/*     */
/*     */
/* <center>*/
/*                          <div class="form-group">{{ form_row(form.username,{'attr': {'class': 'radio inline'}}) }}</div>*/
/*     <div class="form-group">{{ form_row(form.email,{'attr': {'class': 'radio inline'}}) }}</div>*/
/*     <div class="form-group">{{ form_row(form.nomUtilisateur,{'attr': {'class': 'radio inline'}}) }}</div>*/
/*     <div class="form-group">{{ form_row(form.competences,{'attr': {'class': 'form-control'}}) }}</div>*/
/*     <div class="form-group">{{ form_row(form.experience,{'attr': {'class': 'form-control'}}) }}</div>*/
/*                                 */
/* {{ form_start(form, { 'action': path('fos_user_profile_edit'), 'attr': { 'class': 'fos_user_profile_edit' } }) }}*/
/*     {{ form_widget(form) }}*/
/*     <div>*/
/*         */
/*     <div class="form-group">{{ form_row(form.username,{'attr': {'class': 'form-control'}}) }}</div>*/
/*     <div class="form-group">{{ form_row(form.nomUtilisateur,{'attr': {'class': 'form-control'}}) }}</div>*/
/*     <div class="form-group">{{ form_row(form.competences,{'attr': {'class': 'form-control'}}) }}</div>*/
/*     <div class="form-group">{{ form_row(form.experience,{'attr': {'class': 'form-control'}}) }}</div>*/
/*     <div class="form-group">{{ form_row(form.current_password,{'attr': {'class': 'form-control'}}) }}</div></div></center>*/
/*             {#<a class="btn btn-cta-primary pull-right" href="{{ path('profile.edit.submit')|trans }}" <i class="fa fa-paper-plane"></i> Edit Submit</a>#}*/
/* <div class="new_people">*/
/*             <a input type="submit" href="{{path('fos_user_profile_show')}}">Update changes</a>*/
/*     <input type="submit" href="{{ path('fos_user_profile_show')}}" />*/
/* </div>  */
/* {{ form_end(form) }}*/
/* */
