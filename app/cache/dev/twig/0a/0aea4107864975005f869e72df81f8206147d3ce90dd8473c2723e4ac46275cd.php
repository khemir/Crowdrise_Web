<?php

/* CrowdriseAdministrationBundle:Default:allideas.html.twig */
class __TwigTemplate_089e83f4d23a2c06444c5486d38e98bcbe2151d944de793925e9ef6b751ab7b5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("CrowdriseAdministrationBundle::layout.html.twig", "CrowdriseAdministrationBundle:Default:allideas.html.twig", 1);
        $this->blocks = array(
            'nav_li_dash' => array($this, 'block_nav_li_dash'),
            'nav_li_allideas' => array($this, 'block_nav_li_allideas'),
            'pagecontent' => array($this, 'block_pagecontent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "CrowdriseAdministrationBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_nav_li_dash($context, array $blocks = array())
    {
        // line 4
        echo "<li><a href=\"";
        echo $this->env->getExtension('routing')->getPath("crowdrise_administration_homepage");
        echo "\"><span>Dashboard</span> <i class=\"icon-screen2\"></i></a></li>
";
    }

    // line 7
    public function block_nav_li_allideas($context, array $blocks = array())
    {
        // line 8
        echo "<li class=\"active\"><a href=\"";
        echo $this->env->getExtension('routing')->getPath("crowdrise_administration_all_ideas");
        echo "\"><span>All Ideas</span> <i class=\"icon-lamp2\"></i></a> </li>
";
    }

    // line 12
    public function block_pagecontent($context, array $blocks = array())
    {
        // line 13
        echo "
    <!-- Page content -->
  <div class=\"page-content\">
    <!-- Page header -->
    <div class=\"page-header\">
      <div class=\"page-title\">
        <h3>All Ideas <small>Management</small></h3>
      </div>

      <div id=\"reportrange\" class=\"range\">
        <div class=\"visible-xs header-element-toggle\"><a class=\"btn btn-primary btn-icon\"><i class=\"icon-calendar\"></i></a></div>
        <div class=\"date-range\"></div>
        <span class=\"label label-danger\">9</span></div>

    </div>
    <!-- /page header -->
    <!-- Breadcrumbs line -->
    <div class=\"breadcrumb-line\">
      <ul class=\"breadcrumb\">
        <li><a href=\"index.html\">Home</a></li>
        <li class=\"active\">Requests list</li>
      </ul>
      <div class=\"visible-xs breadcrumb-toggle\"><a class=\"btn btn-link btn-lg btn-icon\" data-toggle=\"collapse\" data-target=\".breadcrumb-buttons\"><i class=\"icon-menu2\"></i></a></div>
     

    </div>
    <!-- /breadcrumbs line -->
    <!-- Page tabs -->
    <div class=\"tabbable page-tabs\">
   
      <div class=\"tab-content\">
        <!-- First tab -->
        <div class=\"tab-pane active fade in\" id=\"all-tasks\">


          <!-- Tasks table -->
          <div class=\"panel panel-default\">
            <div class=\"panel-heading\">
              <h6 class=\"panel-title\"><i class=\"icon-paragraph-justify2\"></i> Requests</h6>
              <span class=\"pull-right label label-danger\">

              3

              </span> </div>
            <div class=\"datatable-tasks\">


              <table class=\"table table-bordered\">
                <thead>
                  <tr>
                    <th>Idea Description</th>
                    <th>Category</th>
                    <th class=\"task-priority\">\$.Needed</th>
                    <th class=\"task-date-added\">Date Added</th>
                    <th class=\"task-progress\">Submitter</th>
                    <th class=\"task-deadline\">Details</th>
                  </tr>
                </thead>
                <tbody>

                    
                    ";
        // line 74
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["idees"]) ? $context["idees"] : $this->getContext($context, "idees")));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 75
            echo "                  <tr>


                    <td class=\"task-desc\"><a href=\"task_detailed.html\">";
            // line 78
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "intituleIdee", array()), "html", null, true);
            echo "</a> <span>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "descriptionIdee", array()), "html", null, true);
            echo "</span></td>


                    <td>";
            // line 81
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "categorieIdee", array()), "html", null, true);
            echo "</td>

                    ";
            // line 83
            if (($this->getAttribute($context["i"], "sommeRecolteIdee", array()) < 200)) {
                // line 84
                echo "                    <td class=\"text-center\"><span class=\"label label-info\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "sommeRecolteIdee", array()), "html", null, true);
                echo " \$</span></td>
                    ";
            } elseif ((($this->getAttribute(            // line 85
$context["i"], "sommeRecolteIdee", array()) > 200) && ($this->getAttribute($context["i"], "sommeRecolteIdee", array()) < 800))) {
                // line 86
                echo "                    <td class=\"text-center\"><span class=\"label label-success\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "sommeRecolteIdee", array()), "html", null, true);
                echo " \$</span></td>
                    ";
            } else {
                // line 88
                echo "                    <td class=\"text-center\"><span class=\"label label-danger\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "sommeRecolteIdee", array()), "html", null, true);
                echo " \$</span></td>
                    ";
            }
            // line 90
            echo "
                    <td>";
            // line 91
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "dateDepotIdee", array()), "html", null, true);
            echo "</td>


                    <td>";
            // line 94
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["i"], "idUtilisateur", array()), "username", array()), "html", null, true);
            echo "</td>


                    <td> 
               <a href=\"";
            // line 98
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("crowdrise_administration_details_idea", array("id" => $this->getAttribute($context["i"], "idIdee", array()), "intitule" => $this->getAttribute($context["i"], "intituleIdee", array()), "description" => $this->getAttribute($context["i"], "descriptionIdee", array()), "categorie" => $this->getAttribute($context["i"], "categorieIdee", array()), "date" => $this->getAttribute($context["i"], "dateDepotIdee", array()), "somme" => $this->getAttribute($context["i"], "sommeRecolteIdee", array()), "note" => $this->getAttribute($context["i"], "noteIdee", array()))), "html", null, true);
            echo "\"><i class=\"icon-info2\"></i> Informations</a>
                    </td>
                    
                 
                  
                  
                  </tr>
                  
                  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 107
        echo "
                 
                </tbody>
              </table>
            </div>
          </div>
          <!-- /tasks table -->
        </div>
        <!-- /first tab -->

      </div>
    </div>
    <!-- /page tabs -->
    <!-- Footer -->
    <div class=\"footer clearfix\">
      <div class=\"pull-left\">&copy; 2016. Crowdrise Admin by <a href=\"http://themeforest.net/user/Kopyov\">DevX Team</a></div>
      <div class=\"pull-right icons-group\"> <a href=\"#\"><i class=\"icon-screen2\"></i></a> <a href=\"#\"><i class=\"icon-balance\"></i></a> <a href=\"#\"><i class=\"icon-cog3\"></i></a> </div>
    </div>
    <!-- /footer -->
  </div>
  <!-- /page content -->
";
    }

    public function getTemplateName()
    {
        return "CrowdriseAdministrationBundle:Default:allideas.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  190 => 107,  175 => 98,  168 => 94,  162 => 91,  159 => 90,  153 => 88,  147 => 86,  145 => 85,  140 => 84,  138 => 83,  133 => 81,  125 => 78,  120 => 75,  116 => 74,  53 => 13,  50 => 12,  43 => 8,  40 => 7,  33 => 4,  30 => 3,  11 => 1,);
    }
}
/* {% extends "CrowdriseAdministrationBundle::layout.html.twig" %}*/
/* */
/* {% block nav_li_dash %}*/
/* <li><a href="{{path('crowdrise_administration_homepage')}}"><span>Dashboard</span> <i class="icon-screen2"></i></a></li>*/
/* {% endblock %}*/
/* */
/* {% block nav_li_allideas %}*/
/* <li class="active"><a href="{{path('crowdrise_administration_all_ideas')}}"><span>All Ideas</span> <i class="icon-lamp2"></i></a> </li>*/
/* {% endblock %}*/
/* */
/* */
/* {% block pagecontent %}*/
/* */
/*     <!-- Page content -->*/
/*   <div class="page-content">*/
/*     <!-- Page header -->*/
/*     <div class="page-header">*/
/*       <div class="page-title">*/
/*         <h3>All Ideas <small>Management</small></h3>*/
/*       </div>*/
/* */
/*       <div id="reportrange" class="range">*/
/*         <div class="visible-xs header-element-toggle"><a class="btn btn-primary btn-icon"><i class="icon-calendar"></i></a></div>*/
/*         <div class="date-range"></div>*/
/*         <span class="label label-danger">9</span></div>*/
/* */
/*     </div>*/
/*     <!-- /page header -->*/
/*     <!-- Breadcrumbs line -->*/
/*     <div class="breadcrumb-line">*/
/*       <ul class="breadcrumb">*/
/*         <li><a href="index.html">Home</a></li>*/
/*         <li class="active">Requests list</li>*/
/*       </ul>*/
/*       <div class="visible-xs breadcrumb-toggle"><a class="btn btn-link btn-lg btn-icon" data-toggle="collapse" data-target=".breadcrumb-buttons"><i class="icon-menu2"></i></a></div>*/
/*      */
/* */
/*     </div>*/
/*     <!-- /breadcrumbs line -->*/
/*     <!-- Page tabs -->*/
/*     <div class="tabbable page-tabs">*/
/*    */
/*       <div class="tab-content">*/
/*         <!-- First tab -->*/
/*         <div class="tab-pane active fade in" id="all-tasks">*/
/* */
/* */
/*           <!-- Tasks table -->*/
/*           <div class="panel panel-default">*/
/*             <div class="panel-heading">*/
/*               <h6 class="panel-title"><i class="icon-paragraph-justify2"></i> Requests</h6>*/
/*               <span class="pull-right label label-danger">*/
/* */
/*               3*/
/* */
/*               </span> </div>*/
/*             <div class="datatable-tasks">*/
/* */
/* */
/*               <table class="table table-bordered">*/
/*                 <thead>*/
/*                   <tr>*/
/*                     <th>Idea Description</th>*/
/*                     <th>Category</th>*/
/*                     <th class="task-priority">$.Needed</th>*/
/*                     <th class="task-date-added">Date Added</th>*/
/*                     <th class="task-progress">Submitter</th>*/
/*                     <th class="task-deadline">Details</th>*/
/*                   </tr>*/
/*                 </thead>*/
/*                 <tbody>*/
/* */
/*                     */
/*                     {% for i in idees %}*/
/*                   <tr>*/
/* */
/* */
/*                     <td class="task-desc"><a href="task_detailed.html">{{i.intituleIdee}}</a> <span>{{i.descriptionIdee}}</span></td>*/
/* */
/* */
/*                     <td>{{i.categorieIdee}}</td>*/
/* */
/*                     {% if i.sommeRecolteIdee<200 %}*/
/*                     <td class="text-center"><span class="label label-info">{{i.sommeRecolteIdee}} $</span></td>*/
/*                     {% elseif i.sommeRecolteIdee>200 and i.sommeRecolteIdee<800 %}*/
/*                     <td class="text-center"><span class="label label-success">{{i.sommeRecolteIdee}} $</span></td>*/
/*                     {% else%}*/
/*                     <td class="text-center"><span class="label label-danger">{{i.sommeRecolteIdee}} $</span></td>*/
/*                     {% endif %}*/
/* */
/*                     <td>{{i.dateDepotIdee}}</td>*/
/* */
/* */
/*                     <td>{{i.idUtilisateur.username}}</td>*/
/* */
/* */
/*                     <td> */
/*                <a href="{{path('crowdrise_administration_details_idea',{'id':i.idIdee,'intitule':i.intituleIdee,'description':i.descriptionIdee, 'categorie':i.categorieIdee,'date':i.dateDepotIdee,'somme':i.sommeRecolteIdee,'note':i.noteIdee})}}"><i class="icon-info2"></i> Informations</a>*/
/*                     </td>*/
/*                     */
/*                  */
/*                   */
/*                   */
/*                   </tr>*/
/*                   */
/*                   {% endfor %}*/
/* */
/*                  */
/*                 </tbody>*/
/*               </table>*/
/*             </div>*/
/*           </div>*/
/*           <!-- /tasks table -->*/
/*         </div>*/
/*         <!-- /first tab -->*/
/* */
/*       </div>*/
/*     </div>*/
/*     <!-- /page tabs -->*/
/*     <!-- Footer -->*/
/*     <div class="footer clearfix">*/
/*       <div class="pull-left">&copy; 2016. Crowdrise Admin by <a href="http://themeforest.net/user/Kopyov">DevX Team</a></div>*/
/*       <div class="pull-right icons-group"> <a href="#"><i class="icon-screen2"></i></a> <a href="#"><i class="icon-balance"></i></a> <a href="#"><i class="icon-cog3"></i></a> </div>*/
/*     </div>*/
/*     <!-- /footer -->*/
/*   </div>*/
/*   <!-- /page content -->*/
/* {% endblock %}*/
/* */
