<?php

/* CrowdriseUserBundle:Profile:affiche.html.twig */
class __TwigTemplate_77f574f1d497543b035b0086a5b4cd3f2b23ca2ce2b51d49af4fcb29209786a6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div> 
    <img src=\"";
        // line 2
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl($this->env->getExtension('routing')->getPath("my_image_route", array("id" => $this->getAttribute((isset($context["image"]) ? $context["image"] : $this->getContext($context, "image")), "id", array())))), "html", null, true);
        echo "\"/> 
</div>
";
    }

    public function getTemplateName()
    {
        return "CrowdriseUserBundle:Profile:affiche.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 2,  19 => 1,);
    }
}
/* <div> */
/*     <img src="{{ asset(path('my_image_route', {'id': image.id})) }}"/> */
/* </div>*/
/* */
