<?php

/* CrowdriseAdministrationBundle:Default:projects.html.twig */
class __TwigTemplate_f15d7aa55a4ac99e78126f8af1a5f970e290fd9e7a20a82e1086e8f4738052ea extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("CrowdriseAdministrationBundle::layout.html.twig", "CrowdriseAdministrationBundle:Default:projects.html.twig", 1);
        $this->blocks = array(
            'nav_li_dash' => array($this, 'block_nav_li_dash'),
            'nav_li_projects' => array($this, 'block_nav_li_projects'),
            'pagecontent' => array($this, 'block_pagecontent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "CrowdriseAdministrationBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_nav_li_dash($context, array $blocks = array())
    {
        // line 4
        echo "<li><a href=\"";
        echo $this->env->getExtension('routing')->getPath("crowdrise_administration_homepage");
        echo "\"><span>Dashboard</span> <i class=\"icon-screen2\"></i></a></li>
";
    }

    // line 7
    public function block_nav_li_projects($context, array $blocks = array())
    {
        // line 8
        echo "<li class=\"active\"><a href=\"";
        echo $this->env->getExtension('routing')->getPath("crowdrise_administration_projects");
        echo "\"><span>Projects</span> <i class=\"icon-stackoverflow\"></i></a> </li>
";
    }

    // line 12
    public function block_pagecontent($context, array $blocks = array())
    {
        // line 13
        echo "    
    
  <!-- Page content -->


  <div class=\"page-content\">
    <!-- Page header -->
    <div class=\"page-header\">
      <div class=\"page-title\">
        <h3>Projects <small>Projects managment</small></h3>
      </div>
      <div id=\"reportrange\" class=\"range\">
        <div class=\"visible-xs header-element-toggle\"><a class=\"btn btn-primary btn-icon\"><i class=\"icon-calendar\"></i></a></div>
        <div class=\"date-range\"></div>
        <span class=\"label label-danger\">9</span></div>
    </div>
    <!-- /page header -->

    <!-- Breadcrumbs line -->
    <div class=\"breadcrumb-line\">
      <ul class=\"breadcrumb\">
        <li><a href=\"index.html\">Home</a></li>
        <li class=\"active\">Projects</li>
      </ul>

   
    </div>
    <!-- /breadcrumbs line -->



    <!-- Page tabs -->
    <div class=\"tabbable page-tabs\">

      <div class=\"tab-content\">

        <!-- Third tab -->
        <div class=\"active\" id=\"list-view\">
          <!-- Table view -->
          <div class=\"panel panel-default\">
            <div class=\"panel-heading\">
              <h5 class=\"panel-title\"><i class=\"icon-stackoverflow\"></i> Projects List</h5>
              <span class=\"label label-danger pull-right\">+23</span> </div>
            <div class=\"datatable-media\">
              <table class=\"table table-bordered table-striped\">


                <thead>

                  <tr>
                    <th class=\"image-column\">Project</th>
                    <th>Description</th>
                    <th>Submission Date</th>
                    <th>Project Info</th>
                    <th class=\"team-links\">Actions</th>
                  </tr>

                </thead>


                <tbody>
                    
                    
                 
                ";
        // line 77
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["projets"]) ? $context["projets"] : $this->getContext($context, "projets")));
        foreach ($context['_seq'] as $context["_key"] => $context["p"]) {
            // line 78
            echo "
                  <tr>                    
                        
                      <td class=\"text-semibold\"><a>";
            // line 81
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "intituleProjet", array()), "html", null, true);
            echo "</a></td>
                    
                    <td class=\"muted\">";
            // line 83
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "descriptionProjet", array()), "html", null, true);
            echo "</td>

                    <td class=\"text-semibold\">";
            // line 85
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "dateDepotProjet", array()), "html", null, true);
            echo "</td>


                    <td class=\"file-info\">
                      
                       <span><strong>Submitter :</strong> <a href=\"#\"> ";
            // line 90
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["p"], "idUtilisateur", array()), "username", array()), "html", null, true);
            echo "</a></span>
                       <span><strong>Category :</strong> ";
            // line 91
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "categorieProjet", array()), "html", null, true);
            echo "</span>
                       <span><strong>Competences needed :</strong> ";
            // line 92
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "competencesDemandeProjet", array()), "html", null, true);
            echo "</span>
                       <span><strong>Key words :</strong> ";
            // line 93
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "motcleProjet", array()), "html", null, true);
            echo "</span>
                       <span><strong>No. of solvers :</strong> ";
            // line 94
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "nbsolversProjet", array()), "html", null, true);
            echo "</span>

                    </td>

                    <td class=\"text-center\"><div class=\"btn-group\">
                        <button type=\"button\" class=\"btn btn-icon btn-danger dropdown-toggle\" data-toggle=\"dropdown\"><i class=\"icon-cog4\"></i></button>
                        <ul class=\"dropdown-menu icons-right dropdown-menu-right\">
                          <li><a href=\"";
            // line 101
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("crowdrise_administration_delete_project", array("id" => $this->getAttribute($context["p"], "idProjet", array()))), "html", null, true);
            echo "\"><i class=\"icon-checkmark3\"></i> Delete</a></li>
                          <li><a href=\"#\"><i class=\"icon-share2\"></i> Report</a></li>
       
           
                        </ul>
                      </div></td>


                  </tr>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['p'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 111
        echo "


                </tbody>


              </table>
            </div>
          </div>
          <!-- /table view -->
        </div>
        <!-- /third tab -->
      </div>
    </div>
    <!-- page tabs -->


    <!-- Footer -->
    <div class=\"footer clearfix\">
      <div class=\"pull-left\">&copy; 2016. Crowdrise Admin by <a href=\"http://themeforest.net/user/Kopyov\">DevX Team</a></div>
      <div class=\"pull-right icons-group\"> <a href=\"#\"><i class=\"icon-screen2\"></i></a> <a href=\"#\"><i class=\"icon-balance\"></i></a> <a href=\"#\"><i class=\"icon-cog3\"></i></a> </div>
    </div>
    <!-- /footer -->
  </div>
  <!-- /page content -->
    
";
    }

    public function getTemplateName()
    {
        return "CrowdriseAdministrationBundle:Default:projects.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  188 => 111,  172 => 101,  162 => 94,  158 => 93,  154 => 92,  150 => 91,  146 => 90,  138 => 85,  133 => 83,  128 => 81,  123 => 78,  119 => 77,  53 => 13,  50 => 12,  43 => 8,  40 => 7,  33 => 4,  30 => 3,  11 => 1,);
    }
}
/* {% extends "CrowdriseAdministrationBundle::layout.html.twig" %}*/
/* */
/* {% block nav_li_dash %}*/
/* <li><a href="{{path('crowdrise_administration_homepage')}}"><span>Dashboard</span> <i class="icon-screen2"></i></a></li>*/
/* {% endblock %}*/
/*        */
/* {% block nav_li_projects %}*/
/* <li class="active"><a href="{{path('crowdrise_administration_projects')}}"><span>Projects</span> <i class="icon-stackoverflow"></i></a> </li>*/
/* {% endblock %}*/
/* */
/* */
/* {% block pagecontent %}*/
/*     */
/*     */
/*   <!-- Page content -->*/
/* */
/* */
/*   <div class="page-content">*/
/*     <!-- Page header -->*/
/*     <div class="page-header">*/
/*       <div class="page-title">*/
/*         <h3>Projects <small>Projects managment</small></h3>*/
/*       </div>*/
/*       <div id="reportrange" class="range">*/
/*         <div class="visible-xs header-element-toggle"><a class="btn btn-primary btn-icon"><i class="icon-calendar"></i></a></div>*/
/*         <div class="date-range"></div>*/
/*         <span class="label label-danger">9</span></div>*/
/*     </div>*/
/*     <!-- /page header -->*/
/* */
/*     <!-- Breadcrumbs line -->*/
/*     <div class="breadcrumb-line">*/
/*       <ul class="breadcrumb">*/
/*         <li><a href="index.html">Home</a></li>*/
/*         <li class="active">Projects</li>*/
/*       </ul>*/
/* */
/*    */
/*     </div>*/
/*     <!-- /breadcrumbs line -->*/
/* */
/* */
/* */
/*     <!-- Page tabs -->*/
/*     <div class="tabbable page-tabs">*/
/* */
/*       <div class="tab-content">*/
/* */
/*         <!-- Third tab -->*/
/*         <div class="active" id="list-view">*/
/*           <!-- Table view -->*/
/*           <div class="panel panel-default">*/
/*             <div class="panel-heading">*/
/*               <h5 class="panel-title"><i class="icon-stackoverflow"></i> Projects List</h5>*/
/*               <span class="label label-danger pull-right">+23</span> </div>*/
/*             <div class="datatable-media">*/
/*               <table class="table table-bordered table-striped">*/
/* */
/* */
/*                 <thead>*/
/* */
/*                   <tr>*/
/*                     <th class="image-column">Project</th>*/
/*                     <th>Description</th>*/
/*                     <th>Submission Date</th>*/
/*                     <th>Project Info</th>*/
/*                     <th class="team-links">Actions</th>*/
/*                   </tr>*/
/* */
/*                 </thead>*/
/* */
/* */
/*                 <tbody>*/
/*                     */
/*                     */
/*                  */
/*                 {% for p in projets %}*/
/* */
/*                   <tr>                    */
/*                         */
/*                       <td class="text-semibold"><a>{{p.intituleProjet}}</a></td>*/
/*                     */
/*                     <td class="muted">{{p.descriptionProjet}}</td>*/
/* */
/*                     <td class="text-semibold">{{p.dateDepotProjet}}</td>*/
/* */
/* */
/*                     <td class="file-info">*/
/*                       */
/*                        <span><strong>Submitter :</strong> <a href="#"> {{p.idUtilisateur.username}}</a></span>*/
/*                        <span><strong>Category :</strong> {{p.categorieProjet}}</span>*/
/*                        <span><strong>Competences needed :</strong> {{p.competencesDemandeProjet}}</span>*/
/*                        <span><strong>Key words :</strong> {{p.motcleProjet}}</span>*/
/*                        <span><strong>No. of solvers :</strong> {{p.nbsolversProjet}}</span>*/
/* */
/*                     </td>*/
/* */
/*                     <td class="text-center"><div class="btn-group">*/
/*                         <button type="button" class="btn btn-icon btn-danger dropdown-toggle" data-toggle="dropdown"><i class="icon-cog4"></i></button>*/
/*                         <ul class="dropdown-menu icons-right dropdown-menu-right">*/
/*                           <li><a href="{{path('crowdrise_administration_delete_project',{'id':p.idProjet})}}"><i class="icon-checkmark3"></i> Delete</a></li>*/
/*                           <li><a href="#"><i class="icon-share2"></i> Report</a></li>*/
/*        */
/*            */
/*                         </ul>*/
/*                       </div></td>*/
/* */
/* */
/*                   </tr>*/
/*                     {% endfor %}*/
/* */
/* */
/* */
/*                 </tbody>*/
/* */
/* */
/*               </table>*/
/*             </div>*/
/*           </div>*/
/*           <!-- /table view -->*/
/*         </div>*/
/*         <!-- /third tab -->*/
/*       </div>*/
/*     </div>*/
/*     <!-- page tabs -->*/
/* */
/* */
/*     <!-- Footer -->*/
/*     <div class="footer clearfix">*/
/*       <div class="pull-left">&copy; 2016. Crowdrise Admin by <a href="http://themeforest.net/user/Kopyov">DevX Team</a></div>*/
/*       <div class="pull-right icons-group"> <a href="#"><i class="icon-screen2"></i></a> <a href="#"><i class="icon-balance"></i></a> <a href="#"><i class="icon-cog3"></i></a> </div>*/
/*     </div>*/
/*     <!-- /footer -->*/
/*   </div>*/
/*   <!-- /page content -->*/
/*     */
/* {% endblock %}*/
/* */
