<?php

/* CrowdriseUserBundle:Profile:Update.html.twig */
class __TwigTemplate_f14d1fa018f5a9c5d0abaf08f2f610ae6603a437756ba861515a01e4965fb68e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("CrowdriseMembreBundle:Default:register.html.twig", "CrowdriseUserBundle:Profile:Update.html.twig", 1);
        $this->blocks = array(
            'container' => array($this, 'block_container'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "CrowdriseMembreBundle:Default:register.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_container($context, array $blocks = array())
    {
        // line 3
        echo "<div id=\"page-wrapper\" class=\"sign-in-wrapper\">
\t\t\t\t<div class=\"graphs\">
\t\t\t\t\t<div class=\"sign-up\">
\t\t\t\t\t\t<h1>Update your profile</h1>
\t\t\t\t\t\t
\t\t\t\t\t\t<h2>Personal Information</h2>
    <div class=\"sign-u\">\t
    ";
        // line 10
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["Form"]) ? $context["Form"] : $this->getContext($context, "Form")), 'form_start');
        echo "
        <div class=\"form-group\">";
        // line 11
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["Form"]) ? $context["Form"] : $this->getContext($context, "Form")), "username", array()), 'row', array("attr" => array("class" => "form-control")));
        echo "</div>
                <div class=\"form-group\">";
        // line 12
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["Form"]) ? $context["Form"] : $this->getContext($context, "Form")), "email", array()), 'row', array("attr" => array("class" => "form-control")));
        echo "</div>
        <div class=\"form-group\">";
        // line 13
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["Form"]) ? $context["Form"] : $this->getContext($context, "Form")), "nomUtilisateur", array()), 'row', array("attr" => array("class" => "form-control")));
        echo "</div>
        <div class=\"form-group\">";
        // line 14
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["Form"]) ? $context["Form"] : $this->getContext($context, "Form")), "competences", array()), 'row', array("attr" => array("class" => "form-control")));
        echo "</div>
        <div class=\"form-group\">";
        // line 15
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["Form"]) ? $context["Form"] : $this->getContext($context, "Form")), "experience", array()), 'row', array("attr" => array("class" => "form-control")));
        echo "</div>

        <center>    <input type=\"submit\" value=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("validate"), "html", null, true);
        echo "\" /></center>

    
</div>
";
        // line 21
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["Form"]) ? $context["Form"] : $this->getContext($context, "Form")), 'rest');
        echo "
        ";
        // line 22
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["Form"]) ? $context["Form"] : $this->getContext($context, "Form")), 'errors');
        echo "
        
        
";
        // line 25
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["Form"]) ? $context["Form"] : $this->getContext($context, "Form")), 'form_end');
        echo "

</div>
                                        </div>
                                </div>
</div>

";
    }

    public function getTemplateName()
    {
        return "CrowdriseUserBundle:Profile:Update.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  82 => 25,  76 => 22,  72 => 21,  65 => 17,  60 => 15,  56 => 14,  52 => 13,  48 => 12,  44 => 11,  40 => 10,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends "CrowdriseMembreBundle:Default:register.html.twig" %}*/
/* {% block container %}*/
/* <div id="page-wrapper" class="sign-in-wrapper">*/
/* 				<div class="graphs">*/
/* 					<div class="sign-up">*/
/* 						<h1>Update your profile</h1>*/
/* 						*/
/* 						<h2>Personal Information</h2>*/
/*     <div class="sign-u">	*/
/*     {{form_start(Form)}}*/
/*         <div class="form-group">{{ form_row(Form.username,{'attr': {'class': 'form-control'}}) }}</div>*/
/*                 <div class="form-group">{{ form_row(Form.email,{'attr': {'class': 'form-control'}}) }}</div>*/
/*         <div class="form-group">{{ form_row(Form.nomUtilisateur,{'attr': {'class': 'form-control'}}) }}</div>*/
/*         <div class="form-group">{{ form_row(Form.competences,{'attr': {'class': 'form-control'}}) }}</div>*/
/*         <div class="form-group">{{ form_row(Form.experience,{'attr': {'class': 'form-control'}}) }}</div>*/
/* */
/*         <center>    <input type="submit" value="{{ 'validate'|trans }}" /></center>*/
/* */
/*     */
/* </div>*/
/* {{form_rest(Form)}}*/
/*         {{form_errors(Form)}}*/
/*         */
/*         */
/* {{form_end(Form)}}*/
/* */
/* </div>*/
/*                                         </div>*/
/*                                 </div>*/
/* </div>*/
/* */
/* {% endblock  %}*/
