<?php

/* CrowdriseMailBundle:Default:new.html.twig */
class __TwigTemplate_06e33c1f2b6266093f3d248ec1752efaf627e15fe0a962a5d87c3f164a0142ff extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<html>
    <head>
        <title>Formulaire Mail</title>

    </head>
    
    <form action=\"";
        // line 7
        echo $this->env->getExtension('routing')->getPath("testmail_sendMail");
        echo "\" method=\"POST\">
        <h1> Jet set contact </h1>
        <br/>
        
        <h2> Subject</h2>
        <input type=\"text\" name=\"Subject\">
        <br/>
        
        <h2> Email</h2>
        <input type=\"text\" name=\"email\">
        <br/>
        
        <h2> Text : </h2>
        <textarea name=\"message\"></textarea>
        
        <input type=\"submit\" value=\"Send\"/>
    </form>
        
        
</html>";
    }

    public function getTemplateName()
    {
        return "CrowdriseMailBundle:Default:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 7,  19 => 1,);
    }
}
/* <html>*/
/*     <head>*/
/*         <title>Formulaire Mail</title>*/
/* */
/*     </head>*/
/*     */
/*     <form action="{{path('testmail_sendMail')}}" method="POST">*/
/*         <h1> Jet set contact </h1>*/
/*         <br/>*/
/*         */
/*         <h2> Subject</h2>*/
/*         <input type="text" name="Subject">*/
/*         <br/>*/
/*         */
/*         <h2> Email</h2>*/
/*         <input type="text" name="email">*/
/*         <br/>*/
/*         */
/*         <h2> Text : </h2>*/
/*         <textarea name="message"></textarea>*/
/*         */
/*         <input type="submit" value="Send"/>*/
/*     </form>*/
/*         */
/*         */
/* </html>*/
