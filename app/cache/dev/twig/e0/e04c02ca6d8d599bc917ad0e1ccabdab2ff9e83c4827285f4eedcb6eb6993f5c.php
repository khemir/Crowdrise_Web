<?php

/* FOSUserBundle:Security:login.html.twig */
class __TwigTemplate_3f5dbdc0a51ea02b647f2b4ce70071bb07ed73baf8b6bb82e89c5b262567f009 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("CrowdriseMembreBundle::layout1.html.twig", "FOSUserBundle:Security:login.html.twig", 2);
        $this->blocks = array(
            'container' => array($this, 'block_container'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "CrowdriseMembreBundle::layout1.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_container($context, array $blocks = array())
    {
        // line 7
        echo "
";
        // line 9
        echo "
";
        // line 10
        $this->displayBlock('fos_user_content', $context, $blocks);
    }

    public function block_fos_user_content($context, array $blocks = array())
    {
        // line 11
        if ((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error"))) {
            // line 12
            echo "    <div>";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getAttribute((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error")), "messageKey", array()), $this->getAttribute((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error")), "messageData", array()), "security"), "html", null, true);
            echo "</div>
";
        }
        // line 14
        echo "\t\t<div id=\"page-wrapper\" class=\"sign-in-wrapper\">
\t\t\t\t<div class=\"graphs\">
\t\t\t\t\t<div class=\"sign-in-form\">
\t\t\t\t\t\t<div class=\"sign-in-form-top\">
\t\t\t\t\t\t\t<h1>Log in</h1>
\t\t\t\t\t\t</div>
                                            <div class=\"signin\">
\t\t\t\t\t\t\t
<form action=\"";
        // line 22
        echo $this->env->getExtension('routing')->getPath("fos_user_security_check");
        echo "\" method=\"post\">
    
    <input type=\"hidden\" name=\"_csrf_token\" value=\"";
        // line 24
        echo twig_escape_filter($this->env, (isset($context["csrf_token"]) ? $context["csrf_token"] : $this->getContext($context, "csrf_token")), "html", null, true);
        echo "\" />
<div class=\"log-input\">
\t<div class=\"log-input-left\">   
    <input type=\"text\" id=\"username\" name=\"_username\"  value=\"";
        // line 27
        echo twig_escape_filter($this->env, (isset($context["last_username"]) ? $context["last_username"] : $this->getContext($context, "last_username")), "html", null, true);
        echo "\" required=\"required\" />
    </div>
          
<div class=\"clearfix\"> </div>
</div>
    
    <div class=\"log-input\">
\t<div class=\"log-input-left\">
    
    
    <input type=\"password\" id=\"password\" name=\"_password\"  required=\"required\" />
        </div>
       
    <div class=\"clearfix\"> </div>
<span class=\"checkbox1\">
    <div class=\"clearfix\"> </div>
    
    <input type=\"checkbox\" id=\"remember_me\" name=\"_remember_me\" value=\"on\" />
    <label for=\"remember_me\">";
        // line 45
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Login Remember_me"), "html", null, true);
        echo "</label>
</span>
</div>
<div class=\"clearfix\"> </div>
    <a href=\"";
        // line 49
        echo $this->env->getExtension('routing')->getPath("crowdrise_membre_homepage");
        echo "\"> 
<input type=\"submit\"   id=\"_submit\" name=\"_submit\" value=\"";
        // line 50
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Submit"), "html", null, true);
        echo "\" /></a>
</form>
                                        </div>
<div class=\"new_people\">
\t\t\t\t\t\t\t<h2>For New People</h2>
\t\t\t\t\t\t\t<p>Having hands on experience in creating innovative designs,I do offer design 
\t\t\t\t\t\t\t\tsolutions which harness.</p>
\t\t\t\t\t\t\t<a href=\"";
        // line 57
        echo $this->env->getExtension('routing')->getPath("fos_user_registration_register");
        echo "\">register Now</a>
\t\t\t\t\t\t</div>
</div>
                                </div>
                </div>
                                                        </div>
";
    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Security:login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  115 => 57,  105 => 50,  101 => 49,  94 => 45,  73 => 27,  67 => 24,  62 => 22,  52 => 14,  46 => 12,  44 => 11,  38 => 10,  35 => 9,  32 => 7,  29 => 5,  11 => 2,);
    }
}
/* */
/* {% extends "CrowdriseMembreBundle::layout1.html.twig" %}*/
/* */
/* */
/* {% block container %}*/
/* {#{% extends "FOSUserBundle::layout.html.twig" %}#}*/
/* */
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/* {% block fos_user_content %}*/
/* {% if error %}*/
/*     <div>{{ error.messageKey|trans(error.messageData, 'security') }}</div>*/
/* {% endif %}*/
/* 		<div id="page-wrapper" class="sign-in-wrapper">*/
/* 				<div class="graphs">*/
/* 					<div class="sign-in-form">*/
/* 						<div class="sign-in-form-top">*/
/* 							<h1>Log in</h1>*/
/* 						</div>*/
/*                                             <div class="signin">*/
/* 							*/
/* <form action="{{ path("fos_user_security_check") }}" method="post">*/
/*     */
/*     <input type="hidden" name="_csrf_token" value="{{ csrf_token }}" />*/
/* <div class="log-input">*/
/* 	<div class="log-input-left">   */
/*     <input type="text" id="username" name="_username"  value="{{ last_username }}" required="required" />*/
/*     </div>*/
/*           */
/* <div class="clearfix"> </div>*/
/* </div>*/
/*     */
/*     <div class="log-input">*/
/* 	<div class="log-input-left">*/
/*     */
/*     */
/*     <input type="password" id="password" name="_password"  required="required" />*/
/*         </div>*/
/*        */
/*     <div class="clearfix"> </div>*/
/* <span class="checkbox1">*/
/*     <div class="clearfix"> </div>*/
/*     */
/*     <input type="checkbox" id="remember_me" name="_remember_me" value="on" />*/
/*     <label for="remember_me">{{ 'Login Remember_me'|trans }}</label>*/
/* </span>*/
/* </div>*/
/* <div class="clearfix"> </div>*/
/*     <a href="{{path('crowdrise_membre_homepage')}}"> */
/* <input type="submit"   id="_submit" name="_submit" value="{{ 'Submit'|trans }}" /></a>*/
/* </form>*/
/*                                         </div>*/
/* <div class="new_people">*/
/* 							<h2>For New People</h2>*/
/* 							<p>Having hands on experience in creating innovative designs,I do offer design */
/* 								solutions which harness.</p>*/
/* 							<a href="{{path('fos_user_registration_register')}}">register Now</a>*/
/* 						</div>*/
/* </div>*/
/*                                 </div>*/
/*                 </div>*/
/*                                                         </div>*/
/* {% endblock fos_user_content %}*/
/* {% endblock %}*/
