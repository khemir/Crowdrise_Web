<?php

/* FOSUserBundle:Profile:AfficheProjet.html.twig */
class __TwigTemplate_1dc6d676f9ac644e6b8b6a8883f81a6b139e346f8bab06810f4842d3af26c7fa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("CrowdriseProfileBundle:Default:profile.html.twig", "FOSUserBundle:Profile:AfficheProjet.html.twig", 1);
        $this->blocks = array(
            'projet' => array($this, 'block_projet'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "CrowdriseProfileBundle:Default:profile.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_projet($context, array $blocks = array())
    {
        // line 3
        echo "

    <h1> Projects List</h1>
    ";
        // line 6
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["projets"]) ? $context["projets"] : $this->getContext($context, "projets")));
        foreach ($context['_seq'] as $context["_key"] => $context["projet"]) {
            // line 7
            echo "        
              ";
            // line 8
            echo twig_escape_filter($this->env, $this->getAttribute($context["projet"], "intituleProjet", array()), "html", null, true);
            echo "
            
            
            

       
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['projet'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 15
        echo "    
    ";
    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Profile:AfficheProjet.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  56 => 15,  43 => 8,  40 => 7,  36 => 6,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends "CrowdriseProfileBundle:Default:profile.html.twig" %}*/
/* {% block projet %}*/
/* */
/* */
/*     <h1> Projects List</h1>*/
/*     {% for projet in projets %}*/
/*         */
/*               {{projet.intituleProjet}}*/
/*             */
/*             */
/*             */
/* */
/*        */
/*     {% endfor %}*/
/*     */
/*     {% endblock %}*/
