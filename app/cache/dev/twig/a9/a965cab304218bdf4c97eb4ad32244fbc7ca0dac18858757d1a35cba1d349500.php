<?php

/* IbrowsXeditableBundle::xeditable.html.twig */
class __TwigTemplate_05972f8bc8255c8f9334969e40c53ec94941f8048d833e46175fa1e1dbf6b770 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'start_tag' => array($this, 'block_start_tag'),
            'attributes' => array($this, 'block_attributes'),
            'class' => array($this, 'block_class'),
            'value' => array($this, 'block_value'),
            'end_tag' => array($this, 'block_end_tag'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('start_tag', $context, $blocks);
        // line 4
        echo "    ";
        $this->displayBlock('attributes', $context, $blocks);
        // line 12
        echo ">
";
        // line 13
        $this->displayBlock('value', $context, $blocks);
        // line 16
        $this->displayBlock('end_tag', $context, $blocks);
    }

    // line 1
    public function block_start_tag($context, array $blocks = array())
    {
        // line 2
        echo "<a
";
    }

    // line 4
    public function block_attributes($context, array $blocks = array())
    {
        // line 5
        echo "        href=\"#\"
        class=\"";
        // line 6
        $this->displayBlock('class', $context, $blocks);
        echo "\"
        data-xeditable
        ";
        // line 8
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["attributes"]) ? $context["attributes"] : $this->getContext($context, "attributes")));
        foreach ($context['_seq'] as $context["key"] => $context["value"]) {
            // line 9
            echo "            ";
            echo twig_escape_filter($this->env, $context["key"], "html", null, true);
            echo "=\"";
            echo twig_escape_filter($this->env, $context["value"], "html", null, true);
            echo "\"
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['value'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 11
        echo "    ";
    }

    // line 6
    public function block_class($context, array $blocks = array())
    {
        echo "ibrowsXeditable";
    }

    // line 13
    public function block_value($context, array $blocks = array())
    {
        // line 14
        echo "    ";
        echo twig_escape_filter($this->env, (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")), "html", null, true);
        echo "
";
    }

    // line 16
    public function block_end_tag($context, array $blocks = array())
    {
        // line 17
        echo "</a>
";
    }

    public function getTemplateName()
    {
        return "IbrowsXeditableBundle::xeditable.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  95 => 17,  92 => 16,  85 => 14,  82 => 13,  76 => 6,  72 => 11,  61 => 9,  57 => 8,  52 => 6,  49 => 5,  46 => 4,  41 => 2,  38 => 1,  34 => 16,  32 => 13,  29 => 12,  26 => 4,  24 => 1,);
    }
}
/* {% block start_tag %}*/
/* <a*/
/* {% endblock %}*/
/*     {% block attributes %}*/
/*         href="#"*/
/*         class="{% block class %}ibrowsXeditable{% endblock %}"*/
/*         data-xeditable*/
/*         {% for key, value in attributes %}*/
/*             {{ key }}="{{ value }}"*/
/*         {% endfor %}*/
/*     {% endblock %}*/
/* >*/
/* {% block value %}*/
/*     {{ value }}*/
/* {% endblock %}*/
/* {% block end_tag %}*/
/* </a>*/
/* {% endblock %}*/
