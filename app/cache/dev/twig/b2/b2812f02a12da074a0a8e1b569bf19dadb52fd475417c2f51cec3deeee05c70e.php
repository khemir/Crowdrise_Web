<?php

/* FOSUserBundle:Registration:register.html.twig */
class __TwigTemplate_e766c5e3cd5a6d2143e59d84e1eb3f2df1f7ccd0a1686eae6fce6e63ed366b78 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("CrowdriseMembreBundle::layout.html.twig", "FOSUserBundle:Registration:register.html.twig", 2);
        $this->blocks = array(
            'container' => array($this, 'block_container'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "CrowdriseMembreBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_container($context, array $blocks = array())
    {
        // line 4
        echo "
";
        // line 5
        $this->displayBlock('fos_user_content', $context, $blocks);
    }

    public function block_fos_user_content($context, array $blocks = array())
    {
        // line 6
        $this->loadTemplate("FOSUserBundle:Registration:register_content.html.twig", "FOSUserBundle:Registration:register.html.twig", 6)->display($context);
    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  41 => 6,  35 => 5,  32 => 4,  29 => 3,  11 => 2,);
    }
}
/* {#{% extends "FOSUserBundle::layout.html.twig" %}#}*/
/* {% extends "CrowdriseMembreBundle::layout.html.twig" %}*/
/* {% block container %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:Registration:register_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* {% endblock  %}*/
/* */
