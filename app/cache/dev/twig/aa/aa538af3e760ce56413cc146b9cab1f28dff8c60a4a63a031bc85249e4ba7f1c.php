<?php

/* IbrowsXeditableBundle::xeditableformerrors.html.twig */
class __TwigTemplate_78684d682ec625bf3e9b08fd68a4b146ff354365fc4b255b6ff07e6e3b2fcd56 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        ob_start();
        // line 2
        echo "    ";
        $context["printer"] = $this;
        // line 3
        echo "    ";
        echo $context["printer"]->getprintError((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")));
        echo "
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        // line 5
        echo "
";
    }

    // line 6
    public function getprintError($__form__ = null)
    {
        $context = $this->env->mergeGlobals(array(
            "form" => $__form__,
            "varargs" => func_num_args() > 1 ? array_slice(func_get_args(), 1) : array(),
        ));

        $blocks = array();

        ob_start();
        try {
            // line 7
            echo "    ";
            $context["printer"] = $this;
            // line 8
            echo "    ";
            ob_start();
            // line 9
            echo "        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars", array(), "any", false, true), "errors", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars", array(), "any", false, true), "errors", array()), array())) : (array())));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 10
                echo "            <div class=\"xeditable-error\">";
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'label');
                echo ": ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["error"], "message", array()), "html", null, true);
                echo "</div>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 12
            echo "        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")));
            foreach ($context['_seq'] as $context["_key"] => $context["subform"]) {
                // line 13
                echo "            ";
                echo $context["printer"]->getprintError($context["subform"]);
                echo "
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subform'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 15
            echo "    ";
            echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    public function getTemplateName()
    {
        return "IbrowsXeditableBundle::xeditableformerrors.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  83 => 15,  74 => 13,  69 => 12,  58 => 10,  53 => 9,  50 => 8,  47 => 7,  35 => 6,  30 => 5,  24 => 3,  21 => 2,  19 => 1,);
    }
}
/* {% spaceless %}*/
/*     {% import _self as printer %}*/
/*     {{ printer.printError(form) }}*/
/* {% endspaceless %}*/
/* */
/* {% macro printError(form) %}*/
/*     {% import _self as printer %}*/
/*     {% spaceless %}*/
/*         {% for error in form.vars.errors|default([]) %}*/
/*             <div class="xeditable-error">{{ form_label(form) }}: {{ error.message }}</div>*/
/*         {% endfor %}*/
/*         {% for subform in form %}*/
/*             {{ printer.printError(subform) }}*/
/*         {% endfor %}*/
/*     {% endspaceless %}*/
/* {% endmacro %}*/
/* */
