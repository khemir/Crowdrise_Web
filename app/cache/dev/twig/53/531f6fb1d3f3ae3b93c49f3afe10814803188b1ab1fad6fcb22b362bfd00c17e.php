<?php

/* FOSUserBundle:Profile:edit.html.twig */
class __TwigTemplate_153f2bbfb8ba781ce6a3f23d68a4f8c662e21ddacdef91b345bf4e3fa4f79f01 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("CrowdriseMembreBundle:Default:register.html.twig", "FOSUserBundle:Profile:edit.html.twig", 2);
        $this->blocks = array(
            'container' => array($this, 'block_container'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "CrowdriseMembreBundle:Default:register.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_container($context, array $blocks = array())
    {
        // line 4
        $this->displayBlock('fos_user_content', $context, $blocks);
        // line 7
        echo "
";
    }

    // line 4
    public function block_fos_user_content($context, array $blocks = array())
    {
        // line 5
        $this->loadTemplate("FOSUserBundle:Profile:edit_content.html.twig", "FOSUserBundle:Profile:edit.html.twig", 5)->display($context);
    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Profile:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 5,  39 => 4,  34 => 7,  32 => 4,  29 => 3,  11 => 2,);
    }
}
/* {#{% extends "FOSUserBundle::layout.html.twig" %}#}*/
/* {% extends "CrowdriseMembreBundle:Default:register.html.twig" %}*/
/* {% block container %}*/
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:Profile:edit_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
/* {% endblock  %}*/
