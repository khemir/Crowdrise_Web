<?php

/* FOSUserBundle:Registration:confirmed.html.twig */
class __TwigTemplate_36ed26afa55060b0444997d0c35ce355e50e5c2b42d396669d3670875b1d2e98 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 4
        $this->parent = $this->loadTemplate("CrowdriseMembreBundle::layout.html.twig", "FOSUserBundle:Registration:confirmed.html.twig", 4);
        $this->blocks = array(
            'container' => array($this, 'block_container'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "CrowdriseMembreBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_container($context, array $blocks = array())
    {
        // line 6
        echo "    
  
";
        // line 8
        $this->displayBlock('fos_user_content', $context, $blocks);
        // line 56
        echo "


";
    }

    // line 8
    public function block_fos_user_content($context, array $blocks = array())
    {
        // line 9
        echo "    <strong><center><p>";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Registration confirmed", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array())), "FOSUserBundle"), "html", null, true);
        echo "</p></center></strong>
   ";
        // line 13
        echo "   <div>
        
                            
                         ";
        // line 23
        echo "            ";
        if ($this->env->getExtension('security')->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
            // line 24
            echo "        <strong><center>  ";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Logged in as %username%", array("%username%" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "username", array())), "FOSUserBundle"), "html", null, true);
            echo " </center></strong>
        <div class=\"header\">    
       ";
            // line 33
            echo "            ";
        } else {
            // line 34
            echo "                <a href=\"";
            echo $this->env->getExtension('routing')->getPath("fos_user_security_login");
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("layout.login", array(), "FOSUserBundle"), "html", null, true);
            echo "</a>
            ";
        }
        // line 36
        echo "        </div>
   </div>

      ";
        // line 48
        echo "  <div class=\"main-banner banner text-center\">
\t  <div class=\"container\">    
\t\t\t<h1>Keep up your idea   <span class=\"segment-heading\"> , Keep up </span>  your work.</h1>
\t\t\t<p>Crowdrise is the best site for ...</p>
\t\t\t<a href=\"";
        // line 52
        echo $this->env->getExtension('routing')->getPath("fos_user_profile_show");
        echo "\">Get started</a>
\t  </div>
\t</div>
";
    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:confirmed.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  89 => 52,  83 => 48,  78 => 36,  70 => 34,  67 => 33,  61 => 24,  58 => 23,  53 => 13,  48 => 9,  45 => 8,  38 => 56,  36 => 8,  32 => 6,  29 => 5,  11 => 4,);
    }
}
/* {#{% extends "FOSUserBundle::layout.html.twig" %}#}*/
/* */
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* {% extends "CrowdriseMembreBundle::layout.html.twig" %}*/
/* {% block container %}*/
/*     */
/*   */
/* {% block fos_user_content %}*/
/*     <strong><center><p>{{ 'Registration confirmed'|trans({'%username%': user.username}) }}</p></center></strong>*/
/*    {# {% if targetUrl %}*/
/*     <p><a href="{{ targetUrl }}">{{ 'registration.back'|trans }}</a></p>*/
/*     {% endif %} #}*/
/*    <div>*/
/*         */
/*                             */
/*                          {#   <a class="account" href="{{path('fos_user_security_login')}}"><strong>Logout</strong></a>*/
/* */
/*                         <span class="active uls-trigger"></span>*/
/*                         */
/*                         */
/* */
/* 		</div>#}*/
/*             {% if is_granted("IS_AUTHENTICATED_REMEMBERED") %}*/
/*         <strong><center>  {{ 'Logged in as %username%'|trans({'%username%': app.user.username}, 'FOSUserBundle') }} </center></strong>*/
/*         <div class="header">    */
/*        {# <div class="header-right">*/
/*             <span class="active uls-trigger"></span>*/
/*                 <a class="account" href="{{path('fos_user_security_logout')}}"><strong >Logout</strong>*/
/*                     */
/*                   {#  {{ 'layout.logout'|trans({}, 'FOSUserBundle') }}*/
/*                 </a>*/
/*             </div>#}*/
/*             {% else %}*/
/*                 <a href="{{ path('fos_user_security_login') }}">{{ 'layout.login'|trans({}, 'FOSUserBundle') }}</a>*/
/*             {% endif %}*/
/*         </div>*/
/*    </div>*/
/* */
/*       {#  {% if app.request.hasPreviousSession %}*/
/*             {% for type, messages in app.session.flashbag.all() %}*/
/*                 {% for message in messages %}*/
/*                     <div class="flash-{{ type }}">*/
/*                         {{ message }}*/
/*                     </div>*/
/*                 {% endfor %}*/
/*             {% endfor %}*/
/*         {% endif %}#}*/
/*   <div class="main-banner banner text-center">*/
/* 	  <div class="container">    */
/* 			<h1>Keep up your idea   <span class="segment-heading"> , Keep up </span>  your work.</h1>*/
/* 			<p>Crowdrise is the best site for ...</p>*/
/* 			<a href="{{path('fos_user_profile_show')}}">Get started</a>*/
/* 	  </div>*/
/* 	</div>*/
/* {% endblock fos_user_content %}*/
/* */
/* */
/* */
/* {% endblock  %}*/
/* */
