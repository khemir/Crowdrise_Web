<?php

/* CrowdriseMembreBundle::login.html.twig */
class __TwigTemplate_3c68f7c6dbc2c090617352348582518d0f3676726229241201c0bd3caf6717b3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'linkcss' => array($this, 'block_linkcss'),
            'js' => array($this, 'block_js'),
            'header' => array($this, 'block_header'),
            'container' => array($this, 'block_container'),
            'footer' => array($this, 'block_footer'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
";
        // line 3
        $this->displayBlock('head', $context, $blocks);
        // line 80
        echo "
    
<body>
";
        // line 83
        $this->displayBlock('header', $context, $blocks);
        // line 106
        echo "
        ";
        // line 107
        $this->displayBlock('container', $context, $blocks);
        // line 343
        echo "
\t\t<!--footer section start-->\t
                
                ";
        // line 346
        $this->displayBlock('footer', $context, $blocks);
        // line 424
        echo "
        <!--footer section end-->
</body>
</html>";
    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        // line 4
        echo "<head>
    
";
        // line 6
        $this->displayBlock('title', $context, $blocks);
        // line 11
        echo "
";
        // line 12
        $this->displayBlock('linkcss', $context, $blocks);
        // line 24
        echo "
<!-- for-mobile-apps -->
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
<meta name=\"keywords\" content=\"Resale Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola web design\" />
<script type=\"application/x-javascript\"> addEventListener(\"load\", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
<!--fonts-->
<link href='//fonts.googleapis.com/css?family=Ubuntu+Condensed' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<!--//fonts-->\t
<!-- js -->

";
        // line 38
        $this->displayBlock('js', $context, $blocks);
        // line 76
        echo "

</head>
";
    }

    // line 6
    public function block_title($context, array $blocks = array())
    {
        // line 7
        echo "
<title>Crowdrise</title>

";
    }

    // line 12
    public function block_linkcss($context, array $blocks = array())
    {
        // line 13
        echo "
<link rel=\"stylesheet\" href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bootstrap.min.css"), "html", null, true);
        echo "\">
<link rel=\"stylesheet\" href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bootstrap-select.css"), "html", null, true);
        echo "\">
<link href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/style.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
<link rel=\"stylesheet\" href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/flexslider.css"), "html", null, true);
        echo "\" type=\"text/css\" media=\"screen\" />
<link rel=\"stylesheet\" href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/font-awesome.min.css"), "html", null, true);
        echo "\" />
<link href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/jquery.uls.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"/>
<link href=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/jquery.uls.grid.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"/>
<link href=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/jquery.uls.lcd.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"/>

";
    }

    // line 38
    public function block_js($context, array $blocks = array())
    {
        // line 39
        echo "
<script type=\"text/javascript\" src=\"";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/bootstrap-select.js"), "html", null, true);
        echo "\"></script>

<script>
  \$(document).ready(function () {
    var mySelect = \$('#first-disabled2');

    \$('#special').on('click', function () {
      mySelect.find('option:selected').prop('disabled', true);
      mySelect.selectpicker('refresh');
    });

    \$('#special2').on('click', function () {
      mySelect.find('option:disabled').prop('disabled', false);
      mySelect.selectpicker('refresh');
    });

    \$('#basic2').selectpicker({
      liveSearch: true,
      maxOptions: 1
    });
  });
</script>

<script type=\"text/javascript\" src=\"";
        // line 65
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.leanModal.min.js"), "html", null, true);
        echo "\"></script>

<script src=\"";
        // line 67
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.uls.data.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 68
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.uls.data.utils.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 69
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.uls.lcd.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 70
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.uls.languagefilter.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 71
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.uls.regionfilter.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 72
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.uls.core.js"), "html", null, true);
        echo "\"></script>


";
    }

    // line 83
    public function block_header($context, array $blocks = array())
    {
        // line 84
        echo "
\t<div class=\"header\">
\t\t<div class=\"container\">
\t\t\t<div class=\"logo\">
                            <a href=\"";
        // line 88
        echo $this->env->getExtension('routing')->getPath("crowdrise_membre_homepage");
        echo "\"><span><strong>Crowd</strong></span><strong>rise</strong></a>
\t\t\t</div>
                        
\t\t\t<div class=\"header-right\">
                            
<a class=\"account\" href=\"";
        // line 93
        echo $this->env->getExtension('routing')->getPath("fos_user_security_logout");
        echo "\"><strong>Logout</strong></a>
                        ";
        // line 95
        echo "                        <span class=\"active uls-trigger\"></span>
                        
                       <a class=\"account\" href=\"";
        // line 97
        echo $this->env->getExtension('routing')->getPath("fos_user_profile_show");
        echo "\"><strong>Profile</strong></a>
                        <span class=\"active uls-trigger\"></span>
                        
                       ";
        // line 101
        echo "
\t\t</div>
\t\t</div>
\t</div>
    ";
    }

    // line 107
    public function block_container($context, array $blocks = array())
    {
        // line 108
        echo "\t<div class=\"main-banner banner text-center\">
\t  <div class=\"container\">    
\t\t\t<h1>Keep up your idea   <span class=\"segment-heading\"> , Keep up </span>  your work.</h1>
\t\t\t<p>Crowdrise is the best site for ...</p>
\t\t\t<a href=\"";
        // line 112
        echo $this->env->getExtension('routing')->getPath("fos_user_profile_show");
        echo "\">Get started</a>
\t  </div>
\t</div>
\t\t<!-- content-starts-here -->
\t\t<div class=\"content\">
\t\t\t<div class=\"categories\">
\t\t\t\t<div class=\"container\">
                                    <center><div class=\"col-md-2 focus-grid\">
\t\t\t\t\t\t<a href=\"";
        // line 120
        echo $this->env->getExtension('routing')->getPath("crowdrise_idees_homepage");
        echo "\">
\t\t\t\t\t\t\t<div class=\"focus-border\">
\t\t\t\t\t\t\t\t<div class=\"focus-layout\">
\t\t\t\t\t\t\t\t\t<div class=\"focus-image\"><i class=\"fa fa-mobile\"></i></div>
\t\t\t\t\t\t\t\t\t<h4 class=\"clrchg\">Ideas</h4>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</a>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"col-md-2 focus-grid\">
\t\t\t\t\t\t<a href=\"";
        // line 131
        echo $this->env->getExtension('routing')->getPath("crowdrise_projets_homepage");
        echo "\">
\t\t\t\t\t\t\t<div class=\"focus-border\">
\t\t\t\t\t\t\t\t<div class=\"focus-layout\">
\t\t\t\t\t\t\t\t\t<div class=\"focus-image\"><i class=\"fa fa-car\"></i></div>
\t\t\t\t\t\t\t\t\t<h4 class=\"clrchg\">Projects</h4>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</a>
\t\t\t\t\t</div>\t
\t\t\t\t\t
\t\t\t\t\t<div class=\"col-md-2 focus-grid\">
\t\t\t\t\t\t<a href=\"";
        // line 142
        echo $this->env->getExtension('routing')->getPath("crowdrise_problemes_homepage");
        echo "\">
\t\t\t\t\t\t\t<div class=\"focus-border\">
\t\t\t\t\t\t\t\t<div class=\"focus-layout\">
\t\t\t\t\t\t\t\t\t<div class=\"focus-image\"><i class=\"fa fa-paw\"></i></div>
\t\t\t\t\t\t\t\t\t<h4 class=\"clrchg\">Stack'Probs</h4>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</a>
\t\t\t\t\t</div>\t
                                    </center>
\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"trending-ads\">
\t\t\t\t<div class=\"container\">
\t\t\t\t<!-- slider -->
\t\t\t\t<div class=\"trend-ads\">
\t\t\t\t\t<h2>Last work</h2>
\t\t\t\t\t\t\t<ul id=\"flexiselDemo3\">
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<div class=\"col-md-3 biseller-column\">
\t\t\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 164
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/p1.jpg"), "html", null, true);
        echo "\"/>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"price\">&#36; 450</span>
\t\t\t\t\t\t\t\t\t\t</a> 
\t\t\t\t\t\t\t\t\t\t<div class=\"ad-info\">
\t\t\t\t\t\t\t\t\t\t\t<h5>There are many variations of passages</h5>
\t\t\t\t\t\t\t\t\t\t\t<span>1 hour ago</span>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"col-md-3 biseller-column\">
\t\t\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 174
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/p2.jpg"), "html", null, true);
        echo "\"/>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"price\">&#36; 399</span>
\t\t\t\t\t\t\t\t\t\t</a> 
\t\t\t\t\t\t\t\t\t\t<div class=\"ad-info\">
\t\t\t\t\t\t\t\t\t\t\t<h5>Lorem Ipsum is simply dummy</h5>
\t\t\t\t\t\t\t\t\t\t\t<span>3 hour ago</span>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"col-md-3 biseller-column\">
\t\t\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 184
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/p3.jpg"), "html", null, true);
        echo "\"/>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"price\">&#36; 199</span>
\t\t\t\t\t\t\t\t\t\t</a> 
\t\t\t\t\t\t\t\t\t\t<div class=\"ad-info\">
\t\t\t\t\t\t\t\t\t\t\t<h5>It is a long established fact that a reader</h5>
\t\t\t\t\t\t\t\t\t\t\t<span>8 hour ago</span>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"col-md-3 biseller-column\">
\t\t\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 194
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/p4.jpg"), "html", null, true);
        echo "\"/>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"price\">&#36; 159</span>
\t\t\t\t\t\t\t\t\t\t</a> 
\t\t\t\t\t\t\t\t\t\t<div class=\"ad-info\">
\t\t\t\t\t\t\t\t\t\t\t<h5>passage of Lorem Ipsum you need to be</h5>
\t\t\t\t\t\t\t\t\t\t\t<span>19 hour ago</span>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<div class=\"col-md-3 biseller-column\">
\t\t\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 206
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/p5.jpg"), "html", null, true);
        echo "\"/>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"price\">&#36; 1599</span>
\t\t\t\t\t\t\t\t\t\t</a> 
\t\t\t\t\t\t\t\t\t\t<div class=\"ad-info\">
\t\t\t\t\t\t\t\t\t\t\t<h5>There are many variations of passages</h5>
\t\t\t\t\t\t\t\t\t\t\t<span>1 hour ago</span>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"col-md-3 biseller-column\">
\t\t\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 216
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/p6.jpg"), "html", null, true);
        echo "\"/>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"price\">&#36; 1099</span>
\t\t\t\t\t\t\t\t\t\t</a> 
\t\t\t\t\t\t\t\t\t\t<div class=\"ad-info\">
\t\t\t\t\t\t\t\t\t\t\t<h5>passage of Lorem Ipsum you need to be</h5>
\t\t\t\t\t\t\t\t\t\t\t<span>1 day ago</span>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"col-md-3 biseller-column\">
\t\t\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 226
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/p7.jpg"), "html", null, true);
        echo "\"/>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"price\">&#36; 109</span>
\t\t\t\t\t\t\t\t\t\t</a> 
\t\t\t\t\t\t\t\t\t\t<div class=\"ad-info\">
\t\t\t\t\t\t\t\t\t\t\t<h5>It is a long established fact that a reader</h5>
\t\t\t\t\t\t\t\t\t\t\t<span>9 hour ago</span>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"col-md-3 biseller-column\">
\t\t\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 236
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/p8.jpg"), "html", null, true);
        echo "\"/>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"price\">&#36; 189</span>
\t\t\t\t\t\t\t\t\t\t</a> 
\t\t\t\t\t\t\t\t\t\t<div class=\"ad-info\">
\t\t\t\t\t\t\t\t\t\t\t<h5>Lorem Ipsum is simply dummy</h5>
\t\t\t\t\t\t\t\t\t\t\t<span>3 hour ago</span>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<div class=\"col-md-3 biseller-column\">
\t\t\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 248
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/p9.jpg"), "html", null, true);
        echo "\"/>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"price\">&#36; 2599</span>
\t\t\t\t\t\t\t\t\t\t</a> 
\t\t\t\t\t\t\t\t\t\t<div class=\"ad-info\">
\t\t\t\t\t\t\t\t\t\t\t<h5>Lorem Ipsum is simply dummy</h5>
\t\t\t\t\t\t\t\t\t\t\t<span>3 hour ago</span>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"col-md-3 biseller-column\">
\t\t\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 258
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/p10.jpg"), "html", null, true);
        echo "\"/>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"price\">&#36; 3999</span>
\t\t\t\t\t\t\t\t\t\t</a> 
\t\t\t\t\t\t\t\t\t\t<div class=\"ad-info\">
\t\t\t\t\t\t\t\t\t\t\t<h5>It is a long established fact that a reader</h5>
\t\t\t\t\t\t\t\t\t\t\t<span>9 hour ago</span>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"col-md-3 biseller-column\">
\t\t\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 268
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/p11.jpg"), "html", null, true);
        echo "\"/>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"price\">&#36; 2699</span>
\t\t\t\t\t\t\t\t\t\t</a> 
\t\t\t\t\t\t\t\t\t\t<div class=\"ad-info\">
\t\t\t\t\t\t\t\t\t\t\t<h5>passage of Lorem Ipsum you need to be</h5>
\t\t\t\t\t\t\t\t\t\t\t<span>1 day ago</span>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"col-md-3 biseller-column\">
\t\t\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 278
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/p12.jpg"), "html", null, true);
        echo "\"/>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"price\">&#36; 899</span>
\t\t\t\t\t\t\t\t\t\t</a> 
\t\t\t\t\t\t\t\t\t\t<div class=\"ad-info\">
\t\t\t\t\t\t\t\t\t\t\t<h5>There are many variations of passages</h5>
\t\t\t\t\t\t\t\t\t\t\t<span>1 hour ago</span>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t<script type=\"text/javascript\">
\t\t\t\t\t\t \$(window).load(function() {
\t\t\t\t\t\t\t\$(\"#flexiselDemo3\").flexisel({
\t\t\t\t\t\t\t\tvisibleItems:1,
\t\t\t\t\t\t\t\tanimationSpeed: 1000,
\t\t\t\t\t\t\t\tautoPlay: true,
\t\t\t\t\t\t\t\tautoPlaySpeed: 5000,    \t\t
\t\t\t\t\t\t\t\tpauseOnHover: true,
\t\t\t\t\t\t\t\tenableResponsiveBreakpoints: true,
\t\t\t\t\t\t\t\tresponsiveBreakpoints: { 
\t\t\t\t\t\t\t\t\tportrait: { 
\t\t\t\t\t\t\t\t\t\tchangePoint:480,
\t\t\t\t\t\t\t\t\t\tvisibleItems:1
\t\t\t\t\t\t\t\t\t}, 
\t\t\t\t\t\t\t\t\tlandscape: { 
\t\t\t\t\t\t\t\t\t\tchangePoint:640,
\t\t\t\t\t\t\t\t\t\tvisibleItems:1
\t\t\t\t\t\t\t\t\t},
\t\t\t\t\t\t\t\t\ttablet: { 
\t\t\t\t\t\t\t\t\t\tchangePoint:768,
\t\t\t\t\t\t\t\t\t\tvisibleItems:1
\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t
\t\t\t\t\t\t});
\t\t\t\t\t   </script>
\t\t\t\t\t   <script type=\"text/javascript\" src=\"";
        // line 315
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.flexisel.js"), "html", null, true);
        echo "\"></script>
\t\t\t\t\t</div>   
\t\t\t</div>
\t\t\t<!-- //slider -->\t\t\t\t
\t\t\t</div>
\t\t\t<div class=\"mobile-app\">
\t\t\t\t<div class=\"container\">
\t\t\t\t\t<div class=\"col-md-5 app-left\">
\t\t\t\t\t\t<a href=\"mobileapp.html\"><img src=\"";
        // line 323
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/app.png"), "html", null, true);
        echo "\" alt=\"\"></a>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-7 app-right\">
\t\t\t\t\t\t<h3>Resale App is the <span>Easiest</span> way for Selling and buying second-hand goods</h3>
\t\t\t\t\t\t<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam auctor Sed bibendum varius euismod. Integer eget turpis sit amet lorem rutrum ullamcorper sed sed dui. vestibulum odio at elementum. Suspendisse et condimentum nibh.</p>
\t\t\t\t\t\t<div class=\"app-buttons\">
\t\t\t\t\t\t\t<div class=\"app-button\">
\t\t\t\t\t\t\t\t<a href=\"#\"><img src=\"";
        // line 330
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/1.png"), "html", null, true);
        echo "\" alt=\"\"></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"app-button\">
\t\t\t\t\t\t\t\t<a href=\"#\"><img src=\"";
        // line 333
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/2.png"), "html", null, true);
        echo "\" alt=\"\"></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"clearfix\"> </div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
                ";
    }

    // line 346
    public function block_footer($context, array $blocks = array())
    {
        // line 347
        echo "
\t\t<footer>
\t\t\t<div class=\"footer-top\">
\t\t\t\t<div class=\"container\">
\t\t\t\t\t<div class=\"foo-grids\">
\t\t\t\t\t\t<div class=\"col-md-3 footer-grid\">
\t\t\t\t\t\t\t<h4 class=\"footer-head\">Who We Are</h4>
\t\t\t\t\t\t\t<p>CrowdRise is the most innovative, modern crowdfunding platform for charitable causes.</p>
\t\t\t\t\t\t\t<p>With our plateform you will be able to raise money for your cause and keep 100% of what you raise..</p>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"col-md-3 footer-grid\">
\t\t\t\t\t\t\t<h4 class=\"footer-head\">Help</h4>
\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t<li><a href=\"howitworks.html\">How it Works</a></li>\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<li><a href=\"sitemap.html\">Sitemap</a></li>
\t\t\t\t\t\t\t\t<li><a href=\"faq.html\">Faq</a></li>
\t\t\t\t\t\t\t\t<li><a href=\"feedback.html\">Feedback</a></li>
\t\t\t\t\t\t\t\t<li><a href=\"contact.html\">Contact</a></li>
\t\t\t\t\t\t\t\t<li><a href=\"typography.html\">Shortcodes</a></li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"col-md-3 footer-grid\">
\t\t\t\t\t\t\t<h4 class=\"footer-head\">Information</h4>
\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t<li><a href=\"regions.html\">Locations Map</a></li>\t
\t\t\t\t\t\t\t\t<li><a href=\"terms.html\">Terms of Use</a></li>
\t\t\t\t\t\t\t\t<li><a href=\"popular-search.html\">Popular searches</a></li>\t
\t\t\t\t\t\t\t\t<li><a href=\"privacy.html\">Privacy Policy</a></li>\t
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"col-md-3 footer-grid\">
\t\t\t\t\t\t\t<h4 class=\"footer-head\">Contact Us</h4>
\t\t\t\t\t\t\t<span class=\"hq\">DevXTeam</span>
\t\t\t\t\t\t\t<address>
\t\t\t\t\t\t\t\t<ul class=\"location\">
\t\t\t\t\t\t\t\t\t<li><span class=\"glyphicon glyphicon-map-marker\"></span></li>
\t\t\t\t\t\t\t\t\t<li>Esprit: Technopôle El Gazala, 2083, Tunisie</li>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t</ul>\t
\t\t\t\t\t\t\t\t<ul class=\"location\">
\t\t\t\t\t\t\t\t\t<li><span class=\"glyphicon glyphicon-earphone\"></span></li>
\t\t\t\t\t\t\t\t\t<li>+(216) 94556903</li>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t</ul>\t
\t\t\t\t\t\t\t\t<ul class=\"location\">
\t\t\t\t\t\t\t\t\t<li><span class=\"glyphicon glyphicon-envelope\"></span></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"mailto:info@example.com\">marwamimouni9@gmail.com</a></li>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t</ul>\t\t\t\t\t\t
\t\t\t\t\t\t\t</address>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t</div>\t\t\t\t\t\t
\t\t\t\t</div>\t
\t\t\t</div>\t
\t\t\t<div class=\"footer-bottom text-center\">
\t\t\t<div class=\"container\">
\t\t\t\t<div class=\"footer-logo\">
\t\t\t\t\t<a href=\"index.html\"><span>Re</span>sale</a>
\t\t\t\t</div>
\t\t\t\t<div class=\"footer-social-icons\">
\t\t\t\t\t<ul>
\t\t\t\t\t\t<li><a class=\"facebook\" href=\"#\"><span>Facebook</span></a></li>
\t\t\t\t\t\t<li><a class=\"twitter\" href=\"#\"><span>Twitter</span></a></li>
\t\t\t\t\t\t<li><a class=\"flickr\" href=\"#\"><span>Flickr</span></a></li>
\t\t\t\t\t\t<li><a class=\"googleplus\" href=\"#\"><span>Google+</span></a></li>
\t\t\t\t\t\t<li><a class=\"dribbble\" href=\"#\"><span>Dribbble</span></a></li>
\t\t\t\t\t</ul>
\t\t\t\t</div>
\t\t\t\t<div class=\"copyrights\">
\t\t\t\t\t<p> © 2016 Esprit. All Rights Reserved | Developed by  <a href=\"http://w3layouts.com/\"> DevX Team</a></p>
\t\t\t\t</div>
\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t</div>
\t\t</div>
\t\t</footer>
               ";
    }

    public function getTemplateName()
    {
        return "CrowdriseMembreBundle::login.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  564 => 347,  561 => 346,  547 => 333,  541 => 330,  531 => 323,  520 => 315,  480 => 278,  467 => 268,  454 => 258,  441 => 248,  426 => 236,  413 => 226,  400 => 216,  387 => 206,  372 => 194,  359 => 184,  346 => 174,  333 => 164,  308 => 142,  294 => 131,  280 => 120,  269 => 112,  263 => 108,  260 => 107,  252 => 101,  246 => 97,  242 => 95,  238 => 93,  230 => 88,  224 => 84,  221 => 83,  213 => 72,  209 => 71,  205 => 70,  201 => 69,  197 => 68,  193 => 67,  188 => 65,  162 => 42,  158 => 41,  154 => 40,  151 => 39,  148 => 38,  141 => 21,  137 => 20,  133 => 19,  129 => 18,  125 => 17,  121 => 16,  117 => 15,  113 => 14,  110 => 13,  107 => 12,  100 => 7,  97 => 6,  90 => 76,  88 => 38,  72 => 24,  70 => 12,  67 => 11,  65 => 6,  61 => 4,  58 => 3,  51 => 424,  49 => 346,  44 => 343,  42 => 107,  39 => 106,  37 => 83,  32 => 80,  30 => 3,  26 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/* {% block head %}*/
/* <head>*/
/*     */
/* {% block title %}*/
/* */
/* <title>Crowdrise</title>*/
/* */
/* {% endblock %}*/
/* */
/* {% block linkcss %}*/
/* */
/* <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">*/
/* <link rel="stylesheet" href="{{asset('css/bootstrap-select.css')}}">*/
/* <link href="{{asset('css/style.css')}}" rel="stylesheet" type="text/css" media="all" />*/
/* <link rel="stylesheet" href="{{asset('css/flexslider.css')}}" type="text/css" media="screen" />*/
/* <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}" />*/
/* <link href="{{asset('css/jquery.uls.css')}}" rel="stylesheet"/>*/
/* <link href="{{asset('css/jquery.uls.grid.css')}}" rel="stylesheet"/>*/
/* <link href="{{asset('css/jquery.uls.lcd.css')}}" rel="stylesheet"/>*/
/* */
/* {% endblock %}*/
/* */
/* <!-- for-mobile-apps -->*/
/* <meta name="viewport" content="width=device-width, initial-scale=1">*/
/* <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />*/
/* <meta name="keywords" content="Resale Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, */
/* Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola web design" />*/
/* <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>*/
/* <!-- //for-mobile-apps -->*/
/* <!--fonts-->*/
/* <link href='//fonts.googleapis.com/css?family=Ubuntu+Condensed' rel='stylesheet' type='text/css'>*/
/* <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>*/
/* <!--//fonts-->	*/
/* <!-- js -->*/
/* */
/* {% block js %}*/
/* */
/* <script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script>*/
/* <script src="{{asset('js/bootstrap.min.js')}}"></script>*/
/* <script src="{{asset('js/bootstrap-select.js')}}"></script>*/
/* */
/* <script>*/
/*   $(document).ready(function () {*/
/*     var mySelect = $('#first-disabled2');*/
/* */
/*     $('#special').on('click', function () {*/
/*       mySelect.find('option:selected').prop('disabled', true);*/
/*       mySelect.selectpicker('refresh');*/
/*     });*/
/* */
/*     $('#special2').on('click', function () {*/
/*       mySelect.find('option:disabled').prop('disabled', false);*/
/*       mySelect.selectpicker('refresh');*/
/*     });*/
/* */
/*     $('#basic2').selectpicker({*/
/*       liveSearch: true,*/
/*       maxOptions: 1*/
/*     });*/
/*   });*/
/* </script>*/
/* */
/* <script type="text/javascript" src="{{asset('js/jquery.leanModal.min.js')}}"></script>*/
/* */
/* <script src="{{asset('js/jquery.uls.data.js')}}"></script>*/
/* <script src="{{asset('js/jquery.uls.data.utils.js')}}"></script>*/
/* <script src="{{asset('js/jquery.uls.lcd.js')}}"></script>*/
/* <script src="{{asset('js/jquery.uls.languagefilter.js')}}"></script>*/
/* <script src="{{asset('js/jquery.uls.regionfilter.js')}}"></script>*/
/* <script src="{{asset('js/jquery.uls.core.js')}}"></script>*/
/* */
/* */
/* {% endblock %}*/
/* */
/* */
/* </head>*/
/* {% endblock %}*/
/* */
/*     */
/* <body>*/
/* {% block header %}*/
/* */
/* 	<div class="header">*/
/* 		<div class="container">*/
/* 			<div class="logo">*/
/*                             <a href="{{path('crowdrise_membre_homepage')}}"><span><strong>Crowd</strong></span><strong>rise</strong></a>*/
/* 			</div>*/
/*                         */
/* 			<div class="header-right">*/
/*                             */
/* <a class="account" href="{{path('fos_user_security_logout')}}"><strong>Logout</strong></a>*/
/*                         {# <a class="account" href="{{path('fos_user_security_logout')}}"><strong >Logout</strong></a>#}*/
/*                         <span class="active uls-trigger"></span>*/
/*                         */
/*                        <a class="account" href="{{path('fos_user_profile_show')}}"><strong>Profile</strong></a>*/
/*                         <span class="active uls-trigger"></span>*/
/*                         */
/*                        {# <a class="account" href="{{path('crowdrise_profile_homepage')}}"><strong>Profile</strong></a>#}*/
/* */
/* 		</div>*/
/* 		</div>*/
/* 	</div>*/
/*     {% endblock %}*/
/* */
/*         {% block container %}*/
/* 	<div class="main-banner banner text-center">*/
/* 	  <div class="container">    */
/* 			<h1>Keep up your idea   <span class="segment-heading"> , Keep up </span>  your work.</h1>*/
/* 			<p>Crowdrise is the best site for ...</p>*/
/* 			<a href="{{path('fos_user_profile_show')}}">Get started</a>*/
/* 	  </div>*/
/* 	</div>*/
/* 		<!-- content-starts-here -->*/
/* 		<div class="content">*/
/* 			<div class="categories">*/
/* 				<div class="container">*/
/*                                     <center><div class="col-md-2 focus-grid">*/
/* 						<a href="{{path('crowdrise_idees_homepage')}}">*/
/* 							<div class="focus-border">*/
/* 								<div class="focus-layout">*/
/* 									<div class="focus-image"><i class="fa fa-mobile"></i></div>*/
/* 									<h4 class="clrchg">Ideas</h4>*/
/* 								</div>*/
/* 							</div>*/
/* 						</a>*/
/* 					</div>*/
/* */
/* 					<div class="col-md-2 focus-grid">*/
/* 						<a href="{{path('crowdrise_projets_homepage')}}">*/
/* 							<div class="focus-border">*/
/* 								<div class="focus-layout">*/
/* 									<div class="focus-image"><i class="fa fa-car"></i></div>*/
/* 									<h4 class="clrchg">Projects</h4>*/
/* 								</div>*/
/* 							</div>*/
/* 						</a>*/
/* 					</div>	*/
/* 					*/
/* 					<div class="col-md-2 focus-grid">*/
/* 						<a href="{{path('crowdrise_problemes_homepage')}}">*/
/* 							<div class="focus-border">*/
/* 								<div class="focus-layout">*/
/* 									<div class="focus-image"><i class="fa fa-paw"></i></div>*/
/* 									<h4 class="clrchg">Stack'Probs</h4>*/
/* 								</div>*/
/* 							</div>*/
/* 						</a>*/
/* 					</div>	*/
/*                                     </center>*/
/* 					<div class="clearfix"></div>*/
/* 				</div>*/
/* 			</div>*/
/* 			<div class="trending-ads">*/
/* 				<div class="container">*/
/* 				<!-- slider -->*/
/* 				<div class="trend-ads">*/
/* 					<h2>Last work</h2>*/
/* 							<ul id="flexiselDemo3">*/
/* 								<li>*/
/* 									<div class="col-md-3 biseller-column">*/
/* 										<a href="single.html">*/
/* 											<img src="{{asset('images/p1.jpg')}}"/>*/
/* 											<span class="price">&#36; 450</span>*/
/* 										</a> */
/* 										<div class="ad-info">*/
/* 											<h5>There are many variations of passages</h5>*/
/* 											<span>1 hour ago</span>*/
/* 										</div>*/
/* 									</div>*/
/* 									<div class="col-md-3 biseller-column">*/
/* 										<a href="single.html">*/
/* 											<img src="{{asset('images/p2.jpg')}}"/>*/
/* 											<span class="price">&#36; 399</span>*/
/* 										</a> */
/* 										<div class="ad-info">*/
/* 											<h5>Lorem Ipsum is simply dummy</h5>*/
/* 											<span>3 hour ago</span>*/
/* 										</div>*/
/* 									</div>*/
/* 									<div class="col-md-3 biseller-column">*/
/* 										<a href="single.html">*/
/* 											<img src="{{asset('images/p3.jpg')}}"/>*/
/* 											<span class="price">&#36; 199</span>*/
/* 										</a> */
/* 										<div class="ad-info">*/
/* 											<h5>It is a long established fact that a reader</h5>*/
/* 											<span>8 hour ago</span>*/
/* 										</div>*/
/* 									</div>*/
/* 									<div class="col-md-3 biseller-column">*/
/* 										<a href="single.html">*/
/* 											<img src="{{asset('images/p4.jpg')}}"/>*/
/* 											<span class="price">&#36; 159</span>*/
/* 										</a> */
/* 										<div class="ad-info">*/
/* 											<h5>passage of Lorem Ipsum you need to be</h5>*/
/* 											<span>19 hour ago</span>*/
/* 										</div>*/
/* 									</div>*/
/* 								</li>*/
/* 								<li>*/
/* 									<div class="col-md-3 biseller-column">*/
/* 										<a href="single.html">*/
/* 											<img src="{{asset('images/p5.jpg')}}"/>*/
/* 											<span class="price">&#36; 1599</span>*/
/* 										</a> */
/* 										<div class="ad-info">*/
/* 											<h5>There are many variations of passages</h5>*/
/* 											<span>1 hour ago</span>*/
/* 										</div>*/
/* 									</div>*/
/* 									<div class="col-md-3 biseller-column">*/
/* 										<a href="single.html">*/
/* 											<img src="{{asset('images/p6.jpg')}}"/>*/
/* 											<span class="price">&#36; 1099</span>*/
/* 										</a> */
/* 										<div class="ad-info">*/
/* 											<h5>passage of Lorem Ipsum you need to be</h5>*/
/* 											<span>1 day ago</span>*/
/* 										</div>*/
/* 									</div>*/
/* 									<div class="col-md-3 biseller-column">*/
/* 										<a href="single.html">*/
/* 											<img src="{{asset('images/p7.jpg')}}"/>*/
/* 											<span class="price">&#36; 109</span>*/
/* 										</a> */
/* 										<div class="ad-info">*/
/* 											<h5>It is a long established fact that a reader</h5>*/
/* 											<span>9 hour ago</span>*/
/* 										</div>*/
/* 									</div>*/
/* 									<div class="col-md-3 biseller-column">*/
/* 										<a href="single.html">*/
/* 											<img src="{{asset('images/p8.jpg')}}"/>*/
/* 											<span class="price">&#36; 189</span>*/
/* 										</a> */
/* 										<div class="ad-info">*/
/* 											<h5>Lorem Ipsum is simply dummy</h5>*/
/* 											<span>3 hour ago</span>*/
/* 										</div>*/
/* 									</div>*/
/* 								</li>*/
/* 								<li>*/
/* 									<div class="col-md-3 biseller-column">*/
/* 										<a href="single.html">*/
/* 											<img src="{{asset('images/p9.jpg')}}"/>*/
/* 											<span class="price">&#36; 2599</span>*/
/* 										</a> */
/* 										<div class="ad-info">*/
/* 											<h5>Lorem Ipsum is simply dummy</h5>*/
/* 											<span>3 hour ago</span>*/
/* 										</div>*/
/* 									</div>*/
/* 									<div class="col-md-3 biseller-column">*/
/* 										<a href="single.html">*/
/* 											<img src="{{asset('images/p10.jpg')}}"/>*/
/* 											<span class="price">&#36; 3999</span>*/
/* 										</a> */
/* 										<div class="ad-info">*/
/* 											<h5>It is a long established fact that a reader</h5>*/
/* 											<span>9 hour ago</span>*/
/* 										</div>*/
/* 									</div>*/
/* 									<div class="col-md-3 biseller-column">*/
/* 										<a href="single.html">*/
/* 											<img src="{{asset('images/p11.jpg')}}"/>*/
/* 											<span class="price">&#36; 2699</span>*/
/* 										</a> */
/* 										<div class="ad-info">*/
/* 											<h5>passage of Lorem Ipsum you need to be</h5>*/
/* 											<span>1 day ago</span>*/
/* 										</div>*/
/* 									</div>*/
/* 									<div class="col-md-3 biseller-column">*/
/* 										<a href="single.html">*/
/* 											<img src="{{asset('images/p12.jpg')}}"/>*/
/* 											<span class="price">&#36; 899</span>*/
/* 										</a> */
/* 										<div class="ad-info">*/
/* 											<h5>There are many variations of passages</h5>*/
/* 											<span>1 hour ago</span>*/
/* 										</div>*/
/* 									</div>*/
/* 								</li>*/
/* 						</ul>*/
/* 					<script type="text/javascript">*/
/* 						 $(window).load(function() {*/
/* 							$("#flexiselDemo3").flexisel({*/
/* 								visibleItems:1,*/
/* 								animationSpeed: 1000,*/
/* 								autoPlay: true,*/
/* 								autoPlaySpeed: 5000,    		*/
/* 								pauseOnHover: true,*/
/* 								enableResponsiveBreakpoints: true,*/
/* 								responsiveBreakpoints: { */
/* 									portrait: { */
/* 										changePoint:480,*/
/* 										visibleItems:1*/
/* 									}, */
/* 									landscape: { */
/* 										changePoint:640,*/
/* 										visibleItems:1*/
/* 									},*/
/* 									tablet: { */
/* 										changePoint:768,*/
/* 										visibleItems:1*/
/* 									}*/
/* 								}*/
/* 							});*/
/* 							*/
/* 						});*/
/* 					   </script>*/
/* 					   <script type="text/javascript" src="{{asset('js/jquery.flexisel.js')}}"></script>*/
/* 					</div>   */
/* 			</div>*/
/* 			<!-- //slider -->				*/
/* 			</div>*/
/* 			<div class="mobile-app">*/
/* 				<div class="container">*/
/* 					<div class="col-md-5 app-left">*/
/* 						<a href="mobileapp.html"><img src="{{asset('images/app.png')}}" alt=""></a>*/
/* 					</div>*/
/* 					<div class="col-md-7 app-right">*/
/* 						<h3>Resale App is the <span>Easiest</span> way for Selling and buying second-hand goods</h3>*/
/* 						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam auctor Sed bibendum varius euismod. Integer eget turpis sit amet lorem rutrum ullamcorper sed sed dui. vestibulum odio at elementum. Suspendisse et condimentum nibh.</p>*/
/* 						<div class="app-buttons">*/
/* 							<div class="app-button">*/
/* 								<a href="#"><img src="{{asset('images/1.png')}}" alt=""></a>*/
/* 							</div>*/
/* 							<div class="app-button">*/
/* 								<a href="#"><img src="{{asset('images/2.png')}}" alt=""></a>*/
/* 							</div>*/
/* 							<div class="clearfix"> </div>*/
/* 						</div>*/
/* 					</div>*/
/* 					<div class="clearfix"></div>*/
/* 				</div>*/
/* 			</div>*/
/* 		</div>*/
/*                 {% endblock %}*/
/* */
/* 		<!--footer section start-->	*/
/*                 */
/*                 {% block footer %}*/
/* */
/* 		<footer>*/
/* 			<div class="footer-top">*/
/* 				<div class="container">*/
/* 					<div class="foo-grids">*/
/* 						<div class="col-md-3 footer-grid">*/
/* 							<h4 class="footer-head">Who We Are</h4>*/
/* 							<p>CrowdRise is the most innovative, modern crowdfunding platform for charitable causes.</p>*/
/* 							<p>With our plateform you will be able to raise money for your cause and keep 100% of what you raise..</p>*/
/* 						</div>*/
/* 						<div class="col-md-3 footer-grid">*/
/* 							<h4 class="footer-head">Help</h4>*/
/* 							<ul>*/
/* 								<li><a href="howitworks.html">How it Works</a></li>						*/
/* 								<li><a href="sitemap.html">Sitemap</a></li>*/
/* 								<li><a href="faq.html">Faq</a></li>*/
/* 								<li><a href="feedback.html">Feedback</a></li>*/
/* 								<li><a href="contact.html">Contact</a></li>*/
/* 								<li><a href="typography.html">Shortcodes</a></li>*/
/* 							</ul>*/
/* 						</div>*/
/* 						<div class="col-md-3 footer-grid">*/
/* 							<h4 class="footer-head">Information</h4>*/
/* 							<ul>*/
/* 								<li><a href="regions.html">Locations Map</a></li>	*/
/* 								<li><a href="terms.html">Terms of Use</a></li>*/
/* 								<li><a href="popular-search.html">Popular searches</a></li>	*/
/* 								<li><a href="privacy.html">Privacy Policy</a></li>	*/
/* 							</ul>*/
/* 						</div>*/
/* 						<div class="col-md-3 footer-grid">*/
/* 							<h4 class="footer-head">Contact Us</h4>*/
/* 							<span class="hq">DevXTeam</span>*/
/* 							<address>*/
/* 								<ul class="location">*/
/* 									<li><span class="glyphicon glyphicon-map-marker"></span></li>*/
/* 									<li>Esprit: Technopôle El Gazala, 2083, Tunisie</li>*/
/* 									<div class="clearfix"></div>*/
/* 								</ul>	*/
/* 								<ul class="location">*/
/* 									<li><span class="glyphicon glyphicon-earphone"></span></li>*/
/* 									<li>+(216) 94556903</li>*/
/* 									<div class="clearfix"></div>*/
/* 								</ul>	*/
/* 								<ul class="location">*/
/* 									<li><span class="glyphicon glyphicon-envelope"></span></li>*/
/* 									<li><a href="mailto:info@example.com">marwamimouni9@gmail.com</a></li>*/
/* 									<div class="clearfix"></div>*/
/* 								</ul>						*/
/* 							</address>*/
/* 						</div>*/
/* 						<div class="clearfix"></div>*/
/* 					</div>						*/
/* 				</div>	*/
/* 			</div>	*/
/* 			<div class="footer-bottom text-center">*/
/* 			<div class="container">*/
/* 				<div class="footer-logo">*/
/* 					<a href="index.html"><span>Re</span>sale</a>*/
/* 				</div>*/
/* 				<div class="footer-social-icons">*/
/* 					<ul>*/
/* 						<li><a class="facebook" href="#"><span>Facebook</span></a></li>*/
/* 						<li><a class="twitter" href="#"><span>Twitter</span></a></li>*/
/* 						<li><a class="flickr" href="#"><span>Flickr</span></a></li>*/
/* 						<li><a class="googleplus" href="#"><span>Google+</span></a></li>*/
/* 						<li><a class="dribbble" href="#"><span>Dribbble</span></a></li>*/
/* 					</ul>*/
/* 				</div>*/
/* 				<div class="copyrights">*/
/* 					<p> © 2016 Esprit. All Rights Reserved | Developed by  <a href="http://w3layouts.com/"> DevX Team</a></p>*/
/* 				</div>*/
/* 				<div class="clearfix"></div>*/
/* 			</div>*/
/* 		</div>*/
/* 		</footer>*/
/*                {% endblock %}*/
/* */
/*         <!--footer section end-->*/
/* </body>*/
/* </html>*/
