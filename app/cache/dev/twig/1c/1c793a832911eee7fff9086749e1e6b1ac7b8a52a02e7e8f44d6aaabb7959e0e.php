<?php

/* CrowdriseMailBundle:Default:mail.html.twig */
class __TwigTemplate_f1db328195fe87c6b6a8376c074d997160e7a4f1dafe68685867d0426724f8d1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("CrowdriseProfileBundle:Default:profile.html.twig", "CrowdriseMailBundle:Default:mail.html.twig", 1);
        $this->blocks = array(
            'container' => array($this, 'block_container'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "CrowdriseProfileBundle:Default:profile.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_container($context, array $blocks = array())
    {
        // line 3
        echo "<html>
    <head>
        <title>Formulaire Mail</title>

    </head>
    <center>   
    <form action=\"";
        // line 9
        echo $this->env->getExtension('routing')->getPath("crowdrise_mail_sendMail");
        echo "\" method=\"POST\">
        <h1> Contact administrator for reclamation </h1>
        <br/>
        
        <h2> Subject</h2>
        <input type=\"text\" name=\"Subject\">
        <br/>
        

        
        <h2> Text : </h2>
        <textarea name=\"message\"></textarea>
        
        <input type=\"submit\" value=\"Send\"/>
    </form>
       </center>
        
</html>
";
    }

    public function getTemplateName()
    {
        return "CrowdriseMailBundle:Default:mail.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  39 => 9,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends "CrowdriseProfileBundle:Default:profile.html.twig" %}*/
/* {% block container %}*/
/* <html>*/
/*     <head>*/
/*         <title>Formulaire Mail</title>*/
/* */
/*     </head>*/
/*     <center>   */
/*     <form action="{{path('crowdrise_mail_sendMail')}}" method="POST">*/
/*         <h1> Contact administrator for reclamation </h1>*/
/*         <br/>*/
/*         */
/*         <h2> Subject</h2>*/
/*         <input type="text" name="Subject">*/
/*         <br/>*/
/*         */
/* */
/*         */
/*         <h2> Text : </h2>*/
/*         <textarea name="message"></textarea>*/
/*         */
/*         <input type="submit" value="Send"/>*/
/*     </form>*/
/*        </center>*/
/*         */
/* </html>*/
/* {% endblock %}*/
/* */
