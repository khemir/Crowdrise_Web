<?php

/* IbrowsXeditableBundle::xeditablecollection.html.twig */
class __TwigTemplate_3fefefb2134aa9ae2c76c55f7ced15879009ba434319a7e38eaa0395af2aeafb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'start' => array($this, 'block_start'),
            'start_collection' => array($this, 'block_start_collection'),
            'create' => array($this, 'block_create'),
            'create_value' => array($this, 'block_create_value'),
            'delete' => array($this, 'block_delete'),
            'delete_value' => array($this, 'block_delete_value'),
            'edit' => array($this, 'block_edit'),
            'edit_value' => array($this, 'block_edit_value'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('start', $context, $blocks);
    }

    public function block_start($context, array $blocks = array())
    {
        // line 2
        echo "<ul class=\"";
        echo twig_escape_filter($this->env, (isset($context["path"]) ? $context["path"] : $this->getContext($context, "path")), "html", null, true);
        echo " inputCollection\" data-xeditable-replace=\"";
        echo twig_escape_filter($this->env, (isset($context["path"]) ? $context["path"] : $this->getContext($context, "path")), "html", null, true);
        echo "\" data-xeditable-replace-callback=\"reinitXeditable\">
    ";
        // line 3
        $this->displayBlock('start_collection', $context, $blocks);
        // line 54
        echo "</ul>
";
    }

    // line 3
    public function block_start_collection($context, array $blocks = array())
    {
        // line 4
        echo "        ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["viewParameters"]) ? $context["viewParameters"] : $this->getContext($context, "viewParameters")));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["formtype"] => $context["viewParameter"]) {
            // line 5
            echo "            ";
            if (twig_in_filter("create", $context["formtype"])) {
                // line 6
                echo "                ";
                $this->displayBlock('create', $context, $blocks);
                // line 20
                echo "            ";
            } elseif (twig_in_filter("delete", $context["formtype"])) {
                // line 21
                echo "                ";
                $this->displayBlock('delete', $context, $blocks);
                // line 35
                echo "            ";
            } else {
                // line 36
                echo "                ";
                $this->displayBlock('edit', $context, $blocks);
                // line 50
                echo "            ";
            }
            // line 51
            echo "
        ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['formtype'], $context['viewParameter'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 53
        echo "    ";
    }

    // line 6
    public function block_create($context, array $blocks = array())
    {
        // line 7
        echo "                    <li  class=\"new\"  >
                        <a
                                href=\"#\"
                                class=\"new ibrowsXeditable\"
                                data-xeditable
                                ";
        // line 12
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["viewParameter"]) ? $context["viewParameter"] : $this->getContext($context, "viewParameter")), "attributes", array()));
        foreach ($context['_seq'] as $context["key"] => $context["value"]) {
            echo twig_escape_filter($this->env, $context["key"], "html", null, true);
            echo "=\"";
            echo twig_escape_filter($this->env, $context["value"], "html", null, true);
            echo "\"";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['value'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 13
        echo "                                >
                            ";
        // line 14
        $this->displayBlock('create_value', $context, $blocks);
        // line 17
        echo "                        </a>
                    </li>
                ";
    }

    // line 14
    public function block_create_value($context, array $blocks = array())
    {
        // line 15
        echo "                                <i class=\"fa fa-plus\"></i> ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("add"), "html", null, true);
        echo "
                            ";
    }

    // line 21
    public function block_delete($context, array $blocks = array())
    {
        // line 22
        echo "                    <li class=\"remove\">
                    <a
                            href=\"#\"
                            class=\"remove ibrowsXeditable\"
                            data-xeditable
                            ";
        // line 27
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["viewParameter"]) ? $context["viewParameter"] : $this->getContext($context, "viewParameter")), "attributes", array()));
        foreach ($context['_seq'] as $context["key"] => $context["value"]) {
            echo twig_escape_filter($this->env, $context["key"], "html", null, true);
            echo "=\"";
            echo twig_escape_filter($this->env, $context["value"], "html", null, true);
            echo "\"";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['value'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 28
        echo "                            >
                    ";
        // line 29
        $this->displayBlock('delete_value', $context, $blocks);
        // line 32
        echo "                    </a>
                    </li>
                ";
    }

    // line 29
    public function block_delete_value($context, array $blocks = array())
    {
        // line 30
        echo "                        <i class=\"fa fa-times\"></i> ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("remove"), "html", null, true);
        echo "
                    ";
    }

    // line 36
    public function block_edit($context, array $blocks = array())
    {
        // line 37
        echo "                    <li  >
                        <a
                                href=\"#\"
                                class=\"ibrowsXeditable\"
                                data-xeditable
                                ";
        // line 42
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["viewParameter"]) ? $context["viewParameter"] : $this->getContext($context, "viewParameter")), "attributes", array()));
        foreach ($context['_seq'] as $context["key"] => $context["value"]) {
            echo twig_escape_filter($this->env, $context["key"], "html", null, true);
            echo "=\"";
            echo twig_escape_filter($this->env, $context["value"], "html", null, true);
            echo "\"";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['value'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 43
        echo "                                >
                            ";
        // line 44
        $this->displayBlock('edit_value', $context, $blocks);
        // line 47
        echo "                        </a>
                    </li>
                ";
    }

    // line 44
    public function block_edit_value($context, array $blocks = array())
    {
        // line 45
        echo "                                 <span class=\"value\">";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["viewParameter"]) ? $context["viewParameter"] : $this->getContext($context, "viewParameter")), "value", array()), "html", null, true);
        echo "</span>
                            ";
    }

    public function getTemplateName()
    {
        return "IbrowsXeditableBundle::xeditablecollection.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  230 => 45,  227 => 44,  221 => 47,  219 => 44,  216 => 43,  204 => 42,  197 => 37,  194 => 36,  187 => 30,  184 => 29,  178 => 32,  176 => 29,  173 => 28,  161 => 27,  154 => 22,  151 => 21,  144 => 15,  141 => 14,  135 => 17,  133 => 14,  130 => 13,  118 => 12,  111 => 7,  108 => 6,  104 => 53,  89 => 51,  86 => 50,  83 => 36,  80 => 35,  77 => 21,  74 => 20,  71 => 6,  68 => 5,  50 => 4,  47 => 3,  42 => 54,  40 => 3,  33 => 2,  27 => 1,);
    }
}
/* {% block start %}*/
/* <ul class="{{ path }} inputCollection" data-xeditable-replace="{{ path }}" data-xeditable-replace-callback="reinitXeditable">*/
/*     {% block start_collection %}*/
/*         {% for formtype, viewParameter in viewParameters %}*/
/*             {% if 'create' in formtype %}*/
/*                 {% block create %}*/
/*                     <li  class="new"  >*/
/*                         <a*/
/*                                 href="#"*/
/*                                 class="new ibrowsXeditable"*/
/*                                 data-xeditable*/
/*                                 {% for key, value in viewParameter.attributes %}{{ key }}="{{ value }}"{% endfor %}*/
/*                                 >*/
/*                             {% block create_value %}*/
/*                                 <i class="fa fa-plus"></i> {{ 'add'|trans }}*/
/*                             {% endblock %}*/
/*                         </a>*/
/*                     </li>*/
/*                 {% endblock %}*/
/*             {% elseif 'delete' in formtype %}*/
/*                 {% block delete %}*/
/*                     <li class="remove">*/
/*                     <a*/
/*                             href="#"*/
/*                             class="remove ibrowsXeditable"*/
/*                             data-xeditable*/
/*                             {% for key, value in viewParameter.attributes %}{{ key }}="{{ value }}"{% endfor %}*/
/*                             >*/
/*                     {% block delete_value %}*/
/*                         <i class="fa fa-times"></i> {{ 'remove'|trans }}*/
/*                     {% endblock %}*/
/*                     </a>*/
/*                     </li>*/
/*                 {% endblock %}*/
/*             {% else %}*/
/*                 {% block edit %}*/
/*                     <li  >*/
/*                         <a*/
/*                                 href="#"*/
/*                                 class="ibrowsXeditable"*/
/*                                 data-xeditable*/
/*                                 {% for key, value in viewParameter.attributes %}{{ key }}="{{ value }}"{% endfor %}*/
/*                                 >*/
/*                             {% block edit_value %}*/
/*                                  <span class="value">{{ viewParameter.value }}</span>*/
/*                             {% endblock %}*/
/*                         </a>*/
/*                     </li>*/
/*                 {% endblock %}*/
/*             {% endif %}*/
/* */
/*         {% endfor %}*/
/*     {% endblock %}*/
/* </ul>*/
/* {% endblock %}*/
/* */
