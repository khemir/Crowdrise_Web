<?php

/* CrowdriseAdministrationBundle:Default:ideas_requests.html.twig */
class __TwigTemplate_5927b5ccc5b092b56231bdbfb6085cbddb870f004975b8cfcaab38337e443a87 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("CrowdriseAdministrationBundle::layout.html.twig", "CrowdriseAdministrationBundle:Default:ideas_requests.html.twig", 1);
        $this->blocks = array(
            'nav_li_dash' => array($this, 'block_nav_li_dash'),
            'nav_li_ideasreq' => array($this, 'block_nav_li_ideasreq'),
            'pagecontent' => array($this, 'block_pagecontent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "CrowdriseAdministrationBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_nav_li_dash($context, array $blocks = array())
    {
        // line 4
        echo "<li><a href=\"";
        echo $this->env->getExtension('routing')->getPath("crowdrise_administration_homepage");
        echo "\"><span>Dashboard</span> <i class=\"icon-screen2\"></i></a></li>
";
    }

    // line 7
    public function block_nav_li_ideasreq($context, array $blocks = array())
    {
        // line 8
        echo "<li class=\"active\"><a href=\"";
        echo $this->env->getExtension('routing')->getPath("crowdrise_administration_ideas_requests");
        echo "\"><span>Ideas Requests</span> <i class=\"icon-stack\"></i></a> </li>
";
    }

    // line 12
    public function block_pagecontent($context, array $blocks = array())
    {
        // line 13
        echo "
    <!-- Page content -->
  <div class=\"page-content\">
    <!-- Page header -->
    <div class=\"page-header\">
      <div class=\"page-title\">
        <h3>Ideas Requests <small>Management</small></h3>
      </div>

      <div id=\"reportrange\" class=\"range\">
        <div class=\"visible-xs header-element-toggle\"><a class=\"btn btn-primary btn-icon\"><i class=\"icon-calendar\"></i></a></div>
        <div class=\"date-range\"></div>
        <span class=\"label label-danger\">9</span></div>

    </div>
    <!-- /page header -->
    <!-- Breadcrumbs line -->
    <div class=\"breadcrumb-line\">
      <ul class=\"breadcrumb\">
        <li><a href=\"index.html\">Home</a></li>
        <li class=\"active\">Requests list</li>
      </ul>
      <div class=\"visible-xs breadcrumb-toggle\"><a class=\"btn btn-link btn-lg btn-icon\" data-toggle=\"collapse\" data-target=\".breadcrumb-buttons\"><i class=\"icon-menu2\"></i></a></div>
     

    </div>
    <!-- /breadcrumbs line -->
    <!-- Page tabs -->
    <div class=\"tabbable page-tabs\">
   
      <div class=\"tab-content\">
        <!-- First tab -->
        <div class=\"tab-pane active fade in\" id=\"all-tasks\">


          <!-- Tasks table -->
          <div class=\"panel panel-default\">
            <div class=\"panel-heading\">
              <h6 class=\"panel-title\"><i class=\"icon-paragraph-justify2\"></i> Requests</h6>
              <span class=\"pull-right label label-danger\">

              3

              </span> </div>
            <div class=\"datatable-tasks\">


              <table class=\"table table-bordered\">
                <thead>
                  <tr>
                    <th>Idea Description</th>
                    <th>Category</th>
                    <th class=\"task-priority\">\$.Needed</th>
                    <th class=\"task-date-added\">Date Added</th>
                    <th class=\"task-progress\">Submitter</th>
                    <th class=\"task-deadline\">Idea Video</th>
                    <th class=\"task-tools text-center\">Actions</th>
                  </tr>
                </thead>
                <tbody>

                    
                    ";
        // line 75
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["idees"]) ? $context["idees"] : $this->getContext($context, "idees")));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 76
            echo "                  <tr>


                    <td class=\"task-desc\"><a href=\"task_detailed.html\">";
            // line 79
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "intituleIdee", array()), "html", null, true);
            echo "</a> <span>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "descriptionIdee", array()), "html", null, true);
            echo "</span></td>


                    <td>";
            // line 82
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "categorieIdee", array()), "html", null, true);
            echo "</td>

                    ";
            // line 84
            if (($this->getAttribute($context["i"], "sommeRecolteIdee", array()) < 200)) {
                // line 85
                echo "                    <td class=\"text-center\"><span class=\"label label-info\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "sommeRecolteIdee", array()), "html", null, true);
                echo " \$</span></td>
                    ";
            } elseif ((($this->getAttribute(            // line 86
$context["i"], "sommeRecolteIdee", array()) > 200) && ($this->getAttribute($context["i"], "sommeRecolteIdee", array()) < 800))) {
                // line 87
                echo "                    <td class=\"text-center\"><span class=\"label label-success\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "sommeRecolteIdee", array()), "html", null, true);
                echo " \$</span></td>
                    ";
            } else {
                // line 89
                echo "                    <td class=\"text-center\"><span class=\"label label-danger\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "sommeRecolteIdee", array()), "html", null, true);
                echo " \$</span></td>
                    ";
            }
            // line 91
            echo "
                    <td>";
            // line 92
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "dateDepotIdee", array()), "html", null, true);
            echo "</td>


                    <td>";
            // line 95
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["i"], "idUtilisateur", array()), "username", array()), "html", null, true);
            echo "</td>


                    <td> <a href=\"";
            // line 98
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "urlVideo", array()), "html", null, true);
            echo "\">Watch Idea Video</a></td>
                    
                  
                    <td class=\"text-center\"><div class=\"btn-group\">
                        <button type=\"button\" class=\"btn btn-icon btn-success dropdown-toggle\" data-toggle=\"dropdown\"><i class=\"icon-cog4\"></i></button>
                        <ul class=\"dropdown-menu icons-right dropdown-menu-right\">
                          <li><a href=\"";
            // line 104
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("crowdrise_administration_accept_idea", array("id" => $this->getAttribute($context["i"], "idIdee", array()), "email" => $this->getAttribute($this->getAttribute($context["i"], "idUtilisateur", array()), "email", array()))), "html", null, true);
            echo "\"><i class=\"icon-checkmark3\"></i> Accept</a></li>
                          <li><a href=\"";
            // line 105
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("crowdrise_administration_refuse_idea", array("id" => $this->getAttribute($context["i"], "idIdee", array()))), "html", null, true);
            echo "\"><i class=\"icon-share2\"></i> Refuse</a></li>
                          <li><a href=\"";
            // line 106
            echo $this->env->getExtension('routing')->getPath("crowdrise_administration_ideas_requests");
            echo "\"><i class=\"icon-quill2\"></i> Waiting</a></li>
           
                        </ul>
                      </div></td>
                  </tr>
                  
                  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 113
        echo "
                 
                </tbody>
              </table>
            </div>
          </div>
          <!-- /tasks table -->
        </div>
        <!-- /first tab -->

      </div>
    </div>
    <!-- /page tabs -->
    <!-- Footer -->
    <div class=\"footer clearfix\">
      <div class=\"pull-left\">&copy; 2016. Crowdrise Admin by <a href=\"http://themeforest.net/user/Kopyov\">DevX Team</a></div>
      <div class=\"pull-right icons-group\"> <a href=\"#\"><i class=\"icon-screen2\"></i></a> <a href=\"#\"><i class=\"icon-balance\"></i></a> <a href=\"#\"><i class=\"icon-cog3\"></i></a> </div>
    </div>
    <!-- /footer -->
  </div>
  <!-- /page content -->
";
    }

    public function getTemplateName()
    {
        return "CrowdriseAdministrationBundle:Default:ideas_requests.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  205 => 113,  192 => 106,  188 => 105,  184 => 104,  175 => 98,  169 => 95,  163 => 92,  160 => 91,  154 => 89,  148 => 87,  146 => 86,  141 => 85,  139 => 84,  134 => 82,  126 => 79,  121 => 76,  117 => 75,  53 => 13,  50 => 12,  43 => 8,  40 => 7,  33 => 4,  30 => 3,  11 => 1,);
    }
}
/* {% extends "CrowdriseAdministrationBundle::layout.html.twig" %}*/
/* */
/* {% block nav_li_dash %}*/
/* <li><a href="{{path('crowdrise_administration_homepage')}}"><span>Dashboard</span> <i class="icon-screen2"></i></a></li>*/
/* {% endblock %}*/
/* */
/* {% block nav_li_ideasreq %}*/
/* <li class="active"><a href="{{path('crowdrise_administration_ideas_requests')}}"><span>Ideas Requests</span> <i class="icon-stack"></i></a> </li>*/
/* {% endblock %}*/
/* */
/* */
/* {% block pagecontent %}*/
/* */
/*     <!-- Page content -->*/
/*   <div class="page-content">*/
/*     <!-- Page header -->*/
/*     <div class="page-header">*/
/*       <div class="page-title">*/
/*         <h3>Ideas Requests <small>Management</small></h3>*/
/*       </div>*/
/* */
/*       <div id="reportrange" class="range">*/
/*         <div class="visible-xs header-element-toggle"><a class="btn btn-primary btn-icon"><i class="icon-calendar"></i></a></div>*/
/*         <div class="date-range"></div>*/
/*         <span class="label label-danger">9</span></div>*/
/* */
/*     </div>*/
/*     <!-- /page header -->*/
/*     <!-- Breadcrumbs line -->*/
/*     <div class="breadcrumb-line">*/
/*       <ul class="breadcrumb">*/
/*         <li><a href="index.html">Home</a></li>*/
/*         <li class="active">Requests list</li>*/
/*       </ul>*/
/*       <div class="visible-xs breadcrumb-toggle"><a class="btn btn-link btn-lg btn-icon" data-toggle="collapse" data-target=".breadcrumb-buttons"><i class="icon-menu2"></i></a></div>*/
/*      */
/* */
/*     </div>*/
/*     <!-- /breadcrumbs line -->*/
/*     <!-- Page tabs -->*/
/*     <div class="tabbable page-tabs">*/
/*    */
/*       <div class="tab-content">*/
/*         <!-- First tab -->*/
/*         <div class="tab-pane active fade in" id="all-tasks">*/
/* */
/* */
/*           <!-- Tasks table -->*/
/*           <div class="panel panel-default">*/
/*             <div class="panel-heading">*/
/*               <h6 class="panel-title"><i class="icon-paragraph-justify2"></i> Requests</h6>*/
/*               <span class="pull-right label label-danger">*/
/* */
/*               3*/
/* */
/*               </span> </div>*/
/*             <div class="datatable-tasks">*/
/* */
/* */
/*               <table class="table table-bordered">*/
/*                 <thead>*/
/*                   <tr>*/
/*                     <th>Idea Description</th>*/
/*                     <th>Category</th>*/
/*                     <th class="task-priority">$.Needed</th>*/
/*                     <th class="task-date-added">Date Added</th>*/
/*                     <th class="task-progress">Submitter</th>*/
/*                     <th class="task-deadline">Idea Video</th>*/
/*                     <th class="task-tools text-center">Actions</th>*/
/*                   </tr>*/
/*                 </thead>*/
/*                 <tbody>*/
/* */
/*                     */
/*                     {% for i in idees %}*/
/*                   <tr>*/
/* */
/* */
/*                     <td class="task-desc"><a href="task_detailed.html">{{i.intituleIdee}}</a> <span>{{i.descriptionIdee}}</span></td>*/
/* */
/* */
/*                     <td>{{i.categorieIdee}}</td>*/
/* */
/*                     {% if i.sommeRecolteIdee<200 %}*/
/*                     <td class="text-center"><span class="label label-info">{{i.sommeRecolteIdee}} $</span></td>*/
/*                     {% elseif i.sommeRecolteIdee>200 and i.sommeRecolteIdee<800 %}*/
/*                     <td class="text-center"><span class="label label-success">{{i.sommeRecolteIdee}} $</span></td>*/
/*                     {% else%}*/
/*                     <td class="text-center"><span class="label label-danger">{{i.sommeRecolteIdee}} $</span></td>*/
/*                     {% endif %}*/
/* */
/*                     <td>{{i.dateDepotIdee}}</td>*/
/* */
/* */
/*                     <td>{{i.idUtilisateur.username}}</td>*/
/* */
/* */
/*                     <td> <a href="{{i.urlVideo}}">Watch Idea Video</a></td>*/
/*                     */
/*                   */
/*                     <td class="text-center"><div class="btn-group">*/
/*                         <button type="button" class="btn btn-icon btn-success dropdown-toggle" data-toggle="dropdown"><i class="icon-cog4"></i></button>*/
/*                         <ul class="dropdown-menu icons-right dropdown-menu-right">*/
/*                           <li><a href="{{path('crowdrise_administration_accept_idea',{'id':i.idIdee,'email':i.idUtilisateur.email})}}"><i class="icon-checkmark3"></i> Accept</a></li>*/
/*                           <li><a href="{{path('crowdrise_administration_refuse_idea',{'id':i.idIdee})}}"><i class="icon-share2"></i> Refuse</a></li>*/
/*                           <li><a href="{{path('crowdrise_administration_ideas_requests')}}"><i class="icon-quill2"></i> Waiting</a></li>*/
/*            */
/*                         </ul>*/
/*                       </div></td>*/
/*                   </tr>*/
/*                   */
/*                   {% endfor %}*/
/* */
/*                  */
/*                 </tbody>*/
/*               </table>*/
/*             </div>*/
/*           </div>*/
/*           <!-- /tasks table -->*/
/*         </div>*/
/*         <!-- /first tab -->*/
/* */
/*       </div>*/
/*     </div>*/
/*     <!-- /page tabs -->*/
/*     <!-- Footer -->*/
/*     <div class="footer clearfix">*/
/*       <div class="pull-left">&copy; 2016. Crowdrise Admin by <a href="http://themeforest.net/user/Kopyov">DevX Team</a></div>*/
/*       <div class="pull-right icons-group"> <a href="#"><i class="icon-screen2"></i></a> <a href="#"><i class="icon-balance"></i></a> <a href="#"><i class="icon-cog3"></i></a> </div>*/
/*     </div>*/
/*     <!-- /footer -->*/
/*   </div>*/
/*   <!-- /page content -->*/
/* {% endblock %}*/
/* */
