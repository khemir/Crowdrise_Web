<?php

/* CrowdriseUserBundle:Projet:AfficheProjet.html.twig */
class __TwigTemplate_760d50be492e967e52fd6240af4ed78be49f0d218ed0e0e750ec9721cd8a7bc0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("CrowdriseUserBundle:Profile:show_content.html.twig", "CrowdriseUserBundle:Projet:AfficheProjet.html.twig", 1);
        $this->blocks = array(
            'projet' => array($this, 'block_projet'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "CrowdriseUserBundle:Profile:show_content.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_projet($context, array $blocks = array())
    {
        echo " 

    <h1> Projects List</h1>
    ";
        // line 6
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["projet"]) ? $context["projet"] : $this->getContext($context, "projet")));
        foreach ($context['_seq'] as $context["_key"] => $context["p"]) {
            // line 7
            echo "    ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "intituleProjet", array()), "html", null, true);
            echo "           
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['p'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 9
        echo "    
    ";
    }

    public function getTemplateName()
    {
        return "CrowdriseUserBundle:Projet:AfficheProjet.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  48 => 9,  39 => 7,  35 => 6,  28 => 3,  11 => 1,);
    }
}
/* {% extends "CrowdriseUserBundle:Profile:show_content.html.twig" %}*/
/* */
/* {% block projet %} */
/* */
/*     <h1> Projects List</h1>*/
/*     {% for p in projet %}*/
/*     {{p.intituleProjet}}           */
/*     {% endfor %}*/
/*     */
/*     {% endblock %}*/
