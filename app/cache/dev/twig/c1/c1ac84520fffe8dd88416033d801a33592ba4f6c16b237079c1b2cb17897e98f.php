<?php

/* FOSUserBundle:Registration:register_content.html.twig */
class __TwigTemplate_5473ad64bf55812ad549005f14b7b78d6d092dbf0c95dcb61b95bf270c1773ec extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "<div id=\"page-wrapper\" class=\"sign-in-wrapper\">
\t\t\t\t<div class=\"graphs\">
\t\t\t\t\t<div class=\"sign-up\">
\t\t\t\t\t\t<h1>Create an account</h1>
\t\t\t\t\t\t<p class=\"creating\"> <strong>Having hands on experience in creating innovative ideas,projects
                                                      you want to sponsor you projects, you have come to the wright place.</strong></p>
\t\t\t\t\t\t<h2>Personal Information</h2>
\t\t\t\t\t\t
             
                 <div class=\"sign-u\">\t
                     
";
        // line 13
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start', array("method" => "post", "action" => $this->env->getExtension('routing')->getPath("fos_user_registration_register"), "attr" => array("class" => "fos_user_registration_register")));
        echo "


";
        // line 31
        echo "<div class=\"form-group\">
        ";
        // line 32
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nomUtilisateur", array()), 'label', array("label" => "Forname*:"));
        echo "
        ";
        // line 33
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nomUtilisateur", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
        ";
        // line 34
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nomUtilisateur", array()), 'errors');
        echo "               
    </div>
   <div class=\"form-group\">
        ";
        // line 37
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "prenomUtilisateur", array()), 'label', array("label" => "Name*:"));
        echo "
        ";
        // line 38
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "prenomUtilisateur", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
        ";
        // line 39
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "prenomUtilisateur", array()), 'errors');
        echo "               
    </div>
    <div class=\"form-group\">";
        // line 41
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "email", array()), 'row', array("attr" => array("class" => "form-control")));
        echo "</div>
    <div class=\"form-group\">";
        // line 42
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "username", array()), 'row', array("attr" => array("class" => "form-control")));
        echo "</div>
   
    <div class=\"form-group\">";
        // line 44
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "plainPassword", array()), "first", array()), 'row', array("attr" => array("class" => "form-control")));
        echo "</div>
    <div class=\"form-group\">";
        // line 45
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "plainPassword", array()), "second", array()), 'row', array("attr" => array("class" => "form-control")));
        echo "</div>
   ";
        // line 47
        echo "    
    <div >
        <input type=\"submit\" value=\"";
        // line 49
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("valider", array(), "FOSUserBundle"), "html", null, true);
        echo "\" />
    </div>
     ";
        // line 51
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'rest');
        echo "

";
        // line 53
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
 </div>
                                   </div>  
 
</div>
</div>";
    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:register_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  99 => 53,  94 => 51,  89 => 49,  85 => 47,  81 => 45,  77 => 44,  72 => 42,  68 => 41,  63 => 39,  59 => 38,  55 => 37,  49 => 34,  45 => 33,  41 => 32,  38 => 31,  32 => 13,  19 => 2,);
    }
}
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* <div id="page-wrapper" class="sign-in-wrapper">*/
/* 				<div class="graphs">*/
/* 					<div class="sign-up">*/
/* 						<h1>Create an account</h1>*/
/* 						<p class="creating"> <strong>Having hands on experience in creating innovative ideas,projects*/
/*                                                       you want to sponsor you projects, you have come to the wright place.</strong></p>*/
/* 						<h2>Personal Information</h2>*/
/* 						*/
/*              */
/*                  <div class="sign-u">	*/
/*                      */
/* {{ form_start(form, {'method': 'post', 'action': path('fos_user_registration_register'), 'attr': {'class': 'fos_user_registration_register'}}) }}*/
/* */
/* */
/* {#{{dump(form.nom.vars)}}*/
/* */
/* {% set class=''%}*/
/* */
/* {% if not form.nom.vars.valid%}*/
/*     {%set class='has error'%}*/
/* {%endif%}*/
/*     {{ form_widget(form) }}*/
/*     <div class="form-group">*/
/*         <label class="control-label" for="{{form.nom.vars.id}}">Nom*: </label>*/
/*         <input type="text"  class="form-control" id="{{form.nom.vars.id}}" name="{{form.nom.vars.full_name}}" value="{{form.nom.vars.value}}">*/
/*         {{form_errors(form.nom)}}*/
/*         {% do form.nom.setRendered %}*/
/*         {{dump(form.nom)}}*/
/*     </div>#}*/
/* <div class="form-group">*/
/*         {{form_label(form.nomUtilisateur, 'Forname*:')}}*/
/*         {{form_widget(form.nomUtilisateur,{'attr': {'class': 'form-control'}})}}*/
/*         {{form_errors(form.nomUtilisateur)}}               */
/*     </div>*/
/*    <div class="form-group">*/
/*         {{form_label(form.prenomUtilisateur, 'Name*:')}}*/
/*         {{form_widget(form.prenomUtilisateur,{'attr': {'class': 'form-control'}})}}*/
/*         {{form_errors(form.prenomUtilisateur)}}               */
/*     </div>*/
/*     <div class="form-group">{{ form_row(form.email,{'attr': {'class': 'form-control'}}) }}</div>*/
/*     <div class="form-group">{{ form_row(form.username,{'attr': {'class': 'form-control'}}) }}</div>*/
/*    */
/*     <div class="form-group">{{ form_row(form.plainPassword.first,{'attr': {'class': 'form-control'}}) }}</div>*/
/*     <div class="form-group">{{ form_row(form.plainPassword.second,{'attr': {'class': 'form-control'}}) }}</div>*/
/*    {#radio inline#}*/
/*     */
/*     <div >*/
/*         <input type="submit" value="{{ 'valider'|trans }}" />*/
/*     </div>*/
/*      {{ form_rest(form) }}*/
/* */
/* {{ form_end(form) }}*/
/*  </div>*/
/*                                    </div>  */
/*  */
/* </div>*/
/* </div>*/
