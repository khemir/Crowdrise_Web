<?php

/* CrowdriseAdministrationBundle:Default:index.html.twig */
class __TwigTemplate_2060f37f3382341c1d9b1dd96ba1f338747e5aeef7dfb67f093bac0963b31756 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("CrowdriseAdministrationBundle::layout.html.twig");

        $this->blocks = array(
            'nbusers' => array($this, 'block_nbusers'),
            'nbideas' => array($this, 'block_nbideas'),
            'nbprojects' => array($this, 'block_nbprojects'),
            'nbclaims' => array($this, 'block_nbclaims'),
            'bundleext_pie' => array($this, 'block_bundleext_pie'),
            'recentidasactivity' => array($this, 'block_recentidasactivity'),
            'recentclaimsactivity' => array($this, 'block_recentclaimsactivity'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "CrowdriseAdministrationBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_nbusers($context, array $blocks = array())
    {
        echo "<h2>";
        echo twig_escape_filter($this->env, (isset($context["nbUsers"]) ? $context["nbUsers"] : null), "html", null, true);
        echo "</h2>";
    }

    // line 5
    public function block_nbideas($context, array $blocks = array())
    {
        echo "<h2>";
        echo twig_escape_filter($this->env, (isset($context["nbIdeas"]) ? $context["nbIdeas"] : null), "html", null, true);
        echo "</h2>";
    }

    // line 7
    public function block_nbprojects($context, array $blocks = array())
    {
        echo "<h2>";
        echo twig_escape_filter($this->env, (isset($context["nbProjects"]) ? $context["nbProjects"] : null), "html", null, true);
        echo "</h2>";
    }

    // line 9
    public function block_nbclaims($context, array $blocks = array())
    {
        echo "<h2>";
        echo twig_escape_filter($this->env, (isset($context["nbClaims"]) ? $context["nbClaims"] : null), "html", null, true);
        echo "</h2>";
    }

    // line 11
    public function block_bundleext_pie($context, array $blocks = array())
    {
        echo "           ";
    }

    // line 14
    public function block_recentidasactivity($context, array $blocks = array())
    {
        // line 15
        echo "                    
";
        // line 16
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["idees"]) ? $context["idees"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 17
            echo "
<li class=\"media\"><a class=\"pull-left\" href=\"#\"><img class=\"media-object\" src=\"";
            // line 18
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/images/demo/users/avatar.png"), "html", null, true);
            echo "\" alt=\"\"></a>
<div class=\"media-body\">
    
<div class=\"clearfix\"><a href=\"#\" class=\"media-heading\">";
            // line 21
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["i"], "idUtilisateur", array()), "username", array()), "html", null, true);
            echo "</a><span class=\"media-notice\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "dateDepotIdee", array()), "html", null, true);
            echo "</span></div>

Added a new idea called <b>";
            // line 23
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "intituleIdee", array()), "html", null, true);
            echo "</b> in category : <a>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "categorieIdee", array()), "html", null, true);
            echo "</a>.
</div>
</li>

";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 28
        echo "
";
    }

    // line 33
    public function block_recentclaimsactivity($context, array $blocks = array())
    {
        // line 34
        echo "
";
        // line 35
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["reclamations"]) ? $context["reclamations"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
            // line 36
            echo "
<li class=\"media\"><a class=\"pull-left\" href=\"#\"><img class=\"media-object\" src=\"";
            // line 37
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/images/demo/users/avatar.png"), "html", null, true);
            echo "\" alt=\"\"></a>
<div class=\"media-body\">
    
<div class=\"clearfix\"><a href=\"#\" class=\"media-heading\">";
            // line 40
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["r"], "idUtilisateur", array()), "username", array()), "html", null, true);
            echo "</a><span class=\"media-notice\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["r"], "dateReclamation", array()), "html", null, true);
            echo "</span></div>

Sent a new Claim.

</div>
</li>

";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 48
        echo "
";
    }

    public function getTemplateName()
    {
        return "CrowdriseAdministrationBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  151 => 48,  135 => 40,  129 => 37,  126 => 36,  122 => 35,  119 => 34,  116 => 33,  111 => 28,  98 => 23,  91 => 21,  85 => 18,  82 => 17,  78 => 16,  75 => 15,  72 => 14,  66 => 11,  58 => 9,  50 => 7,  42 => 5,  34 => 3,);
    }
}
